<?php

/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use App;
use BO\Slim\Render;
use BO\Zmsdb\Department as DepartmentRepository;
use BO\Zmsdb\Exception\Pdo\DeadLockFound;
use BO\Zmsdb\Exception\Pdo\LockTimeout;
use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsdb\Useraccount as UseraccountRepository;
use BO\Zmsentities\Collection\UseraccountList as Collection;
use BO\Zmsentities\Permission;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @suppressWarnings(coupling)
 */
class UseraccountList extends BaseController
{
    protected UseraccountRepository $userAccountRepo;
    protected DepartmentRepository $departmentRepo;

    /**
     * @suppressWarnings(unused)
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @param array $args
     * @return ResponseInterface
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws PDOFailed
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        (new Helper\User($request, 2))->checkRights(Permission::RIGHT_USERACCOUNT);

        $validator = $request->getAttribute('validator');
        $resolveReferences = $validator->getParameter('resolveReferences')->isNumber()->setDefault(1)->getValue();
        $type = $validator->getParameter('type')->isString()->getValue();

        $this->userAccountRepo = new UseraccountRepository();
        $this->departmentRepo = new DepartmentRepository();
        $useraccountList = new Collection();

        if (!$type || $type == 'all') {
            $useraccountList = $this->userAccountRepo->readList($resolveReferences, App::$now);
        }
        if ($type == 'systemwide') {
            $useraccountList = $this->userAccountRepo->readSystemWideAccessUserList($resolveReferences, App::$now);
        }

        $message = Response\Message::create($request);
        $message->data = $useraccountList;

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message->setUpdatedMetaData());
    }
}
