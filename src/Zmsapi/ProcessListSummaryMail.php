<?php
/**
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmsapi;

use App;
use BO\Slim\Render;

use BO\Zmsapi\Helper\Version;
use BO\Zmsapi\Exception\Mail\MailRecipientMissing as MailRecipientMissingException;
use BO\Zmsapi\Exception\Mail\MailSenderFromMissing as MailSenderFromMissingException;
use BO\Zmsapi\Exception\Process\ProcessListSummaryTooOften as ProcessListSummaryTooOftenException;

use BO\Zmsdb\Config as ConfigRepository;
use BO\Zmsdb\Helper\MailService;
use BO\Zmsdb\Mail as MailRepository;
use BO\Zmsdb\Process as ProcessRepository;
use BO\Zmsdb\Department as DepartmentRepository;
use BO\Zmsdb\Exception\Mail\ClientWithoutEmail as ClientWithoutEmailException;

use BO\Zmsentities\Process as ProcessEntity;
use BO\Zmsentities\Collection\ProcessList;
use BO\Zmsentities\Exception\TemplateNotFound as TemplateNotFoundException;
use BO\Zmsentities\EventLog as EventLogEntity;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @SuppressWarnings(Coupling)
 */
class ProcessListSummaryMail extends BaseController
{
    public const REQUEST_REPETITION_SEC = 600;

    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     * @throws ClientWithoutEmailException
     * @throws MailRecipientMissingException
     * @throws MailSenderFromMissingException
     * @throws ProcessListSummaryTooOftenException
     * @throws TemplateNotFoundException
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $validator = $request->getAttribute('validator');
        $mailAddress = $validator->getParameter('mail')->isMail()->hasDNS()->assertValid()->getValue();
        $limit = $validator->getParameter('limit')->isNumber()->setDefault(50)->getValue();

        $mailService = new MailService(new MailRepository(), new ConfigRepository(), new DepartmentRepository());
        $mailService->setMailAddress($mailAddress);
        $mailService->testEventLogEntries(EventLogEntity::CLIENT_PROCESSLIST_REQUEST, self::REQUEST_REPETITION_SEC);
        $mailService->setProcessList($this->readProcessListByEmail($mailAddress, $limit));
        $entity = $mailService->readResolvedEntity(MailService::MAIL_TYPE_OVERVIEW);
        $mail = $mailService->writeMailInQueue($entity, App::$now, false);
        if (!$mail) {
            throw new MailRecipientMissingException();
        }

        $message = Response\Message::create($request);
        $message->data = $mail;

        $mailService->writeEventLogEntry(
            EventLogEntity::CLIENT_PROCESSLIST_REQUEST,
            'zmsapi ' . Version::getString(),
            EventLogEntity::LIVETIME_DAY
        );
        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message->setUpdatedMetaData(), $message->getStatuscode());
    }

    protected function readProcessListByEmail(string $mailAddress, int $limit): ProcessList
    {
        $processList = (new ProcessRepository())->readListByMailAndStatusList(
            $mailAddress,
            [
                ProcessEntity::STATUS_CONFIRMED,
                ProcessEntity::STATUS_PICKUP
            ],
            2,
            $limit,
            ['Datum' => 'ASC', 'Uhrzeit' => 'ASC']
        );
        return $processList->withoutExpiredAppointmentDate(App::$now);
    }
}
