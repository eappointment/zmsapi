<?php
/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use BO\Slim\Render;

use BO\Mellon\Validator;

use BO\Zmsapi\Exception\Scope\ScopeNotFound;

use BO\Zmsapi\Helper\Mimepart as MimepartHelper;
use BO\Zmsdb\Connection\Select as DBConnection;
use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsdb\Scope as ScopeRepository;

use BO\Zmsentities\Exception\SchemaValidation as SchemaValidationException ;
use BO\Zmsentities\Mimepart as MimepartEntity;
use BO\Zmsentities\Useraccount\EntityAccess;
use BO\Zmsentities\Scope as ScopeEntity;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @SuppressWarnings(CouplingBetweenObjects)
 */
class ScopeCalldisplayImageDataUpdate extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     * @throws SchemaValidationException
     * @throws ScopeNotFound
     * @throws PDOFailed
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $scope = (new ScopeRepository)->readEntity($args['id']);
        $this->testScope($scope);

        (new Helper\User($request, 2))->checkRights(
            'scope',
            new EntityAccess($scope)
        );

        $input = Validator::input()->isJson()->getValue();
        $mimepart = new MimepartEntity($input);
        MimepartHelper::testImageSize($mimepart);

        DBConnection::getWriteConnection();
        $message = Response\Message::create($request);
        $message->data = (new ScopeRepository)->writeImageData($scope->id, $mimepart);
        DBConnection::writeCommit();

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message, $message->getStatuscode());
    }

    /**
     * @param ScopeEntity|null $scope
     *
     * @throws ScopeNotFound
     */
    protected function testScope(?ScopeEntity $scope)
    {
        if (!$scope) {
            throw new Exception\Scope\ScopeNotFound();
        }
    }
}
