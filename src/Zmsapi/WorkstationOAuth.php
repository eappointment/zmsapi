<?php

namespace BO\Zmsapi;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

use BO\Slim\Render;
use BO\Mellon\Validator;

use BO\Zmsdb\Workstation;
use BO\Zmsdb\Useraccount;
use BO\Zmsdb\Connection\Select as DBConnection;

use BO\Zmsentities\Useraccount as UseraccountEntity;

use BO\Zmsapi\Exception\Workstation\WorkstationAuthFailed as WorkstationAuthFailedException;

/**
 * @SuppressWarnings(Coupling)
 */
class WorkstationOAuth extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @return String
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ) {
        $validator = $request->getAttribute('validator');
        $resolveReferences = $validator->getParameter('resolveReferences')->isNumber()->setDefault(2)->getValue();
        $state  = $validator->getParameter('state')->isString()->isSmallerThan(40)->isBiggerThan(30)->getValue();
        $input = Validator::input()->isJson()->assertValid()->getValue();
        $entity = (new UseraccountEntity)->createFromOpenIdData($input);
        $entity->testValid();

        if (null === $state || $request->getHeaderLine('X-Authkey') !== $state) {
            throw new WorkstationAuthFailedException();
        }
        DBConnection::getWriteConnection();
        if ((new Useraccount())->readIsUserExisting($entity->getId())) {
            $workstation = $this->getLoggedInWorkstationByOidc($request, $entity, $resolveReferences);
        } else {
            $workstation = $this->writeNewOAuthWorkstation($entity, $state, $resolveReferences);
        }

        $message = Response\Message::create($request);
        $message->data = $workstation;

        $response = Render::withLastModified($response, time(), '0');
        $response = Render::withJson($response, $message->setUpdatedMetaData(), $message->getStatuscode());
        return $response;
    }

    protected function getLoggedInWorkstationByOidc($request, $entity, $resolveReferences)
    {
        Helper\UserAuth::testUseraccountExists($entity->getId());
        
        $workstation = (new Helper\User($request, $resolveReferences))->readWorkstation();
        Helper\User::testWorkstationIsOveraged($workstation);
        
        if (WorkstationLogin::hasLoginHash($entity)) {
            (new Workstation)->writeEntityLogoutByName($entity->id);
            $workstation = $this->writeLoggedInOidc($request, $entity, $resolveReferences);
            $exception = new \BO\Zmsapi\Exception\Useraccount\UserAlreadyLoggedIn();
            $exception->data['authkey'] = $workstation->authkey;
            throw $exception;
        }
        $workstation = $this->writeLoggedInOidc($request, $entity, $resolveReferences);
        return $workstation;
    }

    protected function writeLoggedInOidc($request, $entity, $resolveReferences)
    {
        $workstation = (new Workstation)->writeEntityLoginByOidc(
            $entity->id,
            $request->getHeaderLine('X-Authkey'),
            \App::getNow(),
            $resolveReferences
        );
        return $workstation;
    }

    protected function writeNewOAuthWorkstation(UseraccountEntity $entity, $state, $resolveReferences)
    {
        $useraccount = (new Useraccount)->writeEntity($entity);
        $query = new Workstation();
        $workstation = $query->writeEntityLoginByName(
            $useraccount->getId(),
            $entity->password,
            \App::getNow(),
            $resolveReferences
        );
        $workstation = $query->updateEntityAuthkey(
            $useraccount->getId(),
            $entity->password,
            $state,
            $resolveReferences
        );
        return $workstation;
    }
}
