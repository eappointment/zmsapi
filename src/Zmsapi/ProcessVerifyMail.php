<?php
/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use App;
use BO\Slim\Render;

use BO\Zmsapi\Exception\Mail\MailSenderFromMissing;
use BO\Zmsdb\Config as ConfigRepository;
use BO\Zmsdb\Connection\Select as DBConnection;
use BO\Zmsdb\Department as DepartmentRepository;
use BO\Zmsdb\Exception\Mail\ClientWithoutEmail;
use BO\Zmsdb\Helper\MailService;
use BO\Zmsdb\Mail as MailRepository;
use BO\Zmsdb\Exception\Mail\ClientWithoutEmail as ClientWithoutEmailException;

use BO\Zmsentities\Process as ProcessEntity;
use BO\Zmsentities\Department as DepartmentEntity;
use BO\Zmsentities\Mail as MailEntity;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @SuppressWarnings(Coupling)
 */
class ProcessVerifyMail extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     * @throws MailSenderFromMissing
     * @throws ClientWithoutEmail
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        DBConnection::setCriticalReadSession();
        $validator = $request->getAttribute('validator');
        $input = $validator::input()->isJson()->assertValid()->getValue();
        $entity = new MailEntity($input);
        $this->testEntity($entity);
        $mailService = new MailService(new MailRepository(), new ConfigRepository(), new DepartmentRepository());
        $mail = $mailService->writeMailInQueue($entity, App::$now, false);

        $message = Response\Message::create($request);
        $message->data = $mail ?: $entity;

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message->setUpdatedMetaData(), $message->getStatuscode());
    }

    /**
     * @param MailEntity $entity
     *
     * @throws ClientWithoutEmailException
     */
    protected function testEntity(MailEntity $entity)
    {
        $email = $entity->getFirstClient()->toProperty()->email->get();
        if ($email === null || $email === '') {
            throw new ClientWithoutEmailException();
        }
    }
}
