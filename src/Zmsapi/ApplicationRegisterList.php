<?php
/**
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmsapi;

use BO\Mellon\Validator;
use BO\Slim\Render;
use BO\Zmsdb\ApplicationRegister as Repository;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class ApplicationRegisterList extends BaseController
{
    /**
     * @var Repository
     */
    private $repository;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->repository = new Repository();
        if ($container->has('zmsdb.repositories.application-register')) {
            $this->repository = $container->get('zmsdb.repositories.application-register');
        }
    }

    /**
     * @SuppressWarnings(Param)
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ) {
        (new Helper\User($request))->checkRights('scope');

        /** @var Validator $validator */
        $validator = $request->getAttribute('validator');
        if ($validator->hasParameter('scopeIds')) {
            $param           = Validator::param('scopeIds')->isString()->getValue();
            $scopesIds       = empty($param) ? [] : explode(',', $param);
            $applicationList = $this->repository->readListByScopeIds(
                $scopesIds,
                Validator::param('type')->isString()->getValue()
            );
        } else {
            $applicationList = $this->repository->readList();
        }

        $message = Response\Message::create($request);
        $message->data = $applicationList;

        $response = Render::withLastModified($response, time());
        return Render::withJson($response, $message);
    }
}
