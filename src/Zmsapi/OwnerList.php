<?php
/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use BO\Mellon\Exception;
use BO\Slim\Render;
use BO\Mellon\Validator;
use BO\Zmsdb\Owner as Query;
use BO\Zmsentities\Permission;
use BO\Zmsentities\Collection\OwnerList as Collection;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class OwnerList extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @param array $args
     * @return ResponseInterface
     * @throws Exception
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $workstation = (new Helper\User($request, 2))::$workstation;
        $workstation->getUseraccount()->testRights([
            Permission::RIGHT_USERACCOUNT,
        ]);
        $resolveReferences = Validator::param('resolveReferences')->isNumber()->setDefault(1)->getValue();

        $ownerList = (new Query())->readList($resolveReferences);

        $message = Response\Message::create($request);
        $message->data = $ownerList->withAccess($workstation->getUseraccount());

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message);
    }
}
