<?php

/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/
namespace BO\Zmsapi;

use App;

use BO\Zmsapi\Exception\Apikey\ApiKeyLocked as ApiKeyLockedException;
use BO\Zmsapi\Exception\Process\ProcessAlreadyConfirmed;
use BO\Zmsdb\Apikey as ApikeyRepository;
use BO\Zmsdb\Process as ProcessRepository;
use BO\Zmsdb\Exception\Pdo\DeadLockFound;
use BO\Zmsdb\Exception\Pdo\LockTimeout;
use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsdb\Exception\Process\ProcessReserveFailed as ProcessReserveFailedException;
use BO\Zmsdb\ProcessStatusFree;

use BO\Zmsapi\Exception\Apikey\AccessDenied as AccessDeniedException;
use BO\Zmsapi\Exception\Apikey\ApiKeyNotValid as ApiKeyNotValidException;
use BO\Zmsapi\Helper\ApikeyService;

use BO\Slim\Render;

use BO\Zmsentities\Process as ProcessEntity;
use BO\Zmsentities\Collection\ProcessList;
use BO\Zmsentities\Schema\Entity;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @SuppressWarnings(Cyclomatic)
 * @SuppressWarnings(CouplingBetweenObjects)
 */
class ApikeyProcessReserve extends BaseController
{
    protected $slotsRequired;

    protected $slotType;

    protected $resolveReferences;

    /**
     * @SuppressWarnings(Param)
     *
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     * @throws AccessDeniedException
     * @throws ApiKeyLockedException
     * @throws ApiKeyNotValidException
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws PDOFailed
     * @throws ProcessAlreadyConfirmed
     * @throws ProcessReserveFailedException
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $validator = $request->getAttribute('validator');
        $key = isset($args['key']) ? $validator::value($args['key'])->isString()->isRequired()->getValue() : null;
        $this->slotsRequired = $validator::param('slotsRequired')->isNumber()->getValue();
        $this->resolveReferences = $validator::param('resolveReferences')->isNumber()->setDefault(1)->getValue();
        $input = $validator::input()->isJson()->assertValid()->getValue();
        $process = new ProcessEntity($input);

        $apikeyService = new ApikeyService($key);
        $this->slotType = $apikeyService->getApiclient()->accesslevel;
        if ($this->slotType != 'intern') {
            $this->slotsRequired = 0;
            $process = (new ProcessRepository())->readSlotCount($process);
        }

        $apikeyService->testSession();
        $apikeyService->testHasConfirmedProcess($process);
        $apikeyService->testApikeyAccess();
        $process->apiclient = $apikeyService->getApiclient();
        $process = $this->writeProcess($process, $apikeyService);
        (new ApikeyRepository())->writeProcessInSession($apikeyService->getApikey()->getId(), $process);

        $message = Response\Message::create($request);
        $message->data = $process;

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message->setUpdatedMetaData(), $message->getStatuscode());
    }

    /**
     *
     * @param ProcessEntity $process
     * @param               $apikeyService
     *
     * @return ProcessEntity
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws PDOFailed
     * @throws ProcessReserveFailedException
     */
    protected function writeProcess(ProcessEntity $process, $apikeyService): ProcessEntity
    {
        $sessionProcess = $apikeyService->getMatchingProcessFromSession($process);
        if ($sessionProcess && $sessionProcess->getStatus() == ProcessEntity::STATUS_RESERVED) {
            $process = $this->writeWithNewAppointment($sessionProcess, $process);
        } else {
            $process = $this->writeReservedProcess($process);
        }
        return $process;
    }

    /**
     * @param $process
     *
     * @return ProcessEntity|Entity|null
     * @throws ProcessReserveFailedException
     */
    protected function writeReservedProcess($process)
    {
        return (new ProcessStatusFree)
            ->writeEntityReserved(
                $process,
                App::$now,
                $this->slotType,
                $this->slotsRequired,
                $this->resolveReferences
            );
    }

    protected function writeWithNewAppointment(ProcessEntity $oldProcess, ProcessEntity $newProcess)
    {
        $newProcess->getFirstAppointment()->testValid();
        return ProcessRepository::init()->writeEntityWithNewAppointment(
            $oldProcess,
            $newProcess->getFirstAppointment(),
            App::$now,
            $this->slotType,
            $this->slotsRequired,
            $this->resolveReferences,
            true
        );
    }
}
