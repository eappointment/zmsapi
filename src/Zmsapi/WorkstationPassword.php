<?php

/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmsapi;

use BO\Mellon\Exception;
use BO\Slim\Render;
use BO\Zmsapi\Exception\Useraccount\InvalidCredentials;
use BO\Zmsdb\Useraccount as UseraccountRepository;
use BO\Zmsentities\Exception\SchemaValidation;
use BO\Zmsentities\Useraccount as UseraccountEntity;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class WorkstationPassword extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @param array $args
     * @return ResponseInterface
     * @throws InvalidCredentials
     * @throws Exception
     * @throws \BO\Mellon\Failure\Exception
     * @throws SchemaValidation
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $workstation = (new Helper\User($request, 3))->checkRights();
        $useraccount = $workstation->getUseraccount();

        $validator = $request->getAttribute('validator');
        $input = $validator->getInput()->isJson()->assertValid()->getValue();
        $password = isset($input['password']) ? $validator::value($input['password'])->isString()->getValue() : null;
        $entity = new UseraccountEntity($input);
        $entity->testValid();
        if (isset($input['email'])) {
            $useraccount->email = $input['email'];
        }
        Helper\UserAuth::testPasswordMatching($useraccount, $password);
        if (isset($input['changePassword'])) {
            $useraccount->password = $useraccount->getHash(reset($input['changePassword']));
        }

        $message = Response\Message::create($request);
        $message->data = (new UseraccountRepository())->writeUpdatedEntity($useraccount->getId(), $useraccount);

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message->setUpdatedMetaData(), $message->getStatuscode());
    }
}
