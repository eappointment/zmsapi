<?php
/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use App;
use BO\Slim\Render;

use BO\Zmsapi\Exception\Apikey\AccessDenied as AccessDeniedException;
use BO\Zmsapi\Exception\Apikey\ApiKeyLocked as ApiKeyLockedException;
use BO\Zmsapi\Exception\Apikey\ApiKeyNotValid as ApiKeyNotValidException;
use BO\Zmsapi\Exception\Matching\RequestNotFound as RequestNotFoundException;
use BO\Zmsapi\Exception\Process\ApiclientInvalid as ApiclientInvalidExeption;
use BO\Zmsapi\Exception\Process\ProcessAlreadyConfirmed;
use BO\Zmsapi\Exception\Process\ProcessAlreadyExists as ProcessAlreadyExistsException;

use BO\Zmsdb\Connection\Select as DBConnection;
use BO\Zmsdb\Exception\Process\ProcessReserveFailed as ProcessReserveFailedException;
use BO\Zmsdb\Process as ProcessRepository;
use BO\Zmsdb\ProcessStatusFree;

use BO\Zmsentities\Process as ProcessEntity;
use BO\Zmsentities\Schema\Entity as SchemaEntity;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @SuppressWarnings(Coupling)
 */
class ProcessReserve extends BaseController
{
    protected $slotsRequired;

    protected $slotType;

    protected $resolveReferences;

    private $userAccount = null;

    /**
     * @SuppressWarnings(Param)
     * @SuppressWarnings(Complexity)
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     * @throws AccessDeniedException
     * @throws ApiKeyLockedException
     * @throws ApiKeyNotValidException
     * @throws ApiclientInvalidExeption
     * @throws ProcessAlreadyExistsException
     * @throws ProcessReserveFailedException
     * @throws RequestNotFoundException
     * @throws ProcessAlreadyConfirmed
     */
    public function readResponse(
        RequestInterface  $request,
        ResponseInterface $response,
        array             $args
    ): ResponseInterface {
        $validator = $request->getAttribute('validator');
        $this->slotsRequired = $validator::param('slotsRequired')->isNumber()->getValue();
        $this->slotType = $validator::param('slotType')->isString()->getValue();
        $this->resolveReferences = $validator::param('resolveReferences')->isNumber()->setDefault(1)->getValue();
        $input = $validator::input()->isJson()->assertValid()->getValue();
        $process = new ProcessEntity($input);
        $this->testProcessExists($process);

        DBConnection::setCriticalReadSession();

        if ($this->slotType || $this->slotsRequired) {
            $workstation = (new Helper\User($request))->checkRights();
            $this->userAccount = (isset($workstation)) ?  $workstation->getUseraccount() : null;
            Helper\Matching::testCurrentScopeHasRequest($process);
        } else {
            $this->slotsRequired = 0;
            $this->slotType = 'public';
            $process= (new ProcessRepository())->readSlotCount($process);
        }

        $message = Response\Message::create($request);
        $message->data = $this->writeReservedProcess($process);

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message->setUpdatedMetaData(), $message->getStatuscode());
    }

    /**
     * @param $process
     *
     * @throws ProcessAlreadyExistsException
     */
    protected function testProcessExists($process)
    {
        if ($process && $process->hasId()) {
            throw new ProcessAlreadyExistsException();
        }
    }

    /**
     * @param $process
     *
     * @return ProcessEntity|SchemaEntity|null
     * @throws ProcessReserveFailedException
     */
    protected function writeReservedProcess($process)
    {
        return (new ProcessStatusFree)
            ->writeEntityReserved(
                $process,
                App::$now,
                $this->slotType,
                $this->slotsRequired,
                $this->resolveReferences,
                $this->userAccount
            );
    }
}
