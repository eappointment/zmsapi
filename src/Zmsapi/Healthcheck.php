<?php
/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use \BO\Slim\Render;
use BO\Zmsclient\Status;
use \BO\Zmsdb\Status as Query;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class Healthcheck extends BaseController
{
    /**
     * @SuppressWarnings(UnusedFormalParameter)
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $response = Status::testStatus($response, function () {
            return (new Query())->readEntity(\App::$now, false);
        });
        return Render::withLastModified($response, time(), '5');
    }
}
