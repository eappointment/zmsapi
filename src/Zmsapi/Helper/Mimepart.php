<?php

declare(strict_types=1);

namespace BO\Zmsapi\Helper;

use BO\Zmsdb\Scope as ScopeRepository;
use BO\Zmsentities\Exception\SchemaValidation as SchemaValidationException;
use BO\Zmsentities\Mimepart as MimepartEntity;
use League\JsonGuard\ValidationError;

class Mimepart
{
    /**
     * @param MimepartEntity $mimepart
     *
     * @throws SchemaValidationException
     */
    public static function testImageSize(MimepartEntity $mimepart)
    {
        if (mb_strlen($mimepart->content) > ScopeRepository::IMAGE_SIZE_LIMIT_BYTES) {
            $msg   = 'Die Größe des Bildes überschreitet den angegebenen Grenzwert.';
            $error = new ValidationError(
                $msg,
                'maxLength',
                ['string'],
                null,
                'content',
                new \stdClass(),
                '/properties/content/maxLength'
            );

            throw (new SchemaValidationException())->setValidationError([$error]);
        }
    }
}
