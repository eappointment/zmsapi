<?php

namespace BO\Zmsapi\Helper;

use BO\Zmsdb\Apiclient as ApiclientRepository;
use BO\Zmsdb\Apikey as ApikeyRepository;
use BO\Zmsdb\Process as ProcessRepository;
use BO\Zmsdb\Session as SessionRepository;
use BO\Zmsdb\Useraccount as UseraccountRepository;

use BO\Zmsdb\Exception\Pdo\DeadLockFound;
use BO\Zmsdb\Exception\Pdo\LockTimeout;
use BO\Zmsdb\Exception\Pdo\PDOFailed;

use BO\Zmsentities\Apiclient as ApiclientEntity;
use BO\Zmsentities\Process as ProcessEntity;
use BO\Zmsentities\Useraccount as UseraccountEntity;
use BO\Zmsentities\Apikey as ApikeyEntity;
use BO\Zmsentities\Session as SessionEntity;
use BO\Zmsentities\Schema\Entity as SchemaEntity;
use BO\Zmsentities\Collection\ProcessList;

use BO\Zmsapi\Exception\Apikey\ApiKeyNotValid as ApiKeyNotValidException;
use BO\Zmsapi\Exception\Apikey\AccessDenied as AccessDeniedException;
use BO\Zmsapi\Exception\Apikey\ApiKeyLocked as ApiKeyLockedException;
use BO\Zmsapi\Exception\Apikey\MissingRequiredData as MissingRequiredDataException;
use BO\Zmsapi\Exception\Process\ProcessAlreadyConfirmed as ProcessAlreadyConfirmedException;
use Psr\Http\Message\RequestInterface;

/**
 * @SuppressWarnings(Complexity)
 * @SuppressWarnings(CouplingBetweenObjects)
 */
class ApikeyService
{
    const API_KEY_ACCESS_LEVEL = 'public';
    const API_KEY_SESSION_NAME = 'apikey';
    const MIN_SECONDS_TO_INVALIDATION = 172800; //48 hours

    /* @var ApikeyEntity */
    private $apikey;

    /* @var ApiclientEntity */
    private $apiclient;

    /* @var SessionEntity */
    private $session;

    /* @var UseraccountEntity */
    private $useraccount;

    /* @var string */
    private $token;

    /**
     * @param string|null $key
     * @param string|null $token
     * @param bool        $tokenRequired
     * @param bool        $testLockStatus
     *
     * @throws AccessDeniedException
     * @throws ApiKeyLockedException
     * @throws ApiKeyNotValidException
     */
    public function __construct(
        string $key = null,
        string $token = null,
        bool $tokenRequired = false,
        bool $testLockStatus = true
    ) {
        $this->testTokenAccess($token, $tokenRequired);
        $this->setUseraccount();
        $this->setApikeyAndClient($key);
        $this->testApikeyAccess($testLockStatus);
        $this->testApiclientAccess();
        $this->fetchSessionData();
    }

    /**
     * test access to useraccount by token
     *
     * @param string|null $token
     * @param bool        $isRequired
     *
     * @return void
     * @throws AccessDeniedException
     */
    protected function testTokenAccess(string $token = null, bool $isRequired = false): void
    {
        if (! $token && $isRequired) {
            throw new AccessDeniedException();
        }
        $this->token = $token;
    }

    /**
     * @return void
     */
    protected function setUseraccount(): void
    {
        if ($this->token) {
            $this->useraccount = (new UseraccountRepository())->readEntityByToken($this->token);
        }
    }

    /**
     * @param string|null $key
     *
     * @return void
     */
    public function setApikeyAndClient(string $key = null)
    {
        if ($key) {
            if ($key == 'default') {
                $this->apiclient = (new ApiclientRepository())->readEntity($key);
                $this->apikey = new ApikeyEntity([
                    'key' => $this->apiclient->getId(),
                    'apiclient' => $this->apiclient
                ]);
            } else {
                $this->apikey = (new ApikeyRepository())->readEntity($key);
                $this->apiclient = $this->apikey->getApiClient();
            }
        } elseif ($this->getUseraccount() && $this->getUseraccount()->hasId()) {
            $this->apiclient = $this->writeApiclientByUseraccount();
            $this->apikey = $this->writeApikeyByClient();
        }
    }

    /**
     * @param bool $testLockStatus
     *
     * @throws ApiKeyLockedException
     * @throws ApiKeyNotValidException
     */
    public function testApikeyAccess(bool $testLockStatus = true)
    {
        if (! $this->apikey || ! $this->apikey->hasId()) {
            throw new ApiKeyNotValidException();
        }
        if ($testLockStatus && $this->apikey->isLocked()) {
            throw new ApiKeyLockedException();
        }
    }

    /**
     * @throws AccessDeniedException
     */
    public function testSession()
    {
        if (! $this->getSessionEntity() || ! $this->getSessionEntity()->hasId()) {
            throw new AccessDeniedException();
        }
    }

    /**
     * test access to api by apiclient settings
     *
     * @return void
     * @throws AccessDeniedException
     */
    protected function testApiclientAccess(): void
    {
        if (! $this->apikey->getApiClient()->hasId()) {
            throw new AccessDeniedException();
        }
        if (! isset($this->apikey->getApiClient()->accesslevel) ||
            $this->apikey->getApiClient()->accesslevel == ProcessEntity::STATUS_BLOCKED
        ) {
            throw new AccessDeniedException();
        }
    }

    /**
     * @throws ProcessAlreadyConfirmedException
     */
    public function testHasConfirmedProcess(ProcessEntity $process = null)
    {
        if ($this->hasSessionProcessList()) {
            $processList = (new ProcessList())->addData($this->session->getContent()['processList']);
            foreach ($processList as $entity) {
                if ($entity->getStatus() == ProcessEntity::STATUS_CONFIRMED) {
                    throw new ProcessAlreadyConfirmedException();
                }
            }
            $processList = (new ProcessRepository())->readProcessListByClientkey($this->getApiclient()->clientKey);
            if ($process && $processList->hasEntity($process->getId())) {
                $existingProcess = $processList->getEntity($process->getid());
                if ($existingProcess->getStatus() == ProcessEntity::STATUS_CONFIRMED) {
                    throw new ProcessAlreadyConfirmedException();
                }
            }
        }
    }

    /**
     * @throws MissingRequiredDataException
     */
    public function testHasSessionProcessList()
    {
        if (! $this->hasSessionProcessList()) {
            throw new MissingRequiredDataException();
        }
    }

    /**
     * A process in the session is always assigned to a location and always has all services from the session.
     * This checks whether there is a process in the session that has the same assignments to services and locations.
     */
    public function getMatchingProcessFromSession($process)
    {
        if ($this->hasSessionProcessList()) {
            $processList = (new ProcessList())->addData($this->session->getContent()['processList']);
            foreach ($processList as $listEntity) {
                if ($listEntity->getRequests()->hasRequests($process->getRequests()->getIdsCsv()) &&
                    $listEntity->getScopeId() == $process->getScopeId()
                ) {
                    return new ProcessEntity($listEntity);
                }
            }
        }
        return null;
    }

    /**
     * @param int $processId
     *
     * @return ProcessEntity|null
     */
    public function getProcessFromSessionById(int $processId): ?ProcessEntity
    {
        if ($this->hasSessionProcessList()) {
            $processList = (new ProcessList())->addData($this->session->getContent()['processList']);
            return $processList->getEntity($processId);
        }
        return null;
    }

    /**
     *
     * @return ProcessList
     */
    public function getProcessListFromSession(): ProcessList
    {
        $processList = new ProcessList();
        if (! $this->hasSessionProcessList()) {
            return $processList;
        }
        return $processList->addData($this->session->getContent()['processList']);
    }

    protected function hasSessionProcessList(): bool
    {
        return $this->session && $this->session->hasId() && isset($this->session->getContent()['processList']);
    }

    /**
     * write apiclient by useraccount
     * @return ApiclientEntity
     */
    protected function writeApiclientByUseraccount(): ApiclientEntity
    {
        $clientKey = (new UseraccountRepository())->readEntityIdByLoginName($this->getUseraccount()->getId());
        $apiClientEntity = (new ApiclientRepository())->readEntityByUserLoginName($this->getUseraccount()->getId());
        if (! $apiClientEntity->hasId()) {
            $apiClientEntity->clientKey = $clientKey;
            $apiClientEntity->shortname = $this->getUseraccount()->getId();
            $apiClientEntity->accesslevel = self::API_KEY_ACCESS_LEVEL;
            $apiClientEntity->secondsToInvalidation = self::MIN_SECONDS_TO_INVALIDATION;
            if ($this->token) {
                $apiClientEntity->permission['remotecall'] = 1;
            }
            $apiClientEntity = (new ApiclientRepository())->writeEntity($apiClientEntity);
        }
        return $apiClientEntity;
    }

    /**
     * writes a new apikey by apiclient
     *
     * @return ApikeyEntity
     */
    protected function writeApikeyByClient(): ApikeyEntity
    {
        $apikeyEntity = (new ApikeyEntity())->setApiclient($this->apiclient);
        $apikeyEntity->key = bin2hex(random_bytes(16));
        return (new ApikeyRepository())->writeEntity($apikeyEntity);
    }

    /**
     * @return void
     */
    protected function fetchSessionData(): void
    {
        if ($this->getApikey()->getId()) {
            $this->session = (new SessionRepository())
                ->readEntityBasic(self::API_KEY_SESSION_NAME, $this->getApikey()->getId());
        }
    }

    /**
     * write session for apiclient
     *
     * @return void
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws PDOFailed
     */
    public function writeNewSession(): void
    {
        $session = (new SessionEntity())->withClearedContent();
        $session->name = self::API_KEY_SESSION_NAME;
        $session->id = $this->getApikey()->getId();
        $session->testValid();
        $this->session = (new SessionRepository())->updateEntityBasic($session);
    }

    /**
     * @throws PDOFailed
     * @throws DeadLockFound
     * @throws LockTimeout
     */
    public function deleteExpiredApikey()
    {
        if ($this->getApikey()->isExpired(\App::$now)) {
            (new ApikeyRepository())->deleteEntity($this->getApikey()->getId());
        }
    }

    public function deleteSession(): void
    {
        (new SessionRepository())->deleteEntity(self::API_KEY_SESSION_NAME, $this->getApikey()->getId());
        unset($this->session);
    }

    /**
     * @return ApiclientEntity|SchemaEntity
     */
    public function getApiclient(): ApiclientEntity
    {
        return $this->apiclient;
    }

    /**
     * @return ApikeyEntity|SchemaEntity
     */
    public function getApikey()
    {
        return $this->apikey;
    }

    /**
     * @return SchemaEntity|UseraccountEntity
     */
    public function getUseraccount()
    {
        return $this->useraccount;
    }

    /**
     * @return SchemaEntity|SessionEntity
     */
    public function getSessionEntity()
    {
        return $this->session;
    }

    /**
     * @return string|null
     */
    public function getToken()
    {
        return $this->token;
    }
}
