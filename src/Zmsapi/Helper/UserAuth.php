<?php

namespace BO\Zmsapi\Helper;

use BO\Zmsapi\Exception\Useraccount\InvalidCredentials;
use BO\Zmsdb\Apikey as ApikeyRepository;
use BO\Zmsdb\Useraccount;
use BO\Zmsentities\Useraccount as UseraccountEntity;

class UserAuth
{
     /**
     * Get existing useraccount entity with verified password hash
     *
     * @return array $useraccount
    */
    public static function getVerifiedUseraccount($entity)
    {
        $useraccountQuery = new Useraccount();
        $useraccount = $useraccountQuery->readEntity($entity->getId())->withVerifiedHash($entity->password);
        $useraccount = $useraccountQuery->writeUpdatedEntity($useraccount->getId(), $useraccount);
        return $useraccount;
    }

    /**
     * @param UseraccountEntity $useraccount
     * @param string|null $password
     * @return void
     * @throws InvalidCredentials
     */
    public static function testPasswordMatching(UseraccountEntity $useraccount, ?string $password = null): void
    {
        $exception = new InvalidCredentials();
        $exception->data['password']['messages'] = [
            'Der Nutzername und das Passwort passen nicht zusammen'
        ];
        if (!$password) {
            throw $exception;
        }

        $result = (!str_starts_with($useraccount->password, '$')) ?
            ($useraccount->password === md5($password)) :
            password_verify($password, $useraccount->password);
        if (! $result) {
            throw $exception;
        }
    }


    /**
     * Get useraccount entity by http basic auth or XAuthKey
     *
     * @return array $useraccount
    */
    public static function getUseraccountByAuthMethod($request)
    {
        $useraccount = null;
        $basicAuth = static::getBasicAuth($request);
        $xAuthKey = static::getXAuthKey($request);
        $xApiKey = static::getXApiKey($request);

        $useraccountQuery = new Useraccount();

        if ($basicAuth && static::testUseraccountExists($basicAuth['username'])) {
            $useraccount = $useraccountQuery
                ->readEntity($basicAuth['username'])
                ->withVerifiedHash($basicAuth['password']);
            static::testPasswordMatching($useraccount, $basicAuth['password']);
            $useraccount = $useraccountQuery->writeUpdatedEntity($useraccount->getId(), $useraccount);
        } elseif ($xAuthKey) {
            $useraccount = $useraccountQuery->readEntityByAuthKey($xAuthKey);
        } elseif ($xApiKey) {
            $apikey = (new ApikeyRepository())->readEntity($xApiKey);
            $useraccountId = $useraccountQuery->readEntityIdByLoginName($apikey->getApiClient()->shortname);
            $useraccount = $useraccountQuery->readEntityByUserId($useraccountId);
        }

        return $useraccount;
    }

    /**
     * Test if useraccount exists in db
     *
     * @return exception $exception
    */
    public static function testUseraccountExists($loginName, $password = false)
    {
        $query = new Useraccount();
        if (! $query->readIsUserExisting($loginName, $password)) {
            $exception = new InvalidCredentials();
            $exception->data['password']['messages'] = [
                'Der Nutzername oder das Passwort wurden falsch eingegeben'
            ];
            throw $exception;
        }
        return true;
    }

    /**
     * Get Basic Authorization header content.
     *
     * @return array $authorization
     */
    private static function getBasicAuth($request)
    {
        $header = $request->getHeaderLine('Authorization');
        if (strpos($header, 'Basic') !== 0) {
            return false;
        }
        $header = explode(':', base64_decode(substr($header, 6)), 2);
        $authorization = [
            'username' => $header[0],
            'password' => isset($header[1]) ? $header[1] : null,
        ];
        $userInfo = explode(':', $request->getUri()->getUserInfo());
        $userInfo = [
            'username' => $userInfo[0],
            'password' => isset($userInfo[1]) ? $userInfo[1] : null
        ];
        return (! $authorization || $authorization['password'] !== $userInfo['password']) ? false : $authorization;
    }

    /**
     * Get XAuthKey from header
     *
     * @return array $useraccount
    */
    private static function getXAuthKey($request)
    {
        $xAuthKey = $request->getHeaderLine('X-AuthKey');
        if (! $xAuthKey) {
            $cookies = $request->getCookieParams();
            $xAuthKey = (array_key_exists('Zmsclient', $cookies)) ? $cookies['Zmsclient'] : null;
        }
        return ($xAuthKey) ? $xAuthKey : false;
    }

    /**
     * Get XApiKey from header
     *
     * @return array $useraccount
     */
    private static function getXApiKey($request)
    {
        $xApiKey = $request->getHeaderLine('x-api-key');
        if ($xApiKey) {
            return $xApiKey;
        }
        return false;
    }
}
