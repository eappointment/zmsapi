<?php
/**
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmsapi\Helper;

use BO\Zmsapi\BaseController;
use BO\Zmsdb\MaintenanceSchedule as Repository;
use BO\Zmsentities\Exception\SchemaValidation;
use BO\Zmsentities\MaintenanceSchedule as ScheduleEntity;
use Cron\CronExpression;
use League\JsonGuard\ValidationError;
use Psr\Container\ContainerInterface;

class Maintenance
{
    const MAINTENANCE_DELAY_SECONDS = 1800;

    /**
     * @var Repository
     */
    private $repository;

    /**
     * @var false|null|ScheduleEntity
     */
    private $activeEntity = false;

    public function __construct(?ContainerInterface $container = null)
    {
        $this->repository = new Repository();
        if ($container && $container->has('zmsdb.repositories.maintenance-schedule')) {
            $this->repository = $container->get('zmsdb.repositories.maintenance-schedule');
        }
    }

    public function getActiveEntity(): ?ScheduleEntity
    {
        if ($this->activeEntity === false) {
            $this->activeEntity = $this->repository->readActiveEntity();
        }

        return $this->activeEntity;
    }

    /**
     * @throws SchemaValidation
     */
    public static function testValidSchedule(ScheduleEntity $scheduleEntity): void
    {
        if ($scheduleEntity->isRepetitive()) {
            try {
                new CronExpression($scheduleEntity->getTimeString());
            } catch (\Exception $exception) {
                $msg   = 'Der angegebene Wert entspricht nicht der Crontab Syntax oder wird nicht unterstützt';
                $msg  .= ' ( numerische Werte und , oder / ).';
                $error = new ValidationError(
                    $msg,
                    'type',
                    ['string'],
                    $scheduleEntity->getTimeString(),
                    '/timeString',
                    new \stdClass(),
                    '/properties/timeString/type'
                );

                throw (new SchemaValidation())->setValidationError([$error]);
            }
        } elseif (!preg_match('#^\d{4}-\d{2}-\d{2} \d{2}:\d{2}(:\d{2})?$#', $scheduleEntity->getTimeString())) {
            $error = new ValidationError(
                'Der Wert muss das Format JJJJ-MM-TT hh:mm[:ss] einhalten',
                'type',
                ['string'],
                $scheduleEntity->getTimeString(),
                '/timeString',
                new \stdClass(),
                '/properties/timeString/type'
            );

            throw (new SchemaValidation())->setValidationError([$error]);
        }
    }

    /**
     * @SuppressWarnings(CyclomaticComplexity)
     * @SuppressWarnings(NPathComplexity)
     */
    public function isBlockedController(BaseController $controller): bool
    {
        $effectTs = \App::getNow()->getTimestamp() - self::MAINTENANCE_DELAY_SECONDS;
        if (!$this->getActiveEntity()
            || $this->getActiveEntity()->getStartDateTime()->getTimestamp() > $effectTs
        ) {
            return false;
        }

        $controllerName = get_class($controller);

        if (strpos($controllerName, 'Maintenance') !== false
            || strpos($controllerName, 'MailDelete') !== false
            || strpos($controllerName, 'NotificationDelete') !== false
            || strpos($controllerName, 'ProcessLog') !== false
            || strpos($controllerName, 'Session') !== false
            || strpos($controllerName, 'UseraccountUpdate') !== false
            || strpos($controllerName, 'WorkstationPassword') !== false
            || strpos($controllerName, 'WorkstationProcessGet') !== false
            || strpos($controllerName, 'WorkstationDelete') !== false
        ) {
            return false;
        }

        if (strpos($controllerName, 'WorkstationProcess') !== false
            || strpos($controllerName, 'Add') !== false
            || strpos($controllerName, 'Delete') !== false
            || strpos($controllerName, 'Update') !== false
            || strpos($controllerName, 'Hash') !== false
            || strpos($controllerName, 'Confirm') !== false
            || strpos($controllerName, 'ProcessFinish') !== false
            || strpos($controllerName, 'ProcessFree') !== false
            || strpos($controllerName, 'ProcessPickup') !== false
            || strpos($controllerName, 'ProcessQueued') !== false
            || strpos($controllerName, 'ProcessReserve') !== false
            || strpos($controllerName, 'SummaryMail') !== false
            || strpos($controllerName, 'TicketprinterWaitingnumber') !== false
            || strpos($controllerName, 'VerifyMail') !== false
            || strpos($controllerName, 'WorkstationProcess') !== false
        ) {
            return true;
        }

        return false;
    }
}
