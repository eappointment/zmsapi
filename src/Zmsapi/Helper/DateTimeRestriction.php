<?php

/**
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi\Helper;

use BO\Zmsdb\ProcessStatusFree;
use BO\Zmsentities\Calendar as CalendarEntity;
use BO\Zmsentities\Collection\DayList;
use BO\Zmsentities\Collection\ProcessList;
use BO\Zmsentities\Day;
use BO\Zmsentities\Helper\TimeRestriction as TimeRestrictionHelper;

/**
 * @suppressWarnings(complexity)
 * @suppressWarnings(TooManyPublicMethods)
 */
class DateTimeRestriction
{
    /**
      * @var TimeRestrictionHelper $timeRestriction
      */
    protected TimeRestrictionHelper $timeRestriction;

    public function __construct(string $timeRestriction = '')
    {
        $this->timeRestriction = new TimeRestrictionHelper($timeRestriction);
    }

    /**
     *
     * The result is day list whose days are marked as restricted that are full or not bookable by time restriction.
     *
     * @param CalendarEntity $calendar
     * @param string $slotType
     * @param int $slotsRequired
     * @return DayList
     */
    public function getRestrictedDayList(
        CalendarEntity $calendar,
        string $slotType = 'public',
        int $slotsRequired = 0
    ): DayList {
        $restrictedDayList = new DayList();
        foreach ($calendar->getDayList() as $day) {
            if ($this->timeRestriction->isAllowedDay($day) === false) {
                $day->status = Day::RESTRICTED;
            }
            $restrictedDayList->addCachedEntity($day);
        }

        $bookableDayList = clone $calendar->getDayList();
        while ($day = $bookableDayList->getFirstBookableDay()) {
            $calendar->setFirstDayTime($day->toDateTime());
            $calendar->setLastDayTime($day->toDateTime());
            $processList = (new ProcessStatusFree())->readFreeProcesses(
                $calendar,
                $day->toDateTime(),
                $slotType,
                $slotsRequired
            );
            $freeProcessList = $this->getRestrictedProcessList($processList);
            if ($this->timeRestriction->isAllowedDay($day) === false
                || ($processList->count() > 0 && $freeProcessList->count() === 0)
            ) {
                $listDay = $restrictedDayList->getDayByDateTime($day->toDateTime()); // todo:check if necessary
                $listDay->status = Day::RESTRICTED;
            }
            $bookableDayList->removeEntity($day);
        }

        return $restrictedDayList;
    }

    /**
     *
     * the result only contains the list of bookable processes within the current time restriction
     *
     * @param ProcessList $processList
     * @return ProcessList
     */
    public function getRestrictedProcessList(ProcessList $processList): ProcessList
    {
        $restrictedList = new ProcessList();
        foreach ($processList as $process) {
            if ($this->timeRestriction->isAllowed($process->getFirstAppointment()->toDateTime()->getTimestamp())) {
                $restrictedList->addEntity($process);
            }
        }
        return $restrictedList;
    }
}
