<?php
/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use BO\Slim\Render;
use BO\Mellon\Validator;
use BO\Zmsdb\Ticketprinter as TicketprinterRepo;
use BO\Zmsdb\Organisation as Query;
use BO\Zmsentities\Ticketprinter;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class OrganisationHash extends BaseController
{
    /** @var TicketprinterRepo */
    protected $ticketprinterRepo;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->ticketprinterRepo = new TicketprinterRepo();
        if ($container->has('zmsdb.repositories.ticketprinter')) {
            $this->ticketprinterRepo = $container->get('zmsdb.repositories.ticketprinter');
        }
    }

    /**
     * @SuppressWarnings(Param)
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        \BO\Zmsdb\Connection\Select::getWriteConnection();
        $organisation = (new Query())->readEntity($args['id']);
        if (!$organisation) {
            throw new Exception\Organisation\OrganisationNotFound();
        }

        /** @var Validator $validator */
        $validator = $request->getAttribute('validator');
        $existingId = $validator->getParameter('sid')->isString()->isRequired()->getValue();
        $ticketprinter = (new Ticketprinter())->getHashWith($organisation->getId());//new ticketprinter with random hash
        $ticketprinter->name = $validator->getParameter('name')->isString()->setDefault('')->getValue();
        $ticketprinter->hash = $existingId ?? $ticketprinter->hash;

        $existingInst = $existingId ? $this->ticketprinterRepo->readByHash($existingId) : null;
        $existingInst = $existingInst && $existingInst->getId() ? $existingInst : null;
        $ticketprinter = $existingInst ?? $this->ticketprinterRepo->writeEntityWithHash($ticketprinter, $organisation);

        $message = Response\Message::create($request);
        $message->data = $ticketprinter;

        $response = Render::withLastModified($response, time(), '0');
        $response = Render::withJson($response, $message->setUpdatedMetaData(), $message->getStatuscode());

        return $response;
    }
}
