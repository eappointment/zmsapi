<?php

declare(strict_types=1);

namespace BO\Zmsapi;

use BO\Slim\Render;
use BO\Zmsapi\Exception\CheckInConfig\EntryNotFound;
use BO\Zmsapi\Helper\User;
use BO\Zmsdb\CheckInConfig as Repository;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class CheckInConfigGet extends BaseController
{
    /**
     * @var Repository
     */
    private $repository;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->repository = new Repository();
        if ($container->has('zmsdb.repositories.check-in-config')) {
            $this->repository = $container->get('zmsdb.repositories.check-in-config');
        }
    }

    /**
     * @throws EntryNotFound
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        try {
            (new User($request))->checkRights('basic');
        } catch (\Exception $exception) {
            $token = $request->getHeader('X-Token');
            if (\App::SECURE_TOKEN != current($token)) {
                throw new Exception\Config\ConfigAuthentificationFailed();
            }
        }

        $entity = $this->repository->readById($args['id']);
        if ($entity === null) {
            throw new EntryNotFound();
        }

        $message = Response\Message::create($request);
        $message->data = $entity;

        $response = Render::withLastModified($response, time());
        return Render::withJson($response, $message);
    }
}
