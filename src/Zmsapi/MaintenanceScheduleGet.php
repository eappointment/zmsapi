<?php

/**
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmsapi;

use App;
use BO\Mellon\Validator;
use BO\Slim\Render;
use BO\Zmsapi\Exception\Useraccount\InvalidCredentials;
use BO\Zmsdb\MaintenanceSchedule as Repository;
use BO\Zmsentities\Permission;
use Exception;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @suppressWarnings(coupling)
 */
class MaintenanceScheduleGet extends BaseController
{
    /**
     * @var Repository
     */
    private Repository $repository;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->repository = new Repository();
        if ($container->has('zmsdb.repositories.maintenance-schedule')) {
            $this->repository = $container->get('zmsdb.repositories.maintenance-schedule');
        }
    }

    /**
     * @SuppressWarnings(Param)
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {

        try {
            (new Helper\User($request))->checkRights(
                Permission::RIGHT_LOGIN
            );
        } catch (Exception) {
            $token = $request->getHeader('X-Token');
            if (App::SECURE_TOKEN != current($token)) {
                throw new InvalidCredentials();
            }
        }

        $activeRequested = str_ends_with($request->getUri()->getPath(), '/active/');
        $identifier      = array_key_exists('id', $args) ? Validator::value($args['id'])->isNumber()->getValue() : null;

        if (!$activeRequested && ($identifier === null || $identifier === 0)) {
            return $response->withStatus(StatusCodeInterface::STATUS_BAD_REQUEST, 'HTTP Bad Request');
        } elseif ($activeRequested) {
            $scheduleEntry = $this->repository->readActiveEntity();
        } else {
            $scheduleEntry = $this->repository->readEntity($identifier);
        }

        if ($scheduleEntry === null) {
            return $response->withStatus(StatusCodeInterface::STATUS_NOT_FOUND, 'HTTP Not Found');
        }

        $message = Response\Message::create($request);
        $message->data = $scheduleEntry;

        $response = Render::withLastModified($response, time());
        return Render::withJson($response, $message->setUpdatedMetaData());
    }
}
