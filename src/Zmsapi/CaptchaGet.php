<?php

/**
 *
 * @package Zmsappointment
 * @copyright BerlinOnline GmbH
 *
 */

namespace BO\Zmsapi;

use App;
use BO\Slim\Render;
use BO\Zmsentities\Apikey as ApikeyEntity;
use Gregwar\Captcha\CaptchaBuilder;
use Gregwar\Captcha\PhraseBuilder;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use BO\Slim\Request as SlimRequest;
use RuntimeException;

/**
 * @SuppressWarnings(CyclomaticComplexity)
 * @SuppressWarnings(NPathComplexity)
 */
class CaptchaGet extends BaseController
{
    public const CAPTCHA_LENGTH_MIN = 5;
    public const CAPTCHA_LENGTH_MAX = 6;
    public const CAPTCHA_HEIGHT = 250;
    public const CAPTCHA_WIDTH  = 500;
    public const CAPTCHA_LINES_MAX_FRONT = 10;
    public const CAPTCHA_LINES_MAX_BEHIND = 15;
    public const CAPTCHA_BACKGROUND = ['r' => 245, 'g' => 245, 'b' => 245];
    public const CAPTCHA_ALPHABET = "BbCcDdEeFfGHhJkKLMmNnPQRrTtUuVvWwXxYyZz3678";

    protected $lengthRand = 5;

    protected $height = 250;

    protected $width = 500;

    protected $linesMaxFront = 10;

    protected $linesMaxBehind = 15;

    protected $background = [];

    /**
     * @SuppressWarnings(UnusedFormalParameter)
     * show captcha to verify human
     *
     * @param RequestInterface|SlimRequest $request
     * @param ResponseInterface $response
     * @param array $args
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $this->setFormatOptions($request);
        $captchaBuilder = (new CaptchaBuilder(
            null,
            new PhraseBuilder($this->lengthRand, self::CAPTCHA_ALPHABET)
        ))
            ->setBackgroundColor(
                $this->background['r'],
                $this->background['g'],
                $this->background['b']
            )
            ->setMaxBehindLines($this->linesMaxBehind)
            ->setMaxFrontLines($this->linesMaxFront)
            ->build(
                $this->width,
                $this->height,
                $this->getRandomFont()
            );

        $apiKey = new ApikeyEntity();
        $apiKey->key = $apiKey->getHash(strtolower($captchaBuilder->getPhrase()), App::SECURE_TOKEN);
        $apiKey->withCaptchaData($captchaBuilder->inline());

        $message = Response\Message::create($request);
        $message->data = $apiKey;

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message->setUpdatedMetaData(), $message->getStatuscode());
    }

    /**
     * @return string|null
     */
    private function getRandomFont(): ?string
    {
        $dirContent = array_values(array_filter(
            scandir(realpath(APP_PATH . App::$fontPath) . '/'),
            function ($filename) {
                return substr($filename, -4) === '.ttf';
            }
        ));

        if (count($dirContent) === 0) {
            throw new RuntimeException('Failure on finding a font for the Captcha.');
        }

        $path = realpath(APP_PATH . App::$fontPath) . '/' . $dirContent[mt_rand(0, count($dirContent) - 1)];

        //Random use of fonts from the captcha library
        $randomCheck = rand(1, 10);
        return ($randomCheck % 2 == 0) ? $path : null;
    }

    private function setFormatOptions($request): void
    {
        $validator = $request->getAttribute("validator");
        $formatOptions = $validator->getParameter('format')->isArray()->getValue();
        $this->lengthRand = (isset($formatOptions['length']) && $formatOptions['length'] > 3) ?
            $formatOptions['length'] :
            rand(self::CAPTCHA_LENGTH_MIN, self::CAPTCHA_LENGTH_MAX);

        $this->background = (
            isset($formatOptions['background']['r']) &&
            isset($formatOptions['background']['g']) &&
            isset($formatOptions['background']['b'])
        ) ?
            $formatOptions['background'] :
            self::CAPTCHA_BACKGROUND;

        $this->width = (isset($formatOptions['width']) && $formatOptions['width'] > 100) ?
            $formatOptions['width'] :
            self::CAPTCHA_WIDTH;

        $this->height = (isset($formatOptions['height']) && $formatOptions['height'] > 100) ?
            $formatOptions['height'] :
            self::CAPTCHA_HEIGHT;

        $this->linesMaxFront = (isset($formatOptions['linesMaxFront']) && $formatOptions['linesMaxFront'] > 5) ?
            $formatOptions['linesMaxFront'] :
            self::CAPTCHA_LINES_MAX_FRONT;

        $this->linesMaxBehind = (isset($formatOptions['linesMaxBehind']) && $formatOptions['linesMaxBehind'] > 5) ?
            $formatOptions['linesMaxBehind'] :
            self::CAPTCHA_LINES_MAX_BEHIND;
    }
}
