<?php

/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use App;
use BO\Slim\Render;
use BO\Zmsapi\Exception\Process\AuthKeyMatchFailed;
use BO\Zmsapi\Exception\Process\ProcessNotFound;
use BO\Zmsdb\Exception\Pdo\DeadLockFound;
use BO\Zmsdb\Exception\Pdo\LockTimeout;
use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsdb\Process;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @SuppressWarnings(Coupling)
 */
class ProcessReservationExtension extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @SuppressWarnings(Complexity)
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     * @throws AuthKeyMatchFailed
     * @throws ProcessNotFound
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws PDOFailed
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $validator = $request->getAttribute('validator');
        $resolveReferences = $validator::param('resolveReferences')->isNumber()->setDefault(1)->getValue();

        $this->testProcessData($args['id'], $args['authKey']);
        $processRepository = new Process();
        $processRepository->updateReservationDuration($args['id'], $args['authKey'], App::$now);

        $message = Response\Message::create($request);
        $message->data = $processRepository->readEntity($args['id'], $args['authKey'], $resolveReferences);

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message->setUpdatedMetaData(), $message->getStatuscode());
    }

    /**
     * @param $processId
     * @param $authKey
     *
     * @return void
     * @throws AuthKeyMatchFailed
     * @throws ProcessNotFound
     */
    protected function testProcessData($processId, $authKey)
    {
        $authCheck = (new Process())->readAuthKeyByProcessId($processId);
        if (! $authCheck) {
            throw new Exception\Process\ProcessNotFound();
        } elseif ($authCheck['authKey'] != $authKey && $authCheck['authName'] != $authKey) {
            throw new Exception\Process\AuthKeyMatchFailed();
        }
    }
}
