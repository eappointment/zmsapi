<?php
/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use App;
use BO\Slim\Render;

use BO\Zmsapi\Exception\Mail\MailSenderFromMissing;
use BO\Zmsapi\Exception\Process\AuthKeyMatchFailed as AuthKeyMatchFailedException;
use BO\Zmsapi\Exception\Process\EmailRequired as EmailRequiredException;
use BO\Zmsapi\Exception\Process\ProcessNotFound as ProcessNotFoundException;

use BO\Zmsdb\Connection\Select as DBConnection;
use BO\Zmsdb\Exception\Mail\ClientWithoutEmail;
use BO\Zmsdb\Helper\MailService;
use BO\Zmsdb\Mail as MailRepository;
use BO\Zmsdb\Process as ProcessRepository;
use BO\Zmsdb\Config as ConfigRepository;
use BO\Zmsdb\Department as DepartmentRepository;

use BO\Zmsentities\Exception\TemplateNotFound;
use BO\Zmsentities\Process as ProcessEntity;
use BO\Zmsentities\Collection\ProcessList as Collection;
use BO\Zmsentities\Mail as MailEntity;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @SuppressWarnings(Coupling)
 */
class ProcessConfirmationMail extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     * @throws AuthKeyMatchFailedException
     * @throws ClientWithoutEmail
     * @throws EmailRequiredException
     * @throws MailSenderFromMissing
     * @throws ProcessNotFoundException
     * @throws TemplateNotFound
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        DBConnection::setCriticalReadSession();

        $validator = $request->getAttribute('validator');
        $resolveReferences = $validator::param('resolveReferences')->isNumber()->setDefault(3)->getValue();
        $input = $validator::input()->isJson()->assertValid()->getValue();
        $process = new ProcessEntity($input);
        $process->testValid();
        $this->testProcessData($process);

        $process = (new ProcessRepository())->readEntity($process->getId(), $process->getAuthKey(), $resolveReferences);
        $message = Response\Message::create($request);

        $mail = $this->writeMail($process);
        $message->data = $mail ?: new MailEntity();

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message->setUpdatedMetaData(), $message->getStatuscode());
    }

    /**
     * @param ProcessEntity $process
     *
     * @return MailEntity|null
     * @throws ClientWithoutEmail
     * @throws MailSenderFromMissing
     * @throws TemplateNotFound
     */
    protected static function writeMail(ProcessEntity $process): ?MailEntity
    {
        $mailService = new MailService(new MailRepository(), new ConfigRepository(), new DepartmentRepository());
        $mailService->setProcessList(static::getProcessListOverview($process));
        $mailType = ($process->isWithAppointment()) ?
            $mailService::MAIL_TYPE_APPOINTMENT :
            $mailService::MAIL_TYPE_SPONTANEOUS;
        $mail = $mailService->readResolvedEntity($mailType);

        if ($process->scope->hasEmailFrom()) {
            $mail = $mailService->writeMailInQueue($mail, App::$now, false);
        }
        return $mail;
    }

    /**
     * @throws ProcessNotFoundException
     * @throws EmailRequiredException
     * @throws AuthKeyMatchFailedException
     */
    protected function testProcessData($process)
    {
        $authCheck = (new ProcessRepository())->readAuthKeyByProcessId($process->id);
        if (! $authCheck) {
            throw new ProcessNotFoundException();
        } elseif ($authCheck['authKey'] != $process->authKey && $authCheck['authName'] != $process->authKey) {
            throw new AuthKeyMatchFailedException();
        } elseif ($process->toProperty()->scope->preferences->client->emailRequired->get() &&
            ! $process->getFirstClient()->hasEmail()
        ) {
            throw new EmailRequiredException();
        }
    }

    public static function getProcessListOverview($process): Collection
    {
        $collection  = (new Collection())->addEntity($process);
        if (in_array(
            getenv('ZMS_ENV'),
            explode(',', (new ConfigRepository())->readProperty('appointments__enableSummaryByMail'))
        ) && $process->getFirstClient()->hasEmail()
        ) {
            $processList = (new ProcessRepository())->readListByMailAndStatusList(
                $process->getFirstClient()->email,
                [
                    ProcessEntity::STATUS_CONFIRMED,
                    ProcessEntity::STATUS_PICKUP
                ],
                2,
                50
            );
            //add list of found processes without the main process
            $collection->addList($processList->withOutProcessId($process->getId()));
        }
        return $collection->withoutExpiredAppointmentDate(App::$now);
    }
}
