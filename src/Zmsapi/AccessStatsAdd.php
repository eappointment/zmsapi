<?php
/**
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmsapi;

use BO\Mellon\Unvalidated;
use BO\Mellon\ValidJson;
use BO\Slim\Render;
use BO\Zmsdb\AccessStats as Repository;
use BO\Zmsentities\AccessStats;
use BO\Zmsentities\Useraccount;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class AccessStatsAdd extends BaseController
{
    /**
     * @var Repository
     */
    private $repository;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->repository = new Repository();
        if ($container->has('zmsdb.repositories.access-stats')) {
            $this->repository = $container->get('zmsdb.repositories.access-stats');
        }
    }

    /**
     * @SuppressWarnings(Param)
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ) {
        Helper\User::$request = $request;

        /** @var Unvalidated|ValidJson $validatorInput */
        $validatorInput = $request->getAttribute('validator')->getInput();
        $input = $validatorInput->isJson()->assertValid()->getValue();
        $entity = new AccessStats($input);
        $entity->testValid();

        $this->repository->writeEntity($entity);

        $message = Response\Message::create($request);
        $message->data = $entity;

        $response = Render::withLastModified($response, time())
            ->withStatus(StatusCodeInterface::STATUS_CREATED, 'stats entry created');
        return Render::withJson($response, $message);
    }
}
