<?php

declare(strict_types=1);

namespace BO\Zmsapi;

use BO\Mellon\Unvalidated;
use BO\Mellon\Validator;
use BO\Mellon\ValidJson;
use BO\Slim\Render;
use BO\Zmsapi\Exception\CheckInConfig\EntryNotFound;
use BO\Zmsdb\CheckInConfig as Repository;
use BO\Zmsentities\CheckInConfig as ConfigEntity;
use BO\Zmsentities\EventLog as EventLogEntity;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class CheckInConfigUpdate extends CheckInConfigAdd
{
    /**
     * @throws EntryNotFound
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $this->user = new Helper\User($request);
        $this->user->checkRights('scope');

        /** @var Unvalidated|ValidJson $validatorInput */
        $validatorInput = $request->getAttribute('validator')->getInput();
        $input = $validatorInput->isJson()->assertValid()->getValue();

        $entity = new ConfigEntity($input);
        $entity->testValid();

        Validator::value($args['id'])->isEqualTo($entity->getId())->assertValid();

        try {
            $this->repository->update($entity, 0);
        } catch (\Exception $e) {
            /*
            return $response->withStatus(
                StatusCodeInterface::STATUS_INTERNAL_SERVER_ERROR,
                'error on persisting the entity data'
            );
            */
        }

        $this->addEventLogEntry(EventLogEntity::CHECKIN_CONFIG_CHANGED, $entity);

        $message = Response\Message::create($request);
        $message->data = $entity;

        $response = Render::withLastModified($response, time())
            ->withStatus(StatusCodeInterface::STATUS_OK, 'config entry created');

        return Render::withJson($response, $message);
    }
}
