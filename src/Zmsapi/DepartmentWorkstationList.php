<?php
/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use BO\Slim\Render;
use BO\Zmsapi\Exception\PreConditionFailed;
use BO\Zmsdb\Workstation;
use BO\Zmsentities\Permission;
use Fig\Http\Message\StatusCodeInterface;
use BO\Zmsentities\Exception\UserAccountMissingDepartment;
use BO\Zmsentities\Exception\UseraccountMissingLogin;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class DepartmentWorkstationList extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @param array $args
     * @return ResponseInterface
     * @throws PreConditionFailed
     * @throws UserAccountMissingDepartment
     * @throws UseraccountMissingLogin
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        (new Helper\User($request))->checkRights(
            Permission::RIGHT_USERACCOUNT
        );

        $validator = $request->getAttribute('validator');
        $resolveReferences = $validator->getParameter('resolveReferences')->isNumber()->setDefault(1)->getValue();
        if (!isset($args['id'])) {
            throw new PreConditionFailed();
        }
        $departmentId = $validator::value($args['id'])->isNumber()->getValue();
        $department = Helper\User::checkDepartment($departmentId);

        $workstationList = (new Workstation())->readCollectionByDepartmentId($department->id, $resolveReferences);
        $message = Response\Message::create($request);
        $message->data = $workstationList;

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message);
    }
}
