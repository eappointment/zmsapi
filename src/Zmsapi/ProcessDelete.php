<?php
/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use App;
use BO\Slim\Render;

use BO\Zmsapi\Exception\Process\AuthKeyMatchFailed;
use BO\Zmsapi\Exception\Process\ProcessDeleteFailed;
use BO\Zmsapi\Exception\Process\ProcessExpired;
use BO\Zmsapi\Exception\Process\ProcessNotFound;
use BO\Zmsdb\Exception\Pdo\DeadLockFound;
use BO\Zmsdb\Exception\Pdo\LockTimeout;
use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsdb\Exception\Process\ProcessArchiveUpdateFailed;

use BO\Zmsentities\Process as ProcessEntity;
use BO\Zmsentities\Collection\ProcessList;

use BO\Zmsdb\Config as ConfigRepository;
use BO\Zmsdb\Connection\Select;
use BO\Zmsdb\Helper\MailService;
use BO\Zmsdb\Helper\NoAuth;
use BO\Zmsdb\Mail as MailRepository;
use BO\Zmsdb\Process as ProcessRepository;

use BO\Zmsentities\Exception\TemplateNotFound;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @SuppressWarnings(CouplingBetweenObjects)
 */
class ProcessDelete extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     * @throws AuthKeyMatchFailed
     * @throws ProcessDeleteFailed
     * @throws ProcessNotFound
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws PDOFailed
     * @throws ProcessArchiveUpdateFailed
     * @throws TemplateNotFound
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        Select::getWriteConnection();

        $validator = $request->getAttribute('validator');
        $processId = $validator::value($args['id'])->isNumber()->getValue();
        //resolve to deep of 2 to get resolved provider data
        $process = (new ProcessRepository())->readEntity($processId, new NoAuth(), 2);
        $this->testProcessData($process, $args['authKey']);

        if ($process->status == ProcessEntity::STATUS_RESERVED) {
            if (!(new ProcessRepository())->writeBlockedEntity($process)) {
                throw new Exception\Process\ProcessDeleteFailed(); // @codeCoverageIgnore
            }
            $processDeleted = $process;
        } else {
            $processDeleted = (new ProcessRepository())->writeCanceledEntity($processId, $args['authKey']);
            if (! $processDeleted || ! $processDeleted->hasId()) {
                throw new Exception\Process\ProcessDeleteFailed(); // @codeCoverageIgnore
            }
        }

        $this->writeMails($request, $process);
        $message = Response\Message::create($request);
        $message->data = $processDeleted;

        $response = Render::withLastModified($response, time(), '0');
        $response = Render::withJson($response, $message->setUpdatedMetaData(), $message->getStatuscode());
        return $response;
    }

    /**
     * @throws TemplateNotFound
     */
    protected function writeMails($request, $process)
    {
        if ($process->hasScopeAdmin() && $process->status != ProcessEntity::STATUS_RESERVED) {
            $authority = $request->getUri()->getAuthority();
            $validator = $request->getAttribute('validator');
            $initiator = $validator->getParameter('initiator')->isString()->getValue();
            $mailService = new MailService(new MailRepository(), new ConfigRepository());
            $mailService->setInitiator($initiator ?? "$authority API-User");
            $mailService->setProcessList((new ProcessList())->addEntity($process));
            $mail = $mailService->readResolvedEntity($mailService::MAIL_TYPE_DELETE);
            $mailService->writeInQueueWithAdmin($mail);
        }
    }

    /**
     * @throws ProcessNotFound
     * @throws ProcessExpired
     * @throws AuthKeyMatchFailed
     */
    protected function testProcessData($process, $authKey)
    {
        if (! $process) {
            throw new Exception\Process\ProcessNotFound();
        }
        if ($process->getFirstAppointment()->date < App::$now->getTimestamp()) {
            throw new Exception\Process\ProcessExpired();
        }

        $authName = $process->getFirstClient()['familyName'];
        if ($process['authKey'] != $authKey && $authName != $authKey) {
            throw new Exception\Process\AuthKeyMatchFailed();
        }
    }
}
