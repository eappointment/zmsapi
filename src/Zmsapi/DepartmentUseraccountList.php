<?php
/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use App;
use BO\Slim\Render;
use BO\Zmsapi\Exception\PreConditionFailed;
use BO\Zmsdb\Useraccount as Query;
use BO\Zmsentities\Exception\UserAccountMissingDepartment;
use BO\Zmsentities\Exception\UseraccountMissingLogin;
use BO\Zmsentities\Permission;
use BO\Zmsentities\Useraccount;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class DepartmentUseraccountList extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     *
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @param array $args
     * @return ResponseInterface
     * @throws UserAccountMissingDepartment
     * @throws UseraccountMissingLogin
     * @throws PreConditionFailed
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        (new Helper\User($request, 1))->checkRights(Permission::RIGHT_USERACCOUNT);

        $validator = $request->getAttribute('validator');
        $resolveReferences = $validator->getParameter('resolveReferences')->isNumber()->setDefault(1)->getValue();
        if (!isset($args['id'])) {
            throw new PreConditionFailed();
        }
        $departmentId = $validator::value($args['id'])->isNumber()->getValue();
        $department = Helper\User::checkDepartment($departmentId);

        /** @var Useraccount $userAccount */
        $userAccountList = (new Query())->readCollectionByDepartmentId(
            $department->getId(),
            $resolveReferences,
            App::$now
        );
        foreach ($userAccountList as $userAccount) {
            if ($resolveReferences < 1 && !$userAccount->getDepartmentById($department->getId())) {
                // add at least the department by which the list was requested
                $userAccount->getDepartmentList()->addEntity($department);
            }
        }

        $message = Response\Message::create($request);
        $message->data = $userAccountList;

        $response = Render::withLastModified($response, time(), '0');

        return Render::withJson($response, $message, StatusCodeInterface::STATUS_OK);
    }
}
