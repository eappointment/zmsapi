<?php
/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use BO\Slim\Render;

use BO\Zmsdb\Config as ConfigRepository;
use BO\Zmsdb\Helper\MailService;
use BO\Zmsdb\Mail as MailRepository;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class MailDelete extends MailAdd
{
    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        (new Helper\User($request))->checkRights('superuser');
        $validator = $request->getAttribute('validator');
        $mailId = $validator::value($args['id'])->isNumber()->getValue();

        $message = Response\Message::create($request);
        $message->data = $this->mailService->writeMailDelete($mailId);

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message->setUpdatedMetaData());
    }
}
