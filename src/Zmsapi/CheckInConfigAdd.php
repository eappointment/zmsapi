<?php

declare(strict_types=1);

namespace BO\Zmsapi;

use BO\Mellon\Unvalidated;
use BO\Mellon\ValidJson;

use BO\Slim\Render;
use BO\Zmsapi\Helper\User;
use BO\Zmsdb\CheckInConfig as Repository;
use BO\Zmsdb\EventLog as EventLogRepo;
use BO\Zmsentities\CheckInConfig as ConfigEntity;
use BO\Zmsentities\EventLog as EventLogEntity;
use BO\Zmsentities\Exception\SchemaValidation;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @SuppressWarnings(CouplingBetweenObjects)
 */
class CheckInConfigAdd extends BaseController
{
    /** @var User */
    protected $user;
    /**
     * @var Repository
     */
    protected $repository;

    /** @var EventLogRepo */
    protected $eventLogRepo;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->repository = new Repository();
        if ($container->has('zmsdb.repositories.check-in-config')) {
            $this->repository = $container->get('zmsdb.repositories.check-in-config');
        }

        $this->eventLogRepo = new EventLogRepo();
        if ($container->has('zmsdb.repositories.check-in-config')) {
            $this->repository = $container->get('zmsdb.repositories.check-in-config');
        }
    }

    /**
     * @throws SchemaValidation
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $this->user = new Helper\User($request);
        $this->user->checkRights('scope');

        /** @var Unvalidated|ValidJson $validatorInput */
        $validatorInput = $request->getAttribute('validator')->getInput();
        $input = $validatorInput->isJson()->assertValid()->getValue();

        $entity = new ConfigEntity($input);
        $entity->testValid();

        try {
            $this->repository->persist($entity);
        } catch (\Exception $e) {
            return $response->withStatus(
                StatusCodeInterface::STATUS_INTERNAL_SERVER_ERROR,
                'error on persisting the entity data'
            );
        }

        $this->addEventLogEntry(EventLogEntity::CHECKIN_CONFIG_ADDED, $entity);

        $message = Response\Message::create($request);
        $message->data = $entity;

        $response = Render::withLastModified($response, time())
            ->withStatus(StatusCodeInterface::STATUS_CREATED, 'config entry created');

        return Render::withJson($response, $message);
    }

    protected function addEventLogEntry(string $name, ConfigEntity $configEntity): void
    {
        $eventLogEntity = new EventLogEntity();
        $eventLogEntity->addData([
            'name' => $name,
            'origin' => 'zmsapi',
            'referenceType' => 'checkinconfig.id',
            'reference' => $configEntity->getId(),
            'context' => ['user' => ['id' => $this->user::readWorkstation()->getId()]],
        ])->setSecondsToLive(EventLogEntity::LIVETIME_MONTH);

        try {
            $this->eventLogRepo->writeEntity($eventLogEntity);
        } catch (\Exception $e) {
            return;
        }
    }
}
