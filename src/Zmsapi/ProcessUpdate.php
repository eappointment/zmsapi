<?php
/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use App;
use BO\Slim\Render;

use BO\Zmsapi\Exception\Apikey\AccessDenied;
use BO\Zmsapi\Exception\Apikey\ApiKeyLocked;
use BO\Zmsapi\Exception\Apikey\ApiKeyNotValid;
use BO\Zmsapi\Exception\Matching\RequestNotFound;
use BO\Zmsapi\Exception\Process\ApiclientInvalid;
use BO\Zmsapi\Exception\Process\AuthKeyMatchFailed;
use BO\Zmsapi\Exception\Process\ProcessNotFound;

use BO\Zmsapi\Helper\ApikeyService;
use BO\Zmsdb\Apikey as ApikeyRepository;
use BO\Zmsdb\Connection\Select as DBConnection;
use BO\Zmsdb\Apiclient as ApiclientRepository;
use BO\Zmsdb\Config as ConfigRepository;
use BO\Zmsdb\Exception\Pdo\DeadLockFound;
use BO\Zmsdb\Exception\Pdo\LockTimeout;
use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsdb\Exception\Process\ProcessArchiveUpdateFailed;
use BO\Zmsdb\Exception\Process\ProcessUpdateFailed;
use BO\Zmsdb\Helper\MailService;
use BO\Zmsdb\Mail as MailRepository;
use BO\Zmsdb\Process as ProcessRepository;

use BO\Zmsentities\Collection\ProcessList;
use BO\Zmsentities\Exception\TemplateNotFound;
use BO\Zmsentities\Process as ProcessEntity;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @SuppressWarnings(Coupling)
 * @return String
 */
class ProcessUpdate extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @SuppressWarnings(Complexity)
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     * @throws AuthKeyMatchFailed
     * @throws AccessDenied
     * @throws ApiKeyLocked
     * @throws ApiKeyNotValid
     * @throws ProcessNotFound
     * @throws ProcessUpdateFailed
     * @throws RequestNotFound
     * @throws TemplateNotFound
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws PDOFailed
     * @throws ProcessArchiveUpdateFailed
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $validator = $request->getAttribute('validator');
        $slotsRequired = $validator::param('slotsRequired')->isNumber()->getValue();
        $slotType = $validator::param('slotType')->isString()->getValue();
        $clientKey = $request->getHeaderLine('X-Api-Key');
        $initiator = $validator::param('initiator')->isString()->getValue();
        $resolveReferences = $validator::param('resolveReferences')->isNumber()->setDefault(2)->getValue();
        $input = $validator::input()->isJson()->assertValid()->getValue();

        $entity = new ProcessEntity($input);
        $entity->testValid();
        $this->testProcessData($entity);

        DBConnection::setCriticalReadSession();

        if ($slotType || $slotsRequired) {
            $workstation = (new Helper\User($request))->checkRights();
            $process = ProcessRepository::init()->updateEntityWithSlots(
                $entity,
                App::$now,
                $slotType,
                $slotsRequired,
                $resolveReferences,
                $workstation->getUseraccount()
            );
            Helper\Matching::testCurrentScopeHasRequest($process);
        } elseif ($clientKey) {
            $apikeyService = new ApikeyService($clientKey);
            $entity->apiclient = $apikeyService->getApiclient();
            $process = (new ProcessRepository())->updateEntity($entity, App::$now, $resolveReferences);
        } else {
            $process = (new ProcessRepository())->updateEntity($entity, App::$now, $resolveReferences);
        }
       
        if ($initiator && $process->hasScopeAdmin()) {
            $mailService = new MailService(new MailRepository(), new ConfigRepository());
            $mailService->setInitiator($initiator);
            $mailService->setProcessList((new ProcessList())->addEntity($process));
            $mail = $mailService->readResolvedEntity(MailService::MAIL_TYPE_UPDATE);
            $mailService->writeInQueueWithAdmin($mail);
        }

        $message = Response\Message::create($request);
        $message->data = $process;
        
        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message->setUpdatedMetaData(), $message->getStatuscode());
    }

    /**
     * @param $entity
     *
     * @throws AuthKeyMatchFailed
     * @throws ProcessNotFound
     */
    protected function testProcessData($entity)
    {
        $authCheck = (new ProcessRepository())->readAuthKeyByProcessId($entity->id);
        if (! $authCheck) {
            throw new Exception\Process\ProcessNotFound();
        } elseif ($authCheck['authKey'] != $entity->authKey && $authCheck['authName'] != $entity->authKey) {
            throw new Exception\Process\AuthKeyMatchFailed();
        }
    }
}
