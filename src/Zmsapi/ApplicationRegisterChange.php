<?php

declare(strict_types=1);

namespace BO\Zmsapi;

use BO\Mellon\Unvalidated;
use BO\Mellon\ValidJson;
use BO\Zmsapi\Exception\ApplicationRegister\EntryNotFound;
use BO\Zmsentities\Helper\ApplicationRegister as RegisterHelper;
use BO\Zmsdb\ApplicationRegister as Repository;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class ApplicationRegisterChange extends BaseController
{
    /**
     * @var Repository
     */
    private $repository;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->repository = new Repository();
        if ($container->has('zmsdb.repositories.application-register')) {
            $this->repository = $container->get('zmsdb.repositories.application-register');
        }
    }

    /**
     * @SuppressWarnings(Param)
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        (new Helper\User($request))->checkRights('scope');

        $entity = $this->repository->readEntity($args['id']);
        if ($entity === null) {
            throw new EntryNotFound();
        }

        /** @var Unvalidated|ValidJson $validatorInput */
        $validatorInput = $request->getAttribute('validator')->getInput();
        $input = $validatorInput->isJson()->assertValid()->getValue();

        foreach ($input as $key => $value) {
            $entity->addChangeRequest($key, $value);
        }

        $status = StatusCodeInterface::STATUS_ACCEPTED;
        if (RegisterHelper::hasChangedAsRequested($entity)) {
            $entity->clearChangeRequest();
            $status = StatusCodeInterface::STATUS_OK;
        }
        $this->repository->updateChangeRequest($entity);

        return $response->withStatus($status, 'Accepted'); // processed asynchronously
    }
}
