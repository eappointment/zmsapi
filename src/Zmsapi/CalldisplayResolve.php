<?php
/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use BO\Slim\Render;
use BO\Mellon\Validator;
use BO\Zmsdb\ApplicationRegister as ApplicationRegisterRepository;
use BO\Zmsdb\Calldisplay as Query;
use BO\Zmsentities\Calldisplay as Entity;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @SuppressWarnings(Coupling)
 * @return String
 */
class CalldisplayResolve extends BaseController
{
    /** @var ApplicationRegisterRepository */
    protected $appRegRepo;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->appRegRepo = new ApplicationRegisterRepository();
        if ($container->has('zmsdb.repositories.application-register')) {
            $this->appRegRepo = $container->get('zmsdb.repositories.application-register');
        }
    }


    /**
     * @SuppressWarnings(Param)
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $query = new Query();
        $resolveReferences = Validator::param('resolveReferences')->isNumber()->setDefault(1)->getValue();
        $input = Validator::input()->isJson()->assertValid()->getValue();
        $entity = new Entity($input);

        $this->testScopeAndCluster($entity);

        $entity->status = $entity->status ?? [];
        $entity->status['createDate'] = $entity->status['createDate'] ?? \App::$now->format('Y-m-d');
        $entity->status['lastDate'] = $entity->status['lastDate'] ?? \App::$now->format('Y-m-d');
        $entity->status['daysActive'] = $entity->status['daysActive'] ?? 1;

        if (!empty($entity->hash)) {
            $completeEntity  = $this->appRegRepo->readEntity($entity->hash);
            if ($completeEntity) {
                $entity->status['createDate'] = $completeEntity->startDate->format('Y-m-d');
                $entity->status['lastDate'] = $completeEntity->lastDate->format('Y-m-d');
                $entity->status['daysActive'] = $completeEntity->daysActive;
            }
        }

        $message = Response\Message::create($request);
        $message->data = $query->readResolvedEntity($entity, \App::getNow(), $resolveReferences);

        $response = Render::withLastModified($response, time(), '0');
        $response = Render::withJson($response, $message->setUpdatedMetaData(), $message->getStatuscode());

        return $response;
    }

    protected function testScopeAndCluster($calldisplay)
    {
        if (! $calldisplay->hasScopeList() && ! $calldisplay->hasClusterList()) {
            throw new Exception\Calldisplay\ScopeAndClusterNotFound();
        }
        foreach ($calldisplay->getClusterList() as $cluster) {
            $foundCluster = (new \BO\Zmsdb\Cluster)->readEntity($cluster->id);
            if (! $foundCluster) {
                throw new Exception\Cluster\ClusterNotFound();
            }
            if ($foundCluster->offsetExists('scopes')) {
                $cluster['scopes'] = $foundCluster['scopes'];
            }
        }
        foreach ($calldisplay->getScopeList() as $scope) {
            $scope = (new \BO\Zmsdb\Scope)->readEntity($scope->id);
            if (! $scope) {
                throw new Exception\Scope\ScopeNotFound();
            }
        }
    }
}
