<?php

/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use BO\Slim\Render;

use BO\Zmsapi\Exception\Process\ProcessAlreadyCalled as ProcessAlreadyCalledException;
use BO\Zmsapi\Exception\Process\ProcessDeleteFailed as ProcessDeleteFailedException;
use BO\Zmsapi\Exception\Process\ProcessNoAccess as ProcessNoAccessException;
use BO\Zmsapi\Exception\Process\ProcessNotFound as ProcessNotFoundException;
use BO\Zmsapi\Exception\Process\ProcessExpired as ProcessExpiredException;

use BO\Zmsappointment\Helper\Process;
use BO\Zmsdb\Cluster as ClusterRepository;
use BO\Zmsdb\Connection\Select as DBConnection;
use BO\Zmsdb\Exception\Pdo\DeadLockFound;
use BO\Zmsdb\Exception\Pdo\LockTimeout;
use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsdb\Exception\Process\ProcessArchiveUpdateFailed;
use BO\Zmsdb\Helper\NoAuth;
use BO\Zmsdb\Process as ProcessRepository;

use BO\Zmsentities\Exception\SchemaValidation;
use BO\Zmsentities\Exception\TemplateNotFound;
use BO\Zmsentities\Exception\WorkstationProcessMatchScopeFailed;
use BO\Zmsentities\Process as ProcessEntity;

use BO\Zmsentities\Workstation as WorkstationEntity;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @SuppressWarnings(coupling)
 */
class ProcessDeleteQuick extends ProcessDelete
{
    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     * @throws PDOFailed
     * @throws ProcessAlreadyCalledException
     * @throws ProcessDeleteFailedException
     * @throws ProcessNotFoundException
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws ProcessArchiveUpdateFailed
     * @throws ProcessNoAccessException
     * @throws TemplateNotFound
     */
    public function readResponse(
        RequestInterface  $request,
        ResponseInterface $response,
        array             $args
    ): ResponseInterface {
        DBConnection::getWriteConnection();

        $workstation = (new Helper\User($request, 1))->checkRights('basic');
        $validator = $request->getAttribute('validator');
        $initiator = $validator->getParameter('initiator')->isString()->getValue();
        $processId = $validator::value($args['id'])->isNumber()->getValue();
        $process = (new ProcessRepository())->readEntity($processId, new NoAuth(), 1);

        $this->testProcess($workstation, $process, $initiator);
        $process->status = ProcessEntity::STATUS_BLOCKED;
        $this->writeMails($request, $process);

        $status = (new ProcessRepository())->writeCanceledEntity($process->getId(), $process->getAuthKey());
        if (!$status) {
            throw new ProcessDeleteFailedException(); // @codeCoverageIgnore
        }
        $message = Response\Message::create($request);
        $message->data = $process;

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message->setUpdatedMetaData(), $message->getStatuscode());
    }

    /**
     * @param WorkstationEntity $workstation
     * @param ProcessEntity $process
     * @param ?string $initiator
     * @throws ProcessAlreadyCalledException
     * @throws ProcessExpiredException
     * @throws ProcessNoAccessException
     * @throws ProcessNotFoundException
     * @throws SchemaValidation
     */
    protected function testProcess(
        WorkstationEntity $workstation,
        ProcessEntity $process,
        ?string $initiator = null
    ): void {
        if (!$process->hasId()) {
            throw new ProcessNotFoundException();
        }
        if ($initiator != 'admin-cancel-all' && $process->getFirstAppointment()->date < \App::$now->getTimestamp()) {
            throw new ProcessExpiredException();
        }

        $this->testProcessAccess($workstation, $process);

        if (ProcessEntity::STATUS_CALLED == $process->status || ProcessEntity::STATUS_PROCESSING == $process->status) {
            throw new ProcessAlreadyCalledException();
        }
        $process->testValid();
    }

    /**
     * @throws ProcessNoAccessException
     */
    protected function testProcessAccess(WorkstationEntity $workstation, $process)
    {
        if ($workstation->hasSuperUseraccount()) {
            return true;
        }

        $cluster = (new ClusterRepository)->readByScopeId($workstation->scope['id'], 1);
        try {
            $workstation->testMatchingProcessScope($workstation->getScopeList($cluster), $process);
        } catch (WorkstationProcessMatchScopeFailed $e) {
            throw new ProcessNoAccessException();
        }
    }
}
