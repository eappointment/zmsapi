<?php

/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/
namespace BO\Zmsapi;

use BO\Slim\Render;
use BO\Zmsapi\Exception\Apikey\AccessDenied;
use BO\Zmsapi\Exception\Apikey\ApiKeyNotFound;
use BO\Zmsapi\Exception\Apikey\ApiKeyNotValid;
use BO\Zmsapi\Helper\ApikeyService;
use BO\Zmsdb\Apikey as ApikeyRepository;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class ApikeyLockGet extends BaseController
{

    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     * @throws AccessDenied
     * @throws ApiKeyNotFound
     * @throws ApiKeyNotValid
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $validator = $request->getAttribute('validator');
        $key = isset($args['key']) ? $validator::value($args['key'])->isString()->isRequired()->getValue() : null;
        $token = $validator->getParameter('token')->isString()->getValue();
        $apikeyService = new ApikeyService($key, $token, true);
        $apikeyService->testSession();

        $message = Response\Message::create($request);
        $message->data = (new ApikeyRepository())->withUpdatedLockedStatus($apikeyService->getApikey()->getId(), 1);

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message->setUpdatedMetaData(), $message->getStatuscode());
    }
}
