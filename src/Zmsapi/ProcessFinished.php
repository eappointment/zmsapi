<?php
/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use App;
use BO\Slim\Render;

use BO\Zmsapi\Exception\Mail\MailSenderFromMissing;
use BO\Zmsapi\Exception\Process\ProcessInvalid as ProcessInvalidException;
use BO\Zmsapi\Exception\Process\ProcessNotFound as ProcessNotFoundException;
use BO\Zmsapi\Exception\Process\AuthKeyMatchFailed as AuthKeyMatchFailedException;

use BO\Zmsdb\Exception\Mail\ClientWithoutEmail;
use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsdb\Exception\Process\ProcessUpdateFailed;
use BO\Zmsdb\Helper\MailService;
use BO\Zmsdb\ProcessStatusArchived as Repository;
use BO\Zmsdb\Process as ProcessRepository;
use BO\Zmsdb\Workstation as WorkstationRepository;
use BO\Zmsdb\Config as ConfigRepository;
use BO\Zmsdb\Scope as ScopeRepository;
use BO\Zmsdb\Department as DepartmentRepository;
use BO\Zmsdb\Mail as MailRepository;
use BO\Zmsdb\Helper\NoAuth as NoAuthHelper;
use BO\Zmsdb\Connection\Select as DbConnection;

use BO\Zmsentities\Collection\ProcessList;
use BO\Zmsentities\Exception\TemplateNotFound;
use BO\Zmsentities\Process as ProcessEntity;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @SuppressWarnings(Coupling)
 */
class ProcessFinished extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     * @throws AuthKeyMatchFailedException
     * @throws ClientWithoutEmail
     * @throws MailSenderFromMissing
     * @throws PDOFailed
     * @throws ProcessInvalidException
     * @throws ProcessNotFoundException
     * @throws ProcessUpdateFailed
     * @throws TemplateNotFound
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        DbConnection::getWriteConnection();

        $workstation = (new Helper\User($request))->checkRights();
        $valdidator = $request->getAttribute('validator');
        $input = $valdidator::input()->isJson()->assertValid()->getValue();
        $process = new ProcessEntity($input);
        $process->testValid();
        $this->testProcessData($process);
        $this->testProcessInWorkstation($process, $workstation);

        (new WorkstationRepository())->writeRemovedProcess($workstation);
        (new ProcessRepository())->updateEntity($process, App::$now);
        if ($process->getStatus() != ProcessEntity::STATUS_PENDING) {
            (new Repository())->writeEntityFinished($process, App::$now);
        }
        $this->writeSurveyMail($process, $valdidator);
        
        $message = Response\Message::create($request);
        $message->data = $process;

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message->setUpdatedMetaData(), $message->getStatuscode());
    }

    protected function testProcessInWorkstation($process, $workstation)
    {
        $department = (new DepartmentRepository)->readByScopeId($workstation->scope['id'], 1);
        $workstation->process = $process;
        $workstation->testMatchingProcessScope($department->getScopeList());
    }

    /**
     * @param $process
     *
     * @throws AuthKeyMatchFailedException
     * @throws ProcessInvalidException
     * @throws ProcessNotFoundException
     */
    protected function testProcessData($process)
    {
        $hasValidId = (
            $process->hasId() && (
                $process->getStatus() == ProcessEntity::STATUS_PENDING ||
                $process->getStatus() == ProcessEntity::STATUS_FINISHED
            )
        );
        if (! $hasValidId) {
            throw new ProcessInvalidException();
        }

        $processCheck = (new ProcessRepository())->readEntity($process->id, new NoAuthHelper());
        if (null === $processCheck || false === $processCheck->hasId()) {
            throw new ProcessNotFoundException();
        } elseif ($processCheck->authKey != $process->authKey) {
            throw new AuthKeyMatchFailedException();
        }
    }

    /**
     * @param $process
     *
     * @throws ClientWithoutEmail
     * @throws MailSenderFromMissing
     * @throws TemplateNotFound
     */
    protected function writeSurveyMail($process, $validator): void
    {
        if (!$validator::param('survey')->isNumber()->setDefault(1)->getValue()) {
            return;
        }
        $mailService = new MailService(new MailRepository(), new ConfigRepository());
        $process = clone $process;
        foreach ($process->getClients() as $client) {
            if ($client->hasSurveyAccepted()) {
                $process->scope = (new ScopeRepository())->readEntity($process['scope']['id']);
                $mailService->setProcessList((new ProcessList())->addEntity($process));
                $mail = $mailService->readResolvedEntity($mailService::MAIL_TYPE_SURVEY);
                $mailService->writeMailInQueue($mail, App::$now, false);
            }
        }
    }
}
