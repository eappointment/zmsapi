<?php

/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use App;
use BO\Mellon\Validator;
use BO\Slim\Render;
use BO\Zmsapi\Exception\Apikey\AccessDenied;
use BO\Zmsapi\Exception\Apikey\ApiKeyLocked;
use BO\Zmsapi\Exception\Apikey\ApiKeyNotValid;
use BO\Zmsapi\Exception\Process\FreeProcessListEmpty;
use BO\Zmsapi\Exception\Process\ProcessAlreadyConfirmed;
use BO\Zmsapi\Helper\ApikeyService;
use BO\Zmsapi\Helper\DateTimeRestriction;
use BO\Zmsdb\ProcessStatusFree;
use BO\Zmsentities\Calendar as CalendarEntity;
use BO\Zmsentities\Collection\ProcessList;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @SuppressWarnings(Cyclomatic)
 * @SuppressWarnings(Coupling)
 * @SuppressWarnings(CouplingBetweenObjects)
 */
class ProcessFree extends BaseController
{
    protected $slotsRequired = 0;

    protected $slotType = 'public';

    protected ProcessList $processList;

    protected Validator $validator;

    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     * @throws AccessDenied
     * @throws ApiKeyLocked
     * @throws ApiKeyNotValid
     * @throws ProcessAlreadyConfirmed
     * @throws FreeProcessListEmpty
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $this->validator = $request->getAttribute("validator");
        $slotType = $this->validator->getParameter('slotType')->isString()->getValue();
        $slotsRequired = $this->validator->getParameter('slotsRequired')->isNumber()->getValue();

        if ($slotType || $slotsRequired) {
            (new Helper\User($request))->checkRights();
            $this->slotType = ($slotType) ? $slotType : $this->slotType;
            $this->slotsRequired = ($slotsRequired) ? $slotsRequired : $this->slotsRequired;
        }

        $this->readFreeProcessList();
        $this->testFreeProcessList();
        $this->withTimeRestriction();
        $this->withApiClient($request);

        $message = Response\Message::create($request);
        $message->data = $this->processList;

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message->setUpdatedMetaData());
    }

    /**
     * @param $validator
     *
     * @return void
     */
    protected function readFreeProcessList(): void
    {
        $groupData = $this->validator->getParameter('groupData')->isNumber()->getValue();
        $keepLessData = $this->validator->getParameter('keepLessData')->isArray()->setDefault([])->getValue();
        $input = $this->validator->getInput()->isJson()->assertValid()->getValue();
        $calendar = new CalendarEntity($input);
        $processList = (new ProcessStatusFree())
            ->readFreeProcesses($calendar, App::getNow(), $this->slotType, $this->slotsRequired, (bool)$groupData)
            ->withLessData($keepLessData);
        if ($groupData && count($processList) >= $groupData) {
            $processList = $processList->withUniqueScope(true);
        } elseif ($groupData) {
            $processList = $processList->withUniqueScope();
        }
        $this->processList = $processList;
    }

    protected function withTimeRestriction(): void
    {
        $timerestriction = $this->validator->getParameter('timerestriction')->isString()->getValue();
        if ($timerestriction) {
            $dateTimeRestriction = new DateTimeRestriction($timerestriction);
            $this->processList = $dateTimeRestriction->getRestrictedProcessList($this->processList);
        }
    }

    /**
     * @throws ApiKeyNotValid
     * @throws AccessDenied
     * @throws ApiKeyLocked
     */
    protected function withApiClient(RequestInterface $request): void
    {
        $clientKey = $request->getHeaderLine('X-Api-Key');
        if ($clientKey && $this->processList->count() > 0) {
            $apikeyService = new ApikeyService($clientKey);
            $this->processList->setApiclient($apikeyService->getApiclient());
        }
    }

    /**
     * @param ProcessList $processList
     *
     * @return void
     * @throws FreeProcessListEmpty
     */
    protected function testFreeProcessList(): void
    {
        if ($this->processList->count() === 0) {
            throw new FreeProcessListEmpty();
        }
    }
}
