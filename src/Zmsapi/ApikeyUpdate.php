<?php

/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use App;
use BO\Slim\Render;
use BO\Zmsapi\Exception\Apikey\AccessDenied;
use BO\Zmsapi\Exception\Apikey\ApiKeyNotValid;
use BO\Zmsdb\Apiclient as ApiclientRepository;
use BO\Zmsdb\Apikey as ApikeyRepository;
use BO\Zmsdb\Connection\Select as DBConnection;
use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsentities\Apiclient as ApiclientEntity;
use BO\Zmsentities\Apikey as ApikeyEntity;
use BO\Zmsentities\Schema\Entity;
use Exception;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @SuppressWarnings(coupling)
 */
class ApikeyUpdate extends BaseController
{
    /**
     * @var ApikeyRepository
     */
    protected $apikeyRepository;

    /**
     * @var ApiclientRepository
     */
    protected $apiclientRepository;

    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     * @throws PDOFailed
     * @throws AccessDenied
     * @throws ApiKeyNotValid
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        DBConnection::getWriteConnection();

        $this->apikeyRepository = new ApikeyRepository();
        $this->apiclientRepository = new ApiclientRepository();

        $validator = $request->getAttribute('validator');
        $clientKey = $validator->getParameter('clientkey')->isString()->getValue();
        $input = $validator::input()->isJson()->assertValid()->getValue();
        $postEntity = new ApikeyEntity($input);

        $apiClient = ($clientKey) ?
            $this->apiclientRepository->readEntity($clientKey) :
            $postEntity->getApiClient();
        $this->testAccess($apiClient);

        $apiKey = $this->getUpdatedApikey($apiClient, $postEntity);

        $message = Response\Message::create($request);
        $message->data = $apiKey;

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message->setUpdatedMetaData(), $message->getStatuscode());
    }

    /**
     * @param ApiclientEntity $apiClient
     *
     * @return void
     * @throws AccessDenied
     */
    protected function testAccess(ApiclientEntity $apiClient)
    {
        if (!isset($apiClient->accesslevel) || $apiClient->accesslevel == 'blocked') {
            throw new AccessDenied();
        }
    }

    /**
     * @param ApiclientEntity $apiClient
     * @param ApikeyEntity    $postEntity
     *
     * @return ApikeyEntity|Entity
     * @throws AccessDenied
     */
    protected function getUpdatedApikey(ApiclientEntity $apiClient, ApikeyEntity $postEntity)
    {
        $apiKey = $this->apikeyRepository->readEntity($postEntity->key);
        if (!$apiKey->hasId() || $apiKey->isExpired(App::$now)) {
            try {
                $postEntity->setApiClient($apiClient);
                $apiKey = $this->apikeyRepository->writeEntity($postEntity);
            } catch (Exception $exception) {
                throw new AccessDenied();
            }
        }
        return $apiKey;
    }
}
