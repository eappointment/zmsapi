<?php
/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use BO\Slim\Render;

use BO\Zmsapi\Exception\Apikey\ApiKeyNotFound;
use BO\Zmsapi\Exception\Apikey\ApiKeyNotValid;
use BO\Zmsapi\Exception\Process\ApiclientInvalid as ApiclientInvalidException;
use BO\Zmsapi\Exception\Apikey\AccessDenied as AccessDeniedException;
use BO\Zmsapi\Helper\ApikeyService;

use BO\Zmsentities\Exception\SchemaValidation as SchemaValidationException;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @SuppressWarnings(Coupling)
 */
class ApikeyTokenGet extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface $response
     * @throws AccessDeniedException|SchemaValidationException|ApiclientInvalidException
     * @throws ApiKeyNotFound
     * @throws ApiKeyNotValid
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $validator = $request->getAttribute('validator');
        $token = $validator->getParameter('token')->isString()->getValue();

        $apikeyService = new ApikeyService(null, $token, true);
        $apikeyService->writeNewSession();
        $apikeyService->testSession();
        $message = Response\Message::create($request);
        $message->data = $apikeyService->getApikey();

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message->setUpdatedMetaData(), $message->getStatuscode());
    }
}
