<?php
/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use BO\Slim\Render;

use BO\Zmsdb\Scope;

use BO\Zmsentities\Useraccount\EntityAccess;
use BO\Zmsentities\Scope as ScopeEntity;

use BO\Zmsapi\Exception\Scope\ScopeNotFound as ScopeNotFoundException;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class ScopeUpdate extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     * @throws ScopeNotFoundException
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $validator = $request->getAttribute('validator');
        $input = $validator::input()->isJson()->assertValid()->getValue();
        $scopeId = $validator::value($args['id'])->isNumber()->getValue();

        $scope = (new Scope)->readEntity($scopeId, 1);
        $this->testScope($scope);

        (new Helper\User($request, 2))->checkRights('ticketprinter', new EntityAccess($scope));

        $scope->addData($input)->testValid('de_DE', 1);

        $message = Response\Message::create($request);
        $message->data = (new Scope)->updateEntity($scope->id, $scope);

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message, $message->getStatuscode());
    }

    /**
     * @param ScopeEntity|null $scope
     *
     * @throws ScopeNotFoundException
     */
    protected function testScope(ScopeEntity $scope = null)
    {
        if (! $scope) {
            throw new ScopeNotFoundException();
        }
    }
}
