<?php
/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use BO\Slim\Render;
use BO\Mellon\Validator;

use BO\Zmsapi\Exception\Cluster\ClusterNotFound;

use BO\Zmsapi\Helper\Mimepart as MimepartHelper;
use BO\Zmsdb\Cluster as ClusterRepository;
use BO\Zmsdb\Connection\Select as DBConnection;
use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsdb\Scope as ScopeRepository;

use BO\Zmsentities\Exception\SchemaValidation as SchemaValidationException;
use BO\Zmsentities\Mimepart as MimepartEntity;
use BO\Zmsentities\Cluster as ClusterEntity;

use League\JsonGuard\ValidationError;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @SuppressWarnings(CouplingBetweenObjects)
 */
class ClusterCalldisplayImageDataUpdate extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     * @throws ClusterNotFound
     * @throws SchemaValidationException
     * @throws PDOFailed
     */
    public function readResponse(
        RequestInterface  $request,
        ResponseInterface $response,
        array             $args
    ): ResponseInterface {
        (new Helper\User($request))->checkRights('cluster');
        $cluster = (new ClusterRepository)->readEntity($args['id']);
        $this->testCluster($cluster);

        $input = Validator::input()->isJson()->getValue();
        $mimepart = new MimepartEntity($input);
        MimepartHelper::testImageSize($mimepart);

        DBConnection::getWriteConnection();
        $message = Response\Message::create($request);
        $message->data = (new ClusterRepository)->writeImageData($cluster->id, $mimepart);
        DBConnection::writeCommit();

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message, $message->getStatuscode());
    }

    /**
     * @param ClusterEntity|null $cluster
     *
     * @return void
     * @throws ClusterNotFound
     */
    protected function testCluster(?ClusterEntity $cluster)
    {
        if (!$cluster) {
            throw new Exception\Cluster\ClusterNotFound();
        }
    }
}
