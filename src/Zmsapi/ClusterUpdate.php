<?php
/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use BO\Slim\Render;

use BO\Zmsapi\Exception\Cluster\ClusterNotFound;

use BO\Zmsdb\Cluster as ClusterRepository;

use BO\Zmsentities\Cluster as ClusterEntity;
use BO\Zmsentities\Schema\Entity as SchemaEntity;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class ClusterUpdate extends BaseController
{
    /**
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     * @throws ClusterNotFound
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $validator = $request->getAttribute('validator');
        $input = $validator::input()->isJson()->assertValid()->getValue();
        $clusterId = $validator::value($args['id'])->isNumber()->getValue();

        (new Helper\User($request))->checkRights('cluster');

        $cluster = (new ClusterRepository())->readEntity($clusterId);
        $this->testCluster($cluster);

        $entity = new ClusterEntity($input);
        $entity->testValid();

        $message = Response\Message::create($request);
        $message->data = (new ClusterRepository())->updateEntity($cluster->id, $entity);

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message->setUpdatedMetaData(), $message->getStatuscode());
    }

    /**
     * @param ClusterEntity|SchemaEntity|null $cluster
     *
     * @throws ClusterNotFound
     */
    protected function testCluster(ClusterEntity $cluster = null)
    {
        if (! $cluster) {
            throw new Exception\Cluster\ClusterNotFound();
        }
    }
}
