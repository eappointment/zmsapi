<?php

/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use App;
use BO\Slim\Render;
use BO\Zmsapi\Exception\Apikey\AccessDenied;
use BO\Zmsapi\Exception\Apikey\ApiKeyLocked;
use BO\Zmsapi\Exception\Apikey\ApiKeyNotValid;
use BO\Zmsapi\Exception\Process\ProcessAlreadyConfirmed;
use BO\Zmsapi\Helper\ApikeyService;
use BO\Zmsdb\Apikey as ApikeyRepository;
use BO\Zmsdb\Exception\Pdo\DeadLockFound;
use BO\Zmsdb\Exception\Pdo\LockTimeout;
use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsdb\Exception\Process\ProcessArchiveUpdateFailed;
use BO\Zmsdb\Exception\Process\ProcessUpdateFailed;
use BO\Zmsdb\Process as ProcessRepository;
use BO\Zmsentities\Process as ProcessEntity;
use BO\Zmsentities\Collection\ProcessList;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 *
 * @SuppressWarnings(CouplingBetweenObjects)
 * @SuppressWarnings(TooManyPublicMethods)
 * @SuppressWarnings(Complexity)
 */
class ApikeyProcessUpdateAll extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     * @throws AccessDenied
     * @throws DeadLockFound
     * @throws ApiKeyLocked
     * @throws ApiKeyNotValid
     * @throws LockTimeout
     * @throws PDOFailed
     * @throws ProcessAlreadyConfirmed
     * @throws ProcessUpdateFailed
     * @throws ProcessArchiveUpdateFailed
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $validator = $request->getAttribute('validator');
        $key = isset($args['key']) ? $validator::value($args['key'])->isString()->isRequired()->getValue() : null;
        $token = $validator->getParameter('token')->isString()->isRequired()->getValue();
        $input = $validator::input()->isJson()->assertValid()->getValue();
        $processEntity = (new ProcessEntity())->addData($input);

        $apikeyService = new ApikeyService($key, $token, true);
        $apikeyService->testSession();
        $apikeyService->testHasConfirmedProcess();
        $apikeyService->testApikeyAccess();

        $processList = $this->writeUpdatedProcessList($apikeyService, $processEntity);

        $message = Response\Message::create($request);
        $message->data = $processList;

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message->setUpdatedMetaData(), $message->getStatuscode());
    }

    /**
     * @param ApikeyService $apikeyService
     * @param ProcessList   $updateList
     *
     * @return ProcessList
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws PDOFailed
     * @throws ProcessUpdateFailed
     * @throws ProcessArchiveUpdateFailed
     */
    protected function writeUpdatedProcessList(ApikeyService $apikeyService, ProcessEntity $processEntity): ProcessList
    {
        $sessionProcessList = $apikeyService->getProcessListFromSession();
        $processList = new ProcessList();
        foreach ($sessionProcessList as $sessionProcess) {
                $sessionProcess->withUpdateClientData($processEntity->getFirstClient()->getArrayCopy());
                $sessionProcess->amendment = $processEntity->getAmendment();
            if ($apikeyService->getApiclient()->getPermission()->hasRight('remotecall')) {
                $sessionProcess = $sessionProcess
                    ->withUpdatedRemoteCalls($processEntity->getRemoteCalls(true), true);
            }
                $process = (new ProcessRepository())->updateEntity($sessionProcess, App::$now, 1);
                $processList = (new ApikeyRepository())
                    ->writeProcessInSession($apikeyService->getApikey()->getId(), $process);
        }
        return ($processList->count() > 0) ? $processList : $sessionProcessList;
    }
}
