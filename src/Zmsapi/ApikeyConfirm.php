<?php

/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use App;
use BO\Zmsapi\Exception\Apikey\ApiKeyLocked;
use BO\Zmsapi\Exception\Apikey\MissingRequiredData;
use BO\Zmsapi\Exception\Process\ProcessAlreadyConfirmed;
use BO\Zmsapi\Exception\Apikey\AccessDenied as AccessDeniedException;
use BO\Zmsapi\Exception\Apikey\ApiKeyNotValid as ApiKeyNotValidException;
use BO\Zmsapi\Helper\ApikeyService;
use BO\Zmsdb\Exception\Pdo\DeadLockFound;
use BO\Zmsdb\Exception\Pdo\LockTimeout;
use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsdb\Exception\Process\ProcessArchiveUpdateFailed;
use BO\Zmsdb\Exception\Process\ProcessUpdateFailed;
use BO\Slim\Render;
use BO\Zmsdb\Process as ProcessRepository;
use BO\Zmsentities\Process as ProcessEntity;
use BO\Zmsentities\Collection\ProcessList;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @SuppressWarnings(Cyclomatic)
 * @SuppressWarnings(CouplingBetweenObjects)
 */
class ApikeyConfirm extends BaseController
{
    protected $resolveReferences;

    /**
     * @SuppressWarnings(Param)
     *
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     * @throws AccessDeniedException
     * @throws ApiKeyLocked
     * @throws ApiKeyNotValidException
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws MissingRequiredData
     * @throws PDOFailed
     * @throws ProcessAlreadyConfirmed
     * @throws ProcessUpdateFailed
     * @throws ProcessArchiveUpdateFailed
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $validator = $request->getAttribute('validator');
        $key = isset($args['key']) ? $validator::value($args['key'])->isString()->isRequired()->getValue() : null;
        $token = $validator->getParameter('token')->isString()->getValue();
        $this->resolveReferences = $validator::param('resolveReferences')->isNumber()->setDefault(1)->getValue();
        $input = $validator::input()->isJson()->assertValid()->getValue();
        $processEntity = (new ProcessEntity())->addData($input);

        $apikeyService = new ApikeyService($key, $token, true);
        $apikeyService->testSession();
        $apikeyService->testHasConfirmedProcess();
        $apikeyService->testHasSessionProcessList();
        $processList = $this->writeConfirmedProcessList($apikeyService, $processEntity);

        if ($processList->count() > 0) {
            $apikeyService->deleteExpiredApikey();
            $apikeyService->deleteSession();
        }

        $message = Response\Message::create($request);
        $message->data = $processList;

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message->setUpdatedMetaData(), $message->getStatuscode());
    }

    /**
     *
     * @param                  $apikeyService
     * @param ProcessList|null $updateList
     *
     * @return ProcessList
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws PDOFailed
     * @throws ProcessArchiveUpdateFailed
     * @throws ProcessUpdateFailed
     */
    protected function writeConfirmedProcessList($apikeyService, ProcessEntity $processEntity): ProcessList
    {
        $list = new ProcessList();
        $sessionProcessList = $apikeyService->getProcessListFromSession();
        foreach ($sessionProcessList as $sessionProcess) {
            $updateEntity = $this->writeConfirmedProcess($sessionProcess, $processEntity);
            $list->addEntity($updateEntity);
            //(new ApikeyRepository())->writeProcessInSession($apikeyService->getApikey()->getId(), $updateEntity);
        }
        return $list;
    }

    /**
     * @param ProcessEntity      $process
     * @param ProcessEntity|null $updateProcessEntity
     *
     * @return ProcessEntity
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws PDOFailed
     * @throws ProcessUpdateFailed
     * @throws ProcessArchiveUpdateFailed
     */
    protected function writeConfirmedProcess(
        ProcessEntity $process,
        ProcessEntity $updateProcessEntity
    ): ProcessEntity {
        $process->withUpdateClientData($updateProcessEntity->getFirstClient()->getArrayCopy());
        $process->amendment = $updateProcessEntity->getAmendment();
        $process->status = ProcessEntity::STATUS_CONFIRMED;
        return (new ProcessRepository())->updateEntity($process, \App::$now, $this->resolveReferences);
    }
}
