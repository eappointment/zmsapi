<?php
/**
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmsapi;

use BO\Slim\Render;
use BO\Zmsdb\AccessStats as StatsRepository;
use BO\Zmsdb\Session as SessionRepository;
use BO\Zmsentities\AccessStats;
use BO\Zmsentities\Helper\DateTime;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class AccessStatsList extends BaseController
{
    /**
     * @var StatsRepository
     */
    private $statsRepository;

    /**
     * @var SessionRepository
     */
    private $sessionRepository;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->statsRepository = new StatsRepository();
        if ($container->has('zmsdb.repositories.access-stats')) {
            $this->statsRepository = $container->get('zmsdb.repositories.access-stats');
        }

        $this->sessionRepository = new SessionRepository();
        if ($container->has('zmsdb.repositories.session')) {
            $this->sessionRepository = $container->get('zmsdb.repositories.session');
        }
    }

    /**
     * @SuppressWarnings(Param)
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ) {
        (new Helper\User($request))->checkRights('superuser');
        // used for admin area atm
        $statsList = $this->statsRepository->readList();

        $citizenSessions = $this->sessionRepository->readLastActiveList(15 * 60, 'Zmsappointment');
        foreach ($citizenSessions as $citizen) {
            $dateTime = new DateTime($citizen['ts'], new \DateTimeZone('UTC')); // UTC like the timestamp
            $statsList->addEntity(new AccessStats([
                'id'         => (int)substr(preg_replace("/[^0-9]/", "", $citizen['id']), 0, 18),
                'role'       => 'citizen',
                'location'   => 'zmsappointment',
                'lastActive' => $dateTime->getTimestamp(),
            ]));
        }

        $message = Response\Message::create($request);
        $message->data = $statsList;

        $response = Render::withLastModified($response, time());
        return Render::withJson($response, $message);
    }
}
