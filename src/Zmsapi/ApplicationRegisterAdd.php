<?php
/**
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmsapi;

use BO\Mellon\Unvalidated;
use BO\Mellon\ValidJson;
use BO\Slim\Render;
use BO\Zmsdb\ApplicationRegister as Repository;
use BO\Zmsentities\ApplicationRegister;
use BO\Zmsentities\Helper\ApplicationRegister as Helper;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * adds a register entry or updates an existing one
 */
class ApplicationRegisterAdd extends BaseController
{
    /**
     * @var Repository
     */
    private $repository;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->repository = new Repository();
        if ($container->has('zmsdb.repositories.application-register')) {
            $this->repository = $container->get('zmsdb.repositories.application-register');
        }
    }

    /**
     * @SuppressWarnings(Param)
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ) {
        /** @var Unvalidated|ValidJson $validatorInput */
        $validatorInput = $request->getAttribute('validator')->getInput();
        $input = $validatorInput->isJson()->assertValid()->getValue();
        $entity = new ApplicationRegister($input);
        $entity->testValid();

        $result = $this->repository->writeEntity($entity);

        if ($result->hasChangeRequest() && Helper::hasChangedAsRequested($result)) {
            $result->clearChangeRequest();
            $this->repository->updateChangeRequest($result);
        }

        $message = Response\Message::create($request);
        $message->data = $result;

        $response = Render::withLastModified($response, time())
            ->withStatus(StatusCodeInterface::STATUS_CREATED, 'application register entry created');

        return Render::withJson($response, $message);
    }
}
