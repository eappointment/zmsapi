<?php

/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use App;
use BO\Mellon\Failure\Exception;
use BO\Mellon\Validator;
use BO\Slim\Render;
use BO\Zmsapi\Exception\Calendar\AppointmentsMissed as AppointmentsMissingException;
use BO\Zmsapi\Exception\Calendar\InvalidFirstDay as InvalidFirstDayException;
use BO\Zmsapi\Helper\DateTimeRestriction;
use BO\Zmsdb\Calendar as CalendarRepository;
use BO\Zmsdb\Exception\CalendarWithoutScopes;
use BO\Zmsentities\Calendar as Entity;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @suppressWarnings(coupling)
 */
class CalendarGet extends BaseController
{
    protected string $slotType = 'public';

    protected int $slotsRequired = 0;

    protected Entity $inputEntity;

    protected Entity $calendar;

    protected Validator $validator;

    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @param array $args
     * @return ResponseInterface
     * @throws CalendarWithoutScopes
     * @throws InvalidFirstDayException
     * @throws AppointmentsMissingException
     * @throws Exception
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $this->validator = $request->getAttribute("validator");
        $slotsRequired = $this->validator->getParameter('slotsRequired')->isNumber()->getValue();
        $slotType = $this->validator->getParameter('slotType')->isString()->getValue();

        if ($slotType || $slotsRequired) {
            (new Helper\User($request))->checkRights();
            $this->slotType = ($slotType) ? $slotType : $this->slotType;
            $this->slotsRequired = ($slotsRequired) ? $slotsRequired : $this->slotsRequired;
        }

        $input = $this->validator->getInput()->isJson()->assertValid()->getValue();
        $this->inputEntity = new Entity($input);
        $this->testEntity();

        $this->readCalendar();
        $this->calendarWithDateRange();
        $this->testCalendar();
        $this->calendarWithTimeRestriction();

        $message = Response\Message::create($request);

        $message->data = $this->calendar;

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message->setUpdatedMetaData(), $message->getStatuscode());
    }

    /**
     * @throws CalendarWithoutScopes
     */
    protected function readCalendar(): void
    {
        $hasGQL = $this->validator->getParameter('gql')->isString()->getValue();
        $fillWithEmptyDays = $this->validator->getParameter('fillWithEmptyDays')->isNumber()->setDefault(0)->getValue();
        $untilBookableEnd = $this->validator->getParameter('untilBookableEnd')->isNumber()->setDefault(0)->getValue();
        $untilBookableEnd = ($untilBookableEnd === 1);

        $calendarRepo = new CalendarRepository();
        $calendar = $calendarRepo->readResolvedEntity(
            $this->inputEntity,
            App::getNow(),
            null,
            $this->slotType,
            $this->slotsRequired,
            $untilBookableEnd
        );
        $calendar = ($hasGQL) ? $calendar : $calendar->withLessData();

        if ($fillWithEmptyDays) {
            $calendar = $calendar->withFilledEmptyDays();
        }
        $this->calendar = $calendar;
    }

    protected function calendarWithDateRange(): void
    {
        $this->calendar->days = $this->calendar->days->withDaysInDateRange(
            $this->calendar->getFirstDay(),
            $this->calendar->getLastDay()
        );
    }

    protected function calendarWithTimeRestriction(): void
    {
        $timerestriction = $this->validator->getParameter('timerestriction')->isString()->getValue();
        if ($timerestriction) {
            $dateTimeRestriction = new DateTimeRestriction($timerestriction);
            $this->calendar->days = $dateTimeRestriction->getRestrictedDayList(
                $this->calendar,
                $this->slotType,
                $this->slotsRequired
            );
        }
    }

    /**
     * @return void
     * @throws InvalidFirstDayException
     */
    protected function testEntity(): void
    {
        if (!$this->inputEntity->hasFirstAndLastDay()) {
            throw new InvalidFirstDayException('First and last day are required');
        }
    }

    /**
     * @return void
     * @throws AppointmentsMissingException
     */
    protected function testCalendar(): void
    {
        if ($this->calendar->days->count() == 0) {
            $exception = new AppointmentsMissingException();
            $exception->data = $this->calendar;
            throw $exception;
        }
    }
}
