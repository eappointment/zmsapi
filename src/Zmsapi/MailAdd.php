<?php

/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use App;
use BO\Slim\Render;
use BO\Zmsapi\Exception\Mail\MailSenderFromMissing as MailSenderFromMissingException;
use BO\Zmsdb\Config as ConfigRepository;
use BO\Zmsdb\Exception\Mail\ClientWithoutEmail as ClientWithoutEmailException;
use BO\Zmsdb\Helper\MailService;
use BO\Zmsdb\Mail as MailRepository;
use BO\Zmsentities\Mail as MailEntity;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @SuppressWarnings(coupling)
 */
class MailAdd extends BaseController
{
    /** @var MailService */
    protected $mailService;

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $containerInterface)
    {
        parent::__construct($containerInterface);

        $configRepo = new ConfigRepository();
        if ($containerInterface->has('zmsdb.repositories.config')) {
            $configRepo = $containerInterface->get('zmsdb.repositories.config');
        }

        $mailRepo = new MailRepository();
        if ($containerInterface->has('zmsdb.repositories.mail')) {
            $mailRepo = $containerInterface->get('zmsdb.repositories.mail');
        }

        $this->mailService = new MailService($mailRepo, $configRepo);
    }

    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     * @throws ClientWithoutEmailException|MailSenderFromMissingException
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        (new Helper\User($request))->checkRights('basic');
        $validator = $request->getAttribute('validator');
        $countMailSent = $validator->getParameter('count')->isNumber()->setDefault(1)->getValue();
        $input = $validator::input()->isJson()->assertValid()->getValue();

        $entity = new MailEntity($input);
        $this->testEntity($entity);
        $mail   = $this->mailService->writeMailInQueue($entity, App::$now, ($countMailSent !== 0));

        $response = Render::withLastModified($response, time(), '0');
        $message  = Response\Message::create($request);
        $message->data = $mail ?: $entity;

        return Render::withJson($response, $message->setUpdatedMetaData(), $message->getStatuscode());
    }

    /**
     * @param MailEntity $entity
     *
     * @throws ClientWithoutEmailException
     */
    protected function testEntity(MailEntity $entity)
    {
        $email = $entity->getFirstClient()->toProperty()->email->get();
        if ($email === null || $email === '') {
            throw new ClientWithoutEmailException();
        }
    }
}
