<?php

/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use App;
use BO\Mellon\Exception;
use BO\Slim\Render;
use BO\Mellon\Validator;
use BO\Zmsdb\Helper\PollingHelper;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class PollingGet extends BaseController
{
    public const TYPE_DISTANCE = 'distance';
    public const TYPE_AVAILABILITY_REVIEW = 'availability_review';
    public const TYPE_GROUPED_EMAILS = 'grouped_emails';

    public const TYPE_SURVEYS = 'surveys';

    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @param array $args
     * @return ResponseInterface
     * @throws Exception
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        (new Helper\User($request))->checkRights('superuser');
        $report = Validator::param('report')->isString()->setDefault(self::TYPE_DISTANCE)->getValue();
        $date = Validator::param('date')->isString()->getValue();
        $date = $date ?: App::$now->format('Y-m-d');
        $job = new PollingHelper($date);
        $result = [];

        if ($report == self::TYPE_DISTANCE) {
            $result = $job->startProcessingDistanceReport();
        }

        if ($report == self::TYPE_AVAILABILITY_REVIEW) {
            $result = $job->getAvailabilityReviewData();
        }

        if ($report == self::TYPE_GROUPED_EMAILS) {
            $result = $job->getGroupedEmailData();
        }

        if ($report == self::TYPE_SURVEYS) {
            $result = $job->getSurveysData();
        }

        $message = Response\Message::create($request);
        $message->data = $result;

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message);
    }
}
