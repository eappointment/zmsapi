<?php

/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use \BO\Slim\Render;
use \BO\Zmsdb\Status;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class StatusGet extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $validator = $request->getAttribute('validator');
        $includeProcessStats = $validator->getParameter('includeProcessStats')->isNumber()->setDefault(1)->getValue();
        $statusCategory = $validator->getParameter('statuscategory')->isString()->getValue();
        $statusLabel = $validator->getParameter('statuslabel')->isString()->getValue();

        if ($statusCategory && $statusLabel) {
            $status = (new Status())
                ->readLimitedEntity($statusCategory, $statusLabel, \App::$now)
                ->getFilteredByCategory($statusCategory);
        } else {
            $status = (new Status())->readEntity(\App::$now, $includeProcessStats);
            $status['version'] = Helper\Version::getArray();
        }

        if (\App::DEBUG) {
            $status['opcache'] = [
                'config' => opcache_get_configuration(),
                'status' => opcache_get_status(false)
            ];
        }

        $message = Response\Message::create($request);
        $message->data = $status;

        $response = Render::withLastModified($response, time(), '30');
        return Render::withJson($response, $message->setUpdatedMetaData(), $message->getStatuscode());
    }
}
