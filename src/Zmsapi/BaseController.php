<?php
/**
 * @package Zmsapi
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use BO\Slim\Response;
use BO\Zmsapi\Helper\Maintenance;
use Fig\Http\Message\StatusCodeInterface;
use \Psr\Http\Message\RequestInterface;
use \Psr\Http\Message\ResponseInterface;

/**
 * @SuppressWarnings(NumberOfChildren)
 *
 */
abstract class BaseController extends \BO\Slim\Controller
{
    public function __invoke(RequestInterface $request, ResponseInterface $response, array $args)
    {
        $request = $this->initRequest($request);
        $noCacheResponse = \BO\Slim\Render::withLastModified($response, time(), '0');

        /** @see Maintenance::isBlockedController() */
        $container   = \App::$slim->getContainer();
        if ($container->has('maintenance')
            && $container->get('maintenance')->isBlockedController($this)
        ) {
            return (new Response())->withStatus(StatusCodeInterface::STATUS_SERVICE_UNAVAILABLE, 'Maintenance');
        }

        return $this->readResponse($request, $noCacheResponse, $args);
    }

    /**
     * @codeCoverageIgnore
     *
     */
    public function readResponse(RequestInterface $request, ResponseInterface $response, array $args)
    {
        return parent::__invoke($request, $response, $args);
    }
}
