<?php

/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/
namespace BO\Zmsapi;

use BO\Zmsapi\Exception\Apikey\AccessDenied as AccessDeniedException;
use BO\Zmsapi\Exception\Apikey\ApiKeyLocked;
use BO\Zmsapi\Exception\Apikey\ApiKeyNotValid as ApiKeyNotValidException;
use BO\Zmsapi\Exception\Apikey\ApiKeyNotFound as ApiKeyNotFoundException;
use BO\Zmsapi\Exception\Process\ApiclientInvalid;
use BO\Zmsapi\Exception\Process\ProcessNotFound as ProcessNotFoundException;
use BO\Zmsapi\Helper\ApikeyService;

use BO\Slim\Render;

use BO\Zmsdb\Process as ProcessRepository;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class ApikeyProcessGet extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     * @throws ProcessNotFoundException
     * @throws ApiclientInvalid
     * @throws AccessDeniedException|ApiKeyNotValidException|ApiKeyNotFoundException
     * @throws ApiKeyLocked
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $validator = $request->getAttribute('validator');
        $key = isset($args['key']) ? $validator::value($args['key'])->isString()->isRequired()->getValue() : null;
        $token = $validator->getParameter('token')->isString()->getValue();
        $apikeyService = new ApikeyService($key, $token);
        $apikeyService->testSession();

        $processList = (new ProcessRepository())->readProcessListByClientkey($apikeyService->getApiclient()->clientKey);

        if (! $apikeyService->getToken()) {
            $processList->withoutRemotecalls();
        }
        $message = Response\Message::create($request);
        $message->data = $processList;

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message->setUpdatedMetaData(), $message->getStatuscode());
    }
}
