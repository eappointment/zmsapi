<?php
/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use \BO\Slim\Render;
use \BO\Mellon\Validator;
use \BO\Zmsdb\Workstation;
use \BO\Zmsdb\Useraccount;

/**
 * @SuppressWarnings(Coupling)
 */
class WorkstationLogin extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @return String
     */
    public function readResponse(
        \Psr\Http\Message\RequestInterface $request,
        \Psr\Http\Message\ResponseInterface $response,
        array $args
    ) {
        $validator = $request->getAttribute('validator');
        $resolveReferences = $validator->getParameter('resolveReferences')->isNumber()->setDefault(1)->getValue();
        $input = Validator::input()->isJson()->assertValid()->getValue();
        $entity = new \BO\Zmsentities\Useraccount($input);
        $entity->testValid();

        \BO\Zmsdb\Connection\Select::getWriteConnection();
        $workstation = self::getLoggedInWorkstation($request, $entity, $resolveReferences);
        
        $message = Response\Message::create($request);
        $message->data = $workstation;

        $response = Render::withLastModified($response, time(), '0');
        $response = Render::withJson($response, $message->setUpdatedMetaData(), $message->getStatuscode());
        return $response;
    }

    public static function getLoggedInWorkstation($request, $entity, $resolveReferences)
    {
        Helper\UserAuth::testUseraccountExists($entity->getId());
        $useraccount = Helper\UserAuth::getVerifiedUseraccount($entity);
        Helper\UserAuth::testPasswordMatching($useraccount, $entity->password);
        
        $workstation = (new Helper\User($request, $resolveReferences))->readWorkstation($resolveReferences);
        Helper\User::testWorkstationIsOveraged($workstation);
             
        if (static::hasLoginHash($useraccount)) {
            (new Workstation)->writeEntityLogoutByName($useraccount->id);
            //important to get new authkey to workstation entity for setting to exception data
            $workstation = static::writeLoggedInWorkstation($useraccount, $resolveReferences);
            $exception = new \BO\Zmsapi\Exception\Useraccount\UserAlreadyLoggedIn();
            $exception->data['authkey'] = $workstation->authkey;
            throw $exception;
        }
        $workstation = static::writeLoggedInWorkstation($useraccount, $resolveReferences);
        return $workstation;
    }

    public static function hasLoginHash($useraccount)
    {
        $logInHash = (new Workstation)->readLoggedInHashByName($useraccount->id);
        return (null !== $logInHash);
    }

    protected static function writeLoggedInWorkstation($useraccount, $resolveReferences)
    {
        $workstation = (new Workstation)->writeEntityLoginByName(
            $useraccount->id,
            $useraccount->password,
            \App::getNow(),
            $resolveReferences
        );
        return $workstation;
    }
}
