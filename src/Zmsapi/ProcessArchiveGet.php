<?php
/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use BO\Slim\Render;

use BO\Zmsdb\Process;
use BO\Zmsentities\Process as ProcessEntity;

use BO\Zmsapi\Response\Message;
use BO\Zmsapi\Exception\Process\ProcessArchiveNotFound as ProcessArchiveNotFoundException;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class ProcessArchiveGet extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     * @throws ProcessArchiveNotFoundException
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $validator = $request->getAttribute('validator');
        $resolveReferences = $validator::param('resolveReferences')->isNumber()->setDefault(1)->getValue();

        if (!isset($args['id'])) {
            throw new ProcessArchiveNotFoundException();
        }
        $processArchiveId = $validator::value($args['id'])
            ->isString()
            ->isRequired()
            ->isSmallerThan(32)
            ->isBiggerThan(32)
            ->getValue();
        $accessRights = $validator::param('accessRights')
            ->isString()
            ->isBiggerThan(4)
            ->setDefault('basic')
            ->getValue();

        (new Helper\User($request, 2))->checkRights($accessRights);
        $processArchive = (new Process())->readByProcessArchiveId($processArchiveId, $resolveReferences);
        
        $this->testProcessArchive($processArchive);

        $message = Message::create($request);
        $message->data = $processArchive;

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message->setUpdatedMetaData(), $message->getStatuscode());
    }

    /**
     * @param ProcessEntity|Null $entity
     *
     * @throws ProcessArchiveNotFoundException
     */
    protected function testProcessArchive(?ProcessEntity $entity)
    {
        if (! $entity) {
            throw new ProcessArchiveNotFoundException();
        }
    }
}
