<?php

/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use App;
use Exception;
use BO\Slim\Render;
use BO\Zmsapi\Exception\BadRequest as BadRequestException;
use BO\Zmsapi\Exception\Scope\ScopeNotFound as ScopeNotFoundException;
use BO\Zmsentities\Helper\DateTime as DateTimeHelper;
use BO\Zmsentities\Scope as ScopeEntity;
use BO\Zmsdb\Scope as ScopeRepository;
use BO\Zmsdb\Slot as SlotRepository;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @SuppressWarnings(Coupling)
 */
class SlotListByScopeAndDate extends BaseController
{
    /**
     * @var null|ScopeRepository
     */
    protected $scopeRepository = null;

    /**
     * @var null|SlotRepository
     */
    protected $slotRepository = null;


    /**
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     * @throws ScopeNotFound
     * @throws ScopeNotFoundException
     * @throws PreConditionFailedException
     * @throws Exception
     *
     * @SuppressWarnings(Param)
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        (new Helper\User($request))->checkRights('basic');

        $validator = $request->getAttribute("validator");
        $scopeId = $validator->value($args['id'])->isNumber()->getValue();
        $dateString = $validator->value($args['date'])->isDate('Y-m-d')->getValue();

        $this->scopeRepository = new ScopeRepository();
        $this->slotRepository = new SlotRepository();

        $this->testDateString($dateString);
        $scopeEntity = $this->readTestedScope($scopeId);

        $dateTime = (new DateTimeHelper($dateString))->modify(App::$now->format('H:i'));
        $slotList = $this->slotRepository->readByScopeAndDate(
            $scopeEntity,
            $dateTime
        );

        $message = Response\Message::create($request);
        $message->data = $slotList;

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message);
    }

    /**
     * @param int|null $scopeId
     *
     * @return void
     * @throws ScopeNotFoundException
     */
    protected function readTestedScope(int $scopeId = null): ScopeEntity
    {
        $scopeEntity = $this->scopeRepository->readEntity($scopeId);
        if (!$scopeId || !$scopeEntity) {
            throw new ScopeNotFoundException();
        }
        return $scopeEntity;
    }

    /**
     * @param string|null $dateString
     *
     * @return void
     * @throws PreConditionFailedException
     */
    protected function testDateString(string $dateString = null): void
    {
        if (!$dateString) {
            throw new BadRequestException("Invalid url parameter");
        }
    }
}
