<?php
/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use App;
use BO\Mellon\Validator;
use BO\Slim\Render;

use BO\Zmsapi\Exception\Mail\MailSenderFromMissing;
use BO\Zmsapi\Exception\Process\EmailRequired as EmailRequiredException;
use BO\Zmsapi\Exception\Process\ProcessNotFound as ProcessNotFoundException;

use BO\Zmsdb\Config as ConfigRepository;
use BO\Zmsdb\Connection\Select as DBConnection;
use BO\Zmsdb\Exception\Mail\ClientWithoutEmail;
use BO\Zmsdb\Exception\Pdo\PDOFailed as PDOFailedException;
use BO\Zmsdb\Helper\MailService;
use BO\Zmsdb\Log as LogRepository;
use BO\Zmsdb\Mail as MailRepository;
use BO\Zmsdb\Process as ProcessRepository;
use BO\Zmsdb\Department as DepartmentRepository;

use BO\Zmsentities\Exception\TemplateNotFound;
use BO\Zmsentities\Mail as MailEntity;
use BO\Zmsentities\Process as ProcessEntity;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @SuppressWarnings(Coupling)
 */
class ProcessDeleteMail extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     * @throws ClientWithoutEmail
     * @throws EmailRequiredException
     * @throws PDOFailedException
     * @throws ProcessNotFoundException
     * @throws TemplateNotFound
     * @throws MailSenderFromMissing
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        DBConnection::getWriteConnection();

        /** @var Validator $validator */
        $validator = $request->getAttribute('validator');
        $input = $validator::input()->isJson()->assertValid()->getValue();
        $process = new ProcessEntity($input);
        
        $process->testValid();
        $this->testProcessData($process);

        $mail = $this->writeMail($process);

        $message = Response\Message::create($request);
        $message->data = $mail;

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message);
    }

    /**
     * @param ProcessEntity $process
     *
     * @return MailEntity
     * @throws ClientWithoutEmail
     * @throws MailSenderFromMissing
     * @throws TemplateNotFound
     */
    protected static function writeMail(ProcessEntity $process): MailEntity
    {
        $processList = ProcessConfirmationMail::getProcessListOverview($process);
        $mailService = new MailService(new MailRepository(), new ConfigRepository(), new DepartmentRepository());
        $mailService->setProcessList($processList);
        $mail = $mailService->readResolvedEntity($mailService::MAIL_TYPE_DELETE);
        if (!$process->scope->hasEmailFrom()) {
            return $mail;
        }

        try {
            $mailService->writeMailInQueue($mail, App::getNow(), false);
        } catch (\Exception $exception) {
            $email = $process->getFirstClient()->email;
            if ($exception instanceof \BO\Zmsdb\Exception\Mail\RestrictedMail) {
                LogRepository::writeLogEntry(
                    "Failed to write email to ". trim($email) .' - mail restricted',
                    $mail->process->getId()
                );
            }
        }

        return $mail;
    }

    /**
     * @throws ProcessNotFoundException
     * @throws EmailRequiredException
     */
    protected function testProcessData($process)
    {
        $authCheck = (new ProcessRepository())->readAuthKeyByProcessId($process->getId());
        if (! $authCheck) {
            throw new ProcessNotFoundException();
        } elseif ($process->toProperty()->scope->preferences->client->emailRequired->get() &&
            ! $process->getFirstClient()->hasEmail()
        ) {
            throw new EmailRequiredException();
        }
    }
}
