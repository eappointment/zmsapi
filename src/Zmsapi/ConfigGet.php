<?php
/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use BO\Slim\Render;
use BO\Mellon\Validator;
use BO\Zmsdb\Config as Query;
use BO\Zmsapi\Helper\User;

class ConfigGet extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @return String
     */
    public function readResponse(
        \Psr\Http\Message\RequestInterface $request,
        \Psr\Http\Message\ResponseInterface $response,
        array $args
    ) {
        try {
            (new User($request))->checkRights('basic');
        } catch (\Exception $exception) {
            $token = $request->getHeader('X-Token');
            if (\App::SECURE_TOKEN != current($token)) {
                throw new Exception\Config\ConfigAuthentificationFailed();
            }
        }

        $configRepository = new Query();
        $withDescriptions = Validator::param('withDescriptions')->isNumber()->setDefault(0)->getValue();
        $property = (array_key_exists('property', $args)) ? $args['property'] : '';
        $isGroup = (strpos($property, '__') === false);

        if ($property !== '') {
            $config = ($isGroup) ?
                $configRepository->readPropertyGroup($property) :
                $configRepository->readProperty($property);
        } else {
            $config = $configRepository->readEntity();
            if ($withDescriptions === 1) {
                $descriptions = $configRepository->readDescriptions();
                $config['descriptions'] = $descriptions;
            }
        }
        
        $message = Response\Message::create($request);
        $message->data = $config;

        $response = Render::withLastModified($response, time(), '0');
        $response = Render::withJson($response, $message, $message->getStatuscode());
        return $response;
    }
}
