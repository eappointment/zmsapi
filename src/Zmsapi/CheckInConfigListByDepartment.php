<?php

declare(strict_types=1);

namespace BO\Zmsapi;

use BO\Slim\Render;
use BO\Zmsapi\Exception\CheckInConfig\EntryNotFound;
use BO\Zmsdb\CheckInConfig as Repository;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class CheckInConfigListByDepartment extends BaseController
{
    /**
     * @var Repository
     */
    private $repository;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->repository = new Repository();
        if ($container->has('zmsdb.repositories.check-in-config')) {
            $this->repository = $container->get('zmsdb.repositories.check-in-config');
        }
    }

    /**

     * @throws EntryNotFound
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        (new Helper\User($request))->checkRights('scope');

        $department    = Helper\User::checkDepartment($args['id']);
        $message       = Response\Message::create($request);
        $message->data = $this->repository->listByDepartmentId((int) $department->getId());

        $response = Render::withLastModified($response, time());
        return Render::withJson($response, $message);
    }
}
