<?php
namespace BO\Zmsapi;

use BO\Slim\Render;

use BO\Zmsapi\Exception\Apikey\ApiKeyLocked;
use BO\Zmsapi\Exception\Apikey\ApiKeyNotValid as ApiKeyNotValidException;
use BO\Zmsapi\Exception\Apikey\AccessDenied as AccessDeniedException;
use BO\Zmsapi\Helper\ApikeyService;

use BO\Zmsdb\Exception\Pdo\DeadLockFound;
use BO\Zmsdb\Exception\Pdo\LockTimeout;
use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsdb\Apikey as ApikeyRepository;

use BO\Zmsentities\Calendar as CalendarEntity;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @SuppressWarnings(Coupling)
 */
class ApikeyCalendarSet extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface $response
     * @throws AccessDeniedException
     * @throws ApiKeyLocked
     * @throws ApiKeyNotValidException
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws PDOFailed
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $validator = $request->getAttribute('validator');
        $key = isset($args['key']) ? $validator::value($args['key'])->isString()->isRequired()->getValue() : null;
        $token = $validator->getParameter('token')->isString()->isRequired()->getValue();
        $slotsRequired = $validator->getParameter('slotsRequired')->isNumber()->setDefault(0)->getValue();

        $apikeyService = new ApikeyService($key, $token, true);
        $apikeyService->testSession();

        $input = $validator->getInput()->isJson()->assertValid()->getValue();
        $calendar = (new CalendarEntity())->addData($input);
        (new ApikeyRepository())->writeCalendar($apikeyService->getApikey()->getId(), $calendar, $slotsRequired);
        (new ApikeyRepository())->withUpdatedLockedStatus($apikeyService->getApikey()->getId());

        $message = Response\Message::create($request);
        $message->data = $calendar;

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message->setUpdatedMetaData(), $message->getStatuscode());
    }
}
