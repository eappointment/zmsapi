<?php
/**
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmsapi;

use BO\Slim\Render;
use BO\Zmsdb\MaintenanceSchedule as Repository;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class MaintenanceScheduleList extends BaseController
{
    /**
     * @var Repository
     */
    private $repository;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->repository = new Repository();
        if ($container->has('zmsdb.repositories.maintenance-schedule')) {
            $this->repository = $container->get('zmsdb.repositories.maintenance-schedule');
        }
    }

    /**
     * @SuppressWarnings(Param)
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ) {
        (new Helper\User($request))->checkRights('superuser');
        $scheduleList = $this->repository->readList();

        $message = Response\Message::create($request);
        $message->data = $scheduleList;

        $response = Render::withLastModified($response, time());
        return Render::withJson($response, $message);
    }
}
