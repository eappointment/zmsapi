<?php
/**
 * @copyright BerlinOnline GmbH
 */

declare(strict_types=1);

namespace BO\Zmsapi\Exception\CheckInConfig;

class EntryNotFound extends \Exception
{
    protected $code = 404;

    protected $message = 'Zu der angegebenen id konnte kein Eintrag gefunden werden.';
}
