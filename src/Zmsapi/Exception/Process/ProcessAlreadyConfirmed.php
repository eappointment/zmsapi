<?php

namespace BO\Zmsapi\Exception\Process;

use Exception;

class ProcessAlreadyConfirmed extends Exception
{
    protected $code = 404;

    protected $message = 'An appointment has already been confirmed and a further booking is not possible.';
}
