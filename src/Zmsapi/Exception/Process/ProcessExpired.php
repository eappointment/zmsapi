<?php

namespace BO\Zmsapi\Exception\Process;

/**
 * example class to generate an exception
 */
class ProcessExpired extends \Exception
{
    protected $code = 404;
    protected $message = 'Der Termin zu den angegebenen Daten liegt schon in der Vergangenheit';
}
