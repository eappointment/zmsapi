<?php

namespace BO\Zmsapi\Exception\Apikey;

class AccessDenied extends \Exception
{
    protected $code = 400;

    protected $message = 'Access to api denied';
}
