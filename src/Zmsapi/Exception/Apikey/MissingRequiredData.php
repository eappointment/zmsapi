<?php

namespace BO\Zmsapi\Exception\Apikey;

class MissingRequiredData extends \Exception
{
    protected $code = 404;

    protected $message = 'Data for processing is missing';
}
