<?php

namespace BO\Zmsapi\Exception\Apikey;

class ApiKeyNotValid extends \Exception
{
    protected $code = 400;

    protected $message = 'Apikey not valid, access denied';
}
