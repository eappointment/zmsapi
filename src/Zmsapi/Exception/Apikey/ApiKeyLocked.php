<?php

namespace BO\Zmsapi\Exception\Apikey;

class ApiKeyLocked extends \Exception
{
    protected $code = 423;

    protected $message = 'Apikey is locked';
}
