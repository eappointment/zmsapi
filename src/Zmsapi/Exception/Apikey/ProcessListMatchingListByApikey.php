<?php

namespace BO\Zmsapi\Exception\Apikey;

use Exception;

class ProcessListMatchingListByApikey extends Exception
{
    protected $code = 400;

    protected $message = 'Invalid Input';
}
