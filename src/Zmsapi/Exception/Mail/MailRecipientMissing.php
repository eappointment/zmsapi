<?php

namespace BO\Zmsapi\Exception\Mail;

/**
 * class to generate an exception if children exists
 */
class MailRecipientMissing extends \Exception
{
    protected $code = 404;

    protected $message = 'There is no recipient mail address specified or mail address is blacklisted';
}
