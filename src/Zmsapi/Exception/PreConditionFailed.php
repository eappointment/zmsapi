<?php

namespace BO\Zmsapi\Exception;

class PreConditionFailed extends \Exception
{
    protected $code = 412;

    protected $message = 'Necessary preconditions have not been met or required parameters are not valid.';
}
