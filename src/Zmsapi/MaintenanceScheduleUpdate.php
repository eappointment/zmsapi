<?php
/**
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmsapi;

use BO\Mellon\Unvalidated;
use BO\Mellon\ValidJson;
use BO\Slim\Render;
use BO\Zmsapi\Helper\Maintenance;
use BO\Zmsdb\Helper\MaintenanceUpdateProcess;
use BO\Zmsdb\MaintenanceSchedule as ScheduleRepository;
use BO\Zmsentities\Helper\DateTime;
use BO\Zmsentities\MaintenanceSchedule;
use BO\Zmsentities\Useraccount;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class MaintenanceScheduleUpdate extends BaseController
{
    /**
     * @var ScheduleRepository
     */
    private $scheduleRepo;

    /**
     * @var MaintenanceUpdateProcess
     */
    private $updateProcess;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->scheduleRepo = new ScheduleRepository();
        if ($container->has('zmsdb.repositories.maintenance-schedule')) {
            $this->scheduleRepo = $container->get('zmsdb.repositories.maintenance-schedule');
        }

        $configRepo = null;
        if ($container->has('zmsdb.repositories.config')) {
            $configRepo = $container->get('zmsdb.repositories.config');
        }

        $logger = null;
        if ($container->has('logger.default')) {
            $logger = $container->get('logger.default');
        }

        $this->updateProcess = new MaintenanceUpdateProcess($logger, $configRepo, $this->scheduleRepo);
    }

    /**
     * @SuppressWarnings(Param)
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ) {
        Helper\User::$request = $request;
        /** @var Useraccount $userAccount */
        $workstation = Helper\User::readWorkstation();
        $workstation->getUseraccount()->testRights(['superuser']);

        /** @var Unvalidated|ValidJson $validatorInput */
        $validatorInput = $request->getAttribute('validator')->getInput();
        $input = $validatorInput->isJson()->assertValid()->getValue();

        $entity = new MaintenanceSchedule($input);
        $entity->testValid();
        $entity->setCreatorId((int) $workstation->getId());
        $entity->setCreationDateTime(DateTime::create());

        if ((int) $entity->getId() === 0) {
            return $response->withStatus(StatusCodeInterface::STATUS_NOT_FOUND);
        }

        Maintenance::testValidSchedule($entity);

        $scheduledMaintenance = $this->scheduleRepo->updateEntity($entity);
        if ($scheduledMaintenance === null) {
            return $response->withStatus(StatusCodeInterface::STATUS_INTERNAL_SERVER_ERROR, 'HTTP Internal Error');
        }

        $this->updateProcess->startProcessing();

        $message = Response\Message::create($request);
        $message->data = $scheduledMaintenance;

        $response = Render::withLastModified($response, time());
        return Render::withJson($response, $message->setUpdatedMetaData());
    }
}
