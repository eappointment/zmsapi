<?php
/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use BO\Slim\Render;

use BO\Mellon\Validator;

use BO\Zmsdb\Process as Query;
use BO\Zmsdb\ProcessStatusQueued;
use BO\Zmsdb\Workstation as WorkstationRepository;
use BO\Zmsdb\Cluster as ClusterRepository;
use BO\Zmsdb\Connection\Select as DBConnection;

use BO\Zmsapi\Exception\Process\ProcessNotFound as ProcessNotFoundException;
use BO\Zmsapi\Exception\Process\ProcessInvalid as ProcessInvalidException;
use BO\Zmsapi\Exception\Process\AuthKeyMatchFailed as AuthKeyMatchFailedException;
use BO\Zmsapi\Exception\Workstation\WorkstationHasAssignedProcess as WorkstationHasAssignedProcessException;

use BO\Zmsentities\Process as Entity;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @SuppressWarnings(Coupling)
 */
class ProcessPickup extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $workstation = (new Helper\User($request))->checkRights();
        $input = Validator::input()->isJson()->assertValid()->getValue();
        $entity = new Entity($input);
        DBConnection::getWriteConnection();

        $process = $this->readValidProcess($workstation, $entity, $input);
        $this->testProcessAccess($workstation, $process);
        (new WorkstationRepository)->writeAssignedProcess($workstation, $process, \App::$now);

        $message = Response\Message::create($request);
        $message->data = (new Query)->readEntity($process->id, $process->authKey);

        $response = Render::withLastModified($response, time(), '0');
        $response = Render::withJson($response, $message->setUpdatedMetaData(), $message->getStatuscode());
        return $response;
    }

    protected function testProcessData($entity)
    {
        $entity->testValid();
        $authCheck = (new Query())->readAuthKeyByProcessId($entity->id);
        if (! $authCheck) {
            throw new ProcessNotFoundException();
        } elseif ($authCheck['authKey'] != $entity->authKey && $authCheck['authName'] != $entity->authKey) {
            throw new AuthKeyMatchFailedException();
        }
    }

    protected function testProcessAccess($workstation, $process)
    {
        $cluster = (new ClusterRepository)->readByScopeId($workstation->scope['id'], 1);
        $workstation->testMatchingProcessScope($workstation->getScopeList($cluster), $process);
        if ($workstation->process && $workstation->process->hasId() && $workstation->process->id != $process->id) {
            $exception = new WorkstationHasAssignedProcessException();
            $exception->data = [
                'process' => $workstation->process
            ];
            throw $exception;
        }
    }

    protected function readValidProcess($workstation, $entity, $input)
    {
        if ($entity->hasProcessCredentials()) {
            $this->testProcessData($entity);
            $entity->addData($input);
            $process = (new Query())->updateEntity($entity, \App::$now);
        } elseif ($entity->hasQueueNumber()) {
            // Allow waitingnumbers over 1000 with the fourth parameter
            $process = ProcessStatusQueued::init()
                ->readByQueueNumberAndScope($entity['queue']['number'], $workstation->scope['id'], 0, 100000000);
            if (! $process->id) {
                $workstation = (new WorkstationRepository())->readResolvedReferences($workstation, 1);
                $process = (new Query())->writeNewPickup($workstation->scope, \App::$now, $entity['queue']['number']);
            }
            $process->testValid();
        } else {
            $entity->testValid();
            throw new ProcessInvalidException();
        }
        return $process;
    }
}
