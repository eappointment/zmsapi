<?php
/**
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmsapi;

use BO\Mellon\Validator;
use BO\Slim\Render;
use BO\Zmsdb\Helper\MaintenanceUpdateProcess;
use BO\Zmsdb\MaintenanceSchedule as ScheduleRepository;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class MaintenanceScheduleDelete extends BaseController
{
    /**
     * @var ScheduleRepository
     */
    private $scheduleRepo;

    /**
     * @var MaintenanceUpdateProcess
     */
    private $updateProcess;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->scheduleRepo = new ScheduleRepository();
        if ($container->has('zmsdb.repositories.maintenance-schedule')) {
            $this->scheduleRepo = $container->get('zmsdb.repositories.maintenance-schedule');
        }

        $configRepo = null;
        if ($container->has('zmsdb.repositories.config')) {
            $configRepo = $container->get('zmsdb.repositories.config');
        }

        $logger = null;
        if ($container->has('logger.default')) {
            $logger = $container->get('logger.default');
        }

        $this->updateProcess = new MaintenanceUpdateProcess($logger, $configRepo, $this->scheduleRepo);
    }

    /**
     * @SuppressWarnings(Param)
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ) {
        $identifier = Validator::value($args['id'])->isNumber()->getValue();
        if ($identifier === null) {
            return $response->withStatus(StatusCodeInterface::STATUS_BAD_REQUEST, 'HTTP Bad Request');
        }

        $this->scheduleRepo->deleteEntity($identifier);
        $this->updateProcess->startProcessing();

        $message = Response\Message::create($request);
        $message->data = [];

        $response = Render::withLastModified($response, time());
        return Render::withJson($response, $message);
    }
}
