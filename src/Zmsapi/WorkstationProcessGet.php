<?php

/**
 * @package ZMS API
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi;

use BO\Slim\Render;
use BO\Zmsapi\Exception\Process\ProcessNotFound;
use BO\Zmsdb\Helper\NoAuth;
use BO\Zmsdb\Process as ProcessRepository;
use BO\Zmsdb\Scope as ScopeRepository;
use BO\Zmsdb\Cluster as ClusterRepository;
use BO\Zmsentities\Exception\WorkstationProcessMatchScopeFailed;
use BO\Zmsentities\Workstation as WorkstationEntity;
use BO\Zmsentities\Process as ProcessEntity;
use BO\Zmsentities\Collection\ScopeList;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @suppressWarnings(coupling)
 */
class WorkstationProcessGet extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @param array $args
     * @return ResponseInterface
     * @throws WorkstationProcessMatchScopeFailed|ProcessNotFound
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $validator = $request->getAttribute('validator');
        $selectedScopeId = $validator->getParameter('selectedscope')->isNumber()->getValue();
        $workstation = (new Helper\User($request))->checkRights();
        $processRepository = new ProcessRepository();
        $process = $processRepository->readEntity($args['id'], (new NoAuth()));
        $this->testProcessAccess($process, $workstation, $selectedScopeId);

        $message = Response\Message::create($request);
        $message->data = $process;

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $message->setUpdatedMetaData(), $message->getStatuscode());
    }

    /**
     * @throws WorkstationProcessMatchScopeFailed|ProcessNotFound
     */
    protected function testProcessAccess(ProcessEntity $process, WorkstationEntity $workstation, $selectedScopeId): void
    {
        if (! $process->hasId()) {
            throw new ProcessNotFound();
        }
        if ($workstation->hasId() && ! $workstation->hasSuperUseraccount()) {
            $scopeList = new ScopeList();
            $scopeId = $selectedScopeId ?: $workstation->getScope()->getId();
            if ($workstation->isClusterEnabled()) {
                $cluster = (new ClusterRepository())->readByScopeId($scopeId, 1);
                $scopeList->addList($workstation->getScopeList($cluster));
            } elseif ($scopeId) {
                $scope = (new ScopeRepository())->readEntity($scopeId);
                $scopeList->addEntity($scope);
            }
            try {
                $workstation->testMatchingProcessScope($scopeList, $process);
            } catch (WorkstationProcessMatchScopeFailed $exception) {
                $exception->data['clusterEnabled'] = $workstation->isClusterEnabled();
                throw $exception;
            }
        }
    }
}
