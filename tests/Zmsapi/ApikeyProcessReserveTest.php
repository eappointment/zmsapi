<?php

namespace BO\Zmsapi\Tests;

use BO\Zmsdb\Exception\Pdo\DeadLockFound;
use BO\Zmsdb\Exception\Pdo\LockTimeout;
use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsdb\Session as SessionRepository;

use BO\Zmsentities\Process as ProcessEntity;
use BO\Zmsentities\Collection\ProcessList;
use BO\Zmsentities\Session as SessionEntity;
use Exception;

/**
 *
 * @SuppressWarnings(public)
 */
class ApikeyProcessReserveTest extends Base
{
    protected $classname = "ApikeyProcessReserve";

    public function testRendering()
    {
        $apikey = $this->setApikeyWithSession();
        $processList = new ProcessList(
            json_decode($this->readFixture("GetFreeProcessList.json"))
        );
        $process = $processList->getFirst();
        $process->appointments[0]->slotCount = 3;
        $response = $this->render(['key' => $apikey->getId()], [
            '__body' => json_encode($process),
        ], [], 'POST');

        $this->assertStringContainsString('"slotCount":"1"', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
    }

    public function testInvalidInput()
    {
        $this->expectException('BO\Mellon\Failure\Exception');
        $this->render([], [
            '__body' => '',
        ], [], 'POST');
    }

    /**
     * this test should not book a new appointment, but replace the existing one with the same
     * services and location with the new date, but retain the credentials
     *
     * @throws Exception
     */
    public function testWithClientkeyAlreadyReserved()
    {
        $apikey = $this->setApikeyWithSession();

        //get list with free processes
        $processList = new ProcessList(
            json_decode($this->readFixture("GetFreeProcessList.json"), 1)
        );
        $firstProcess = $processList->getFirst();
        $newProcess = $processList->getLast();

        //handle first process to be reserved
        $response1 = $this->render(['key' => $apikey->getId()], [
            '__body' => json_encode($firstProcess)
        ], [], 'POST');
        //check first appointment date
        self::assertStringContainsString('"date":1464340800', (string)$response1->getBody());
        //get reserved process from previous repsonse
        $reservedProcess = new ProcessEntity(json_decode($response1->getBody(), 1)['data']);

        //handle next free process
        $response2 = $this->render(['key' => $apikey->getId()], [
            '__body' => json_encode($newProcess),
        ], [], 'POST');

        //there should be a new date
        self::assertStringContainsString('"date":1464342000', (string)$response2->getBody());
        //but same credentials as before
        self::assertStringContainsString(
            '"id":"'. $reservedProcess->getId() .'","processArchiveId":"'. $reservedProcess->processArchiveId .'"',
            (string)$response2->getBody()
        );
        self::assertTrue(200 == $response2->getStatusCode());
    }

    /**
     * @throws PDOFailed
     * @throws DeadLockFound
     * @throws LockTimeout
     */
    public function testWithClientkeyConfirmed()
    {
        $this->expectException('BO\Zmsapi\Exception\Process\ProcessAlreadyConfirmed');
        $this->expectExceptionCode(404);

        $apikey = $this->setApikeyWithSession();
        $processList = new ProcessList(
            json_decode($this->readFixture("GetFreeProcessList.json"))
        );
        $processListConfirmed = new ProcessList(
            json_decode($this->readFixture("GetConfirmedProcessList.json"))
        );

        //write session
        $session = new SessionEntity();
        $session->name = 'apikey';
        $session->id = $apikey->getId();
        $session->testValid();
        $session->content['processList'] = $processListConfirmed;
        (new SessionRepository())->updateEntity($session);

        $this->render(['key' => $apikey->getId()], [
            '__body' => json_encode($processList->getFirst())
        ], [], 'POST');
    }

    public function testWithClientkeyInvalid()
    {
        $this->expectException('BO\Zmsapi\Exception\Apikey\ApiKeyNotValid');
        $this->expectExceptionCode(400);
        $processList = new ProcessList(
            json_decode($this->readFixture("GetFreeProcessList.json"))
        );
        $process = $processList->getFirst();
        $this->render(['key' => '__invalid'], [
            '__body' => json_encode($process)
        ], [], 'POST');
    }
}
