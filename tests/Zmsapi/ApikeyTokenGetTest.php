<?php

namespace BO\Zmsapi\Tests;

use BO\Zmsdb\Exception\Pdo\DeadLockFound;
use BO\Zmsdb\Exception\Pdo\LockTimeout;
use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsdb\Useraccount as UseraccountRepository;
use BO\Zmsdb\Query\Useraccount as UseraccountQuery;
use BO\Zmsdb\Query\Base as BaseQuery;

class ApikeyTokenGetTest extends Base
{
    protected $classname = "ApikeyTokenGet";

    const API_TOKEN = 'wMdVa5Nu1seuCRSJxhKl2M3yw8zqaAilPH2Xc2IZs';
    const USER = 'testadmin';

    /**
     * @throws PDOFailed
     * @throws DeadLockFound
     * @throws LockTimeout
     */
    public function testRendering()
    {
        $query = new UseraccountQuery(BaseQuery::UPDATE);
        $query->addConditionLoginName(self::USER);
        $query->addValues(['apitoken' => self::API_TOKEN]);
        (new UseraccountRepository())->writeItem($query);

        $response = $this->render([], ['token' => self::API_TOKEN], []);
        $this->assertStringContainsString('apikey.json', (string)$response->getBody());
        $this->assertStringContainsString('"accesslevel":"public"', (string)$response->getBody());
        $this->assertStringContainsString('"remotecall":"1"', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
    }

    public function testMissingUseraccountApiToken()
    {
        $this->expectException('BO\Zmsapi\Exception\Apikey\ApiKeyNotValid');
        $this->render([], ['token' => self::API_TOKEN], []);
    }

    public function testInvalidToken()
    {
        $this->expectException('BO\Zmsapi\Exception\Apikey\ApiKeyNotValid');
        $this->render([], ['token' => '123456'], []);
    }

    public function testAccessDeniedMissingToken()
    {
        $this->expectException('BO\Zmsapi\Exception\Apikey\AccessDenied');
        $this->render([], [], []);
    }
}
