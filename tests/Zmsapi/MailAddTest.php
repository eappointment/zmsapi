<?php

namespace BO\Zmsapi\Tests;

use BO\Mellon\ValidMail;

class MailAddTest extends Base
{
    protected $classname = "MailAdd";

    public function testRendering()
    {
        $this->setWorkstation();

        $tmp = ValidMail::$disableDnsChecks;
        ValidMail::$disableDnsChecks = true;

        $response = $this->render([], [
            '__body' => $this->readFixture('GetMail.json')
        ], [], 'POST');

        ValidMail::$disableDnsChecks = $tmp;

        $this->assertStringContainsString('mail.json', (string)$response->getBody());
        $this->assertStringNotContainsString('id:0', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());

        return $response;
    }

    public function testEmpty()
    {
        $this->setWorkstation();
        $this->expectException('\BO\Mellon\Failure\Exception');
        $this->render([], [], [], 'POST');
    }

    public function testMissingMail()
    {
        $this->setWorkstation();
        $this->expectException('\BO\Zmsdb\Exception\Mail\ClientWithoutEmail');
        $this->expectExceptionCode(404);
        $this->render([], [
            '__body' => '{
              "createIP": "145.15.3.10",
              "createTimestamp": 1447931596000,
              "multipart": [
                {
                  "queueId": "1234",
                  "mime": "text/html",
                  "content": "<h1>Title</h1><p>Message</p>",
                  "base64": false
                },
                {
                  "queueId": "1234",
                  "mime": "text/plain",
                  "content": "Title\nMessage",
                  "base64": false
                }
              ],
              "process": {
                "clients": [
                    {
                        "familyName": "Max Mustermann",
                        "email": "",
                        "telephone": "030 115"
                    }
                ],
                "id": 10029,
                "authKey": "1c56",
                "reminderTimestamp": 1447931730000,
                "scope": {
                    "id": 151
                },
                "status": "confirmed"
              },
              "subject": "Example Mail"
            }'
        ], [], 'POST');
    }
}
