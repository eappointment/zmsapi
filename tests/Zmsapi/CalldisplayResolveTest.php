<?php

namespace BO\Zmsapi\Tests;

use BO\Zmsdb\ApplicationRegister as RegisterRepository;
use BO\Zmsentities\ApplicationRegister;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

class CalldisplayResolveTest extends Base
{
    use ProphecyTrait;

    protected $classname = "CalldisplayResolve";

    public function testRendering()
    {
        $repoProphecy = $this->prophesize(RegisterRepository::class);
        $repoProphecy
            ->readEntity('hbw5fe802ecabdb017449b966f9462e8e588gFMw')
            ->shouldBeCalled()
            ->willReturn(null);
        \App::$slim->getContainer()->offsetSet('zmsdb.repositories.application-register', $repoProphecy->reveal());

        $this->setWorkstation();
        $response = $this->render([], [
            '__body' => '{
                "scopes": [
                    {
                      "id": 143
                    }
                ],
                "clusters": [
                    {
                      "id": 109
                    }
                ],
                "organisation": {
                    "id": 123
                },
                "contact": {
                    "name": "Bürgeramt"
                },
                "hash": "hbw5fe802ecabdb017449b966f9462e8e588gFMw"
            }'
        ], [], 'POST');
        $this->assertTrue(200 == $response->getStatusCode());
        $this->assertStringContainsString('calldisplay.json', (string)$response->getBody());
        $this->assertStringContainsString('daysActive', (string)$response->getBody());
    }

    public function testEmpty()
    {
        $this->setWorkstation();
        $this->expectException('\BO\Mellon\Failure\Exception');
        $this->render([], [], []);
    }

    public function testClusterNotFound()
    {
        $this->expectException('\BO\Zmsapi\Exception\Cluster\ClusterNotFound');
        $this->expectExceptionCode(404);
        $this->render([], [
            '__body' => '{
                "clusters": [
                    {
                      "id": 999
                    }
                ]
            }'
        ], [], 'POST');
        $this->assertStringContainsString('queue.json', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
    }

    public function testScopeNotFound()
    {
        $this->expectException('\BO\Zmsapi\Exception\Scope\ScopeNotFound');
        $this->expectExceptionCode(404);
        $this->render([], [
            '__body' => '{
                "scopes": [
                    {
                      "id": 999
                    }
                ]
            }'
        ], [], 'POST');
    }

    public function testNotFoundClusterOrScopeLists()
    {
        $this->setWorkstation();
        $this->expectException('\BO\Zmsapi\Exception\Calldisplay\ScopeAndClusterNotFound');
        $this->expectExceptionCode(404);
        $this->render([], [
            '__body' => '{
                "organisation": {
                    "id": 123
                },
                "contact": {
                    "name": "Bürgeramt"
                }
            }'
        ], [], 'POST');
    }
}
