<?php

namespace BO\Zmsapi\Tests;

class SessionUpdateTest extends Base
{
    protected $classname = "SessionUpdate";

    public function testRendering()
    {
        $response = $this->render([], [
            '__body' => '{
                "id": "unittest",
                "name": "unittest"
            }',
        ], [], 'POST');
        $this->assertStringContainsString('session.json', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
    }

    public function testOidcSessionContent()
    {
        $response = $this->render([], [
            'oidc' => 1,
            '__body' => '{
                "id": "unittest",
                "name": "unittest",
                "content": {
                    "access_token": "{}"
                }
            }',
        ], [], 'POST');
        $this->assertStringContainsString('access_token', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
    }

    public function testIsDifferent()
    {
        $response = $this->render([], [
            '__body' => '{
                "id": "unittest",
                "name": "unittest",
                "content": {
                    "basket": {
                        "providers" : "999999999"
                    },
                    "entry": {
                        "providers": "122222"
                    },
                    "error": "isDifferent"
                }
            }',
        ], [], 'POST');
        $this->assertStringContainsString('session.json', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
    }

    public function testEmpty()
    {
        $this->expectException('BO\Mellon\Failure\Exception');
        $this->render([], [
            '__body' => '',
        ], [], 'POST');
    }

    public function testUnvalidInput()
    {
        $this->expectException('\BO\Zmsentities\Exception\SchemaValidation');
        $this->expectExceptionCode(400);
        $this->render([], [
            '__body' => '{}'
        ], [], 'POST');
    }

    public function testUnknownRequest()
    {
        $this->expectException('\BO\Zmsapi\Exception\Matching\RequestNotFound');
        $this->render([], [
            '__body' => '{
                "id": "unittest",
                "name": "unittest",
                "content": {
                    "basket": {
                        "requests" : "999999999"
                    }
                }
        }',
        ], [], 'POST');
    }

    public function testUnknownProvider()
    {
        $this->expectException('\BO\Zmsapi\Exception\Matching\ProviderNotFound');
        $this->render([], [
            '__body' => '{
                "id": "unittest",
                "name": "unittest",
                "content": {
                    "basket": {
                        "providers" : "999999999"
                    }
                }
        }',
        ], [], 'POST');
    }

    public function testNotMatching()
    {
        $this->expectException('\BO\Zmsapi\Exception\Matching\MatchingNotFound');
        $this->render([], [
            '__body' => '{
                "id": "unittest",
                "name": "unittest",
                "content": {
                    "basket": {
                        "requests" : "120703",
                        "providers" : "122222"
                    }
                }
        }',
        ], [], 'POST');
    }

    public function testMatchingRequestWithProviderFromScope()
    {
        $response = $this->render([], [
            '__body' => '{
                "id": "unittest",
                "name": "unittest",
                "content": {
                    "basket": {
                        "requests" : "120703",
                        "scope" : "141"
                    }
                }
        }',
        ], [], 'POST');
        $this->assertStringContainsString('session.json', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
    }
}
