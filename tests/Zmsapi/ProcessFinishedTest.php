<?php

namespace BO\Zmsapi\Tests;

use BO\Mellon\ValidMail;
use BO\Zmsdb\Mail;

class ProcessFinishedTest extends Base
{
    protected $classname = "ProcessFinished";

    public function testRendering()
    {
        $workstation = $this->setWorkstation(138, 'berlinonline', 141);
        $workstation['queue']['clusterEnabled'] = 1;

        $entity = (new \BO\Zmsdb\Process)->readEntity(10030, new \BO\Zmsdb\Helper\NoAuth);
        $this->assertEquals('confirmed', $entity->status);

        $process = json_decode($this->readFixture("GetProcess_10030.json"));
        $process->status = 'finished';

        $response = $this->render([], [
            '__body' => json_encode($process)
        ], [], 'POST');

        $this->assertTrue(200 == $response->getStatusCode());

        $entity = (new \BO\Zmsdb\Process)->readEntity($process->id, new \BO\Zmsdb\Helper\NoAuth);
        $this->assertEquals('blocked', $entity->status);
    }

    public function testWithSurvey()
    {
        $workstation = $this->setWorkstation(138, 'berlinonline', 141);
        $workstation['queue']['clusterEnabled'] = 1;

        $entity = (new \BO\Zmsdb\Process)->readEntity(10030, new \BO\Zmsdb\Helper\NoAuth);
        $this->assertEquals('confirmed', $entity->status);

        $process = json_decode($this->readFixture("GetProcess_10030.json"));
        $process->status = 'finished';

        $tmp = ValidMail::$disableDnsChecks;
        ValidMail::$disableDnsChecks = true;

        $response = $this->render([], [
            'survey' => 1,
            '__body' => json_encode($process)
        ], [], 'POST');

        ValidMail::$disableDnsChecks = true;

        $this->assertTrue(200 == $response->getStatusCode());
        $mailQueue = (new Mail())->readList();
        self::assertEquals('Kundenbefragung des Standorts Bürgeramt Heerstraße', $mailQueue->getFirst()->subject);
        self::assertEquals(1, $mailQueue->count());
    }

    public function testWithoutSurvey()
    {
        $workstation = $this->setWorkstation(138, 'berlinonline', 141);
        $workstation['queue']['clusterEnabled'] = 1;

        $entity = (new \BO\Zmsdb\Process)->readEntity(10030, new \BO\Zmsdb\Helper\NoAuth);
        $this->assertEquals('confirmed', $entity->status);

        $process = json_decode($this->readFixture("GetProcess_10030.json"));
        $process->status = 'finished';

        $response = $this->render([], [
            'survey' => 0,
            '__body' => json_encode($process)
        ], [], 'POST');

        $this->assertTrue(200 == $response->getStatusCode());
        $mailQueue = (new Mail())->readList();
        self::assertEquals(0, $mailQueue->count());
    }

    public function testRenderingPending()
    {
        $workstation = $this->setWorkstation(138, 'berlinonline', 141);
        $workstation['queue']['clusterEnabled'] = 1;

        $process = json_decode($this->readFixture("GetProcess_10030.json"));
        $process->status = 'pending';
        $response = $this->render([], [
            '__body' => json_encode($process)
        ], [], 'POST');

        $this->assertStringContainsString('"status":"pending"', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
        
        $entity = (new \BO\Zmsdb\Process)->readEntity($process->id, new \BO\Zmsdb\Helper\NoAuth);
        $this->assertEquals('pending', $entity->status);
    }

    public function testUnvalidCredentials()
    {
        $this->setWorkstation();
        $this->expectException('\BO\Zmsapi\Exception\Process\ProcessInvalid');
        $this->expectExceptionCode(400);
        $this->render([], [
            '__body' => $this->readFixture("GetProcess_10030.json")
        ], [], 'POST');
    }

    public function testNoAccess()
    {
        $this->expectException('\BO\Zmsentities\Exception\WorkstationProcessMatchScopeFailed');
        $this->expectExceptionCode(403);

        $workstation = $this->setWorkstation(138, 'berlinonline', 141);
        $workstation['queue']['clusterEnabled'] = 1;
        $workstation->process = json_decode($this->readFixture("GetProcess_10030.json"));
        $process = json_decode($this->readFixture("GetProcess_10029.json"));
        $process->status = 'finished';
        $this->render([], [
            '__body' => json_encode($process)
        ], [], 'POST');
    }

    public function testUnvalidInput()
    {
        $this->setWorkstation();
        $this->expectException('\BO\Zmsentities\Exception\SchemaValidation');
        $this->expectExceptionCode(400);
        $this->render([], [
            '__body' => '{
                "status": "confirmed"
            }'
        ], [], 'POST');
    }

    public function testProcessNotFound()
    {
        $this->setWorkstation();
        $this->expectException('\BO\Zmsapi\Exception\Process\ProcessNotFound');
        $this->expectExceptionCode(404);
        $this->render([], [
            '__body' => '{
                "id": 123456,
                "authKey": "abcd",
                "status": "finished",
                "amendment": "Beispiel Termin"
            }'
        ], [], 'POST');
    }

    public function testAuthKeyMatchFailed()
    {
        $this->setWorkstation();
        $this->expectException('\BO\Zmsapi\Exception\Process\AuthKeyMatchFailed');
        $this->expectExceptionCode(403);
        $this->render([], [
            '__body' => '{
                "id": 10029,
                "authKey": "abcd",
                "status": "finished",
                "amendment": "Beispiel Termin"
            }'
        ], [], 'POST');
    }
}
