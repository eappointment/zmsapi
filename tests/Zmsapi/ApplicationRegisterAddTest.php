<?php

declare(strict_types=1);

namespace BO\Zmsapi\Tests;

use BO\Zmsdb\ApplicationRegister as RegisterRepository;

class ApplicationRegisterAddTest extends Base
{
    protected $classname = "ApplicationRegisterAdd";

    public function testRendering()
    {
        $repo = new RegisterRepository();
        \App::$slim->getContainer()->offsetSet('zmsdb.repositories.application-register', $repo);

        $response = $this->render([], [
            '__body' => '{
                "$schema":"https:\\/\\/schema.berlin.de\\/queuemanagement\\/applicationRegister.json",
                "type":"ticketprinter",
                "parameters":"ticketprinter[buttonlist]=s140,s141&sid=hbw5fe802ecabdb017449b966f9462e8e588gFMw",
                "userAgent":"Mozilla\\/5.0 (X11; Linux x86_64) AppleWebKit\\/537.36 Chrome\\/109.0.0.0 Safari\\/537.36",
                "scopeId":142,
                "startDate":"2023-09-25 10:06:14",
                "lastDate":"2023-09-25",
                "daysActive":1,
                "requestedChange":{"parameters":"ticketprinter[buttonlist]=s140,s141"},
                "id":"hbw5fe802ecabdb017449b966f9462e8e588gFMw"
            }'
        ], [], 'POST');

        self::assertStringContainsString('applicationRegister.json', (string)$response->getBody());
        self::assertStringContainsString('"requestedChange":null', (string)$response->getBody());
        self::assertTrue(200 == $response->getStatusCode());

        $response = $this->render([], [
            '__body' => '{
                "$schema":"https:\\/\\/schema.berlin.de\\/queuemanagement\\/applicationRegister.json",
                "type":"ticketprinter",
                "parameters":"ticketprinter[buttonlist]=s140,s141sid=hbw5fe802ecabdb017449b966f9462e8e588gFMw",
                "userAgent":"Mozilla\\/5.0 (X11; Linux x86_64) AppleWebKit\\/537.36 Chrome\\/109.0.0.0 Safari\\/537.36",
                "scopeId":142,
                "startDate":"2023-09-25 10:06:14",
                "lastDate":"2023-09-25",
                "daysActive":1,
                "id":"hbwd295dec9f5dc0c4f5fb93aa3e0ba28c4X3afC"
            }'
        ], [], 'POST');

        self::assertStringContainsString('applicationRegister.json', (string)$response->getBody());
        self::assertTrue(200 == $response->getStatusCode());
    }
}
