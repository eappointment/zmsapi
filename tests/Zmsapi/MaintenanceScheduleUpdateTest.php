<?php
/**
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmsapi\Tests;

use BO\Zmsdb\Config;
use BO\Zmsdb\MaintenanceSchedule as Repository;
use BO\Zmsdb\Log\TestLogger;

use BO\Zmsentities\MaintenanceSchedule;

use Fig\Http\Message\StatusCodeInterface;

use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

class MaintenanceScheduleUpdateTest extends Base
{
    use ProphecyTrait;

    public function testRendering()
    {
        $this->classname = 'MaintenanceScheduleAdd';

        $maintenanceRepo  = new Repository();
        $configRepository = new Config();
        $loggerInstance   = new TestLogger();
        \App::$slim->getContainer()->offsetSet('zmsdb.repositories.maintenance-schedule', $maintenanceRepo);
        \App::$slim->getContainer()->offsetSet('zmsdb.repositories.config', $configRepository);
        \App::$slim->getContainer()->offsetSet('logger.default', $loggerInstance);

        $this->setWorkstation()->getUseraccount()->setRights('superuser', 'useraccount');
        $response = $this->render([], [
            '__body' => '{
                "isActive": 0,
                "isRepetitive": 1,
                "timeString": "0 23 * * *",
                "duration": 90,
                "leadTime": 30,
                "area": "zms",
                "announcement": "Wait for it",
                "documentBody": "There it is"
            }'
        ], [], 'POST');

        $data = json_decode((string) $response->getBody());

        $this->classname = 'MaintenanceScheduleUpdate';

        $response = $this->render([], [
            '__body' => '{
                "id": ' . $data->data->id . ',
                "isActive": 0,
                "isRepetitive": 0,
                "timeString": "2016-04-01 12:34:56",
                "duration": 90,
                "leadTime": 30,
                "area": "zms",
                "announcement": "Wait for it",
                "documentBody": "There it is"
            }'
        ], [], 'POST');

        self::assertStringContainsString('maintenanceSchedule.json', (string)$response->getBody());
        self::assertStringContainsString((string) $data->data->id, (string)$response->getBody());
        self::assertTrue(200 == $response->getStatusCode());

        $response = $this->render([], [
            '__body' => '{
                "id": 0,
                "isActive": 0,
                "isRepetitive": 0,
                "timeString": "2016-04-01 12:34:56",
                "duration": 90,
                "leadTime": 30,
                "area": "zms",
                "announcement": "Wait for it",
                "documentBody": "There it is"
            }'
        ], [], 'POST');

        self::assertSame(StatusCodeInterface::STATUS_NOT_FOUND, $response->getStatusCode());
    }

    public function testDbOperationFails(): void
    {
        $this->testRendering();

        $repoProphecy = $this->prophesize(Repository::class);
        $repoProphecy
            ->updateEntity(Argument::type(MaintenanceSchedule::class))
            ->shouldBeCalled()
            ->willReturn(null);

        \App::$slim->getContainer()->offsetSet('zmsdb.repositories.maintenance-schedule', $repoProphecy->reveal());

        $response = $this->render([], [
            '__body' => '{
                "id": 12,
                "isActive": 0,
                "isRepetitive": 0,
                "timeString": "2016-04-01 12:34:56",
                "duration": 90,
                "leadTime": 30,
                "area": "zms",
                "announcement": "Wait for it",
                "documentBody": "There it is"
            }'
        ], [], 'POST');

        self::assertSame(StatusCodeInterface::STATUS_INTERNAL_SERVER_ERROR, $response->getStatusCode());
    }
}
