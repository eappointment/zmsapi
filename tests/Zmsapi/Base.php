<?php
/**
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsapi\Tests;

use BO\Zmsapi\Helper\User;
use BO\Zmsdb\Connection\Select;
use BO\Zmsentities\Department;
use BO\Zmsentities\Useraccount;
use BO\Zmsentities\Workstation;
use BO\Zmsentities\Scope;
use BO\Zmsentities\Apiclient as ApiclientEntity;
use BO\Zmsentities\Apikey as ApikeyEntity;
use BO\Zmsentities\Session as SessionEntity;

use BO\Zmsdb\Apiclient as ApiclientRepository;
use BO\Zmsdb\Apikey as ApikeyRepository;
use BO\Zmsdb\Session as SessionRepository;

use App;

abstract class Base extends \BO\Slim\PhpUnit\Base
{
    const API_TOKEN = 'wMdVa5Nu1seuCRSJxhKl2M3yw8zqaAilPH2Xc2IZs';
    const API_KEY = '136'; // testadmin
    const USER = 'testadmin';

    protected $namespace = '\\BO\\Zmsapi\\';

    public function setUp(): void
    {
        Select::setTransaction();
        Select::setProfiling();
        Select::setTimeZone('UTC', '+00:00');
    }

    public function tearDown(): void
    {
        \BO\Zmsapi\Helper\User::$workstation = null;
        Select::writeRollback();
        Select::closeWriteConnection();
        Select::closeReadConnection();
    }

    /**
     * @throws \Exception
     */
    public function readFixture($filename)
    {
        $path = dirname(__FILE__) . '/fixtures/' . $filename;
        if (!is_readable($path) || !is_file($path)) {
            throw new \Exception("Fixture $path is not readable");
        }
        return file_get_contents($path);
    }

    protected function setWorkstation(
        $workstationId = 137,
        $loginname = "testuser",
        $scopeId = 143,
        $password = "vorschau"
    ): ?Workstation {
        User::$workstation = new Workstation([
            'id' => $workstationId,
            'useraccount' => new Useraccount([
                'id' => $loginname,
                'password' => md5($password)
            ]),
            'scope' => new Scope([
                'id' => $scopeId,
                'preferences' => [
                    'queue' => [
                        'processingTimeAverage' => 10,
                    ]
                ]
            ]),
            'queue' => [
                "appointmentsOnly" => false,
                "clusterEnabled" => false
            ]
        ]);
        User::$workstationResolved = 2;
        return User::$workstation;
    }

    protected function setDepartment($departmentId): Department
    {
        $department = new Department([
            'id' => $departmentId,
            'name' => "TestDepartment $departmentId",
            ]);
        User::$workstation->getUseraccount()->addDepartment($department);
        return $department;
    }

    protected function setApikeyWithSession($processList = null, $now = null): ApikeyEntity
    {
        $now = $now ?? App::$now->getTimestamp();
        // write apiclient
        $apiclientEntity = new ApiclientEntity([
            'clientKey' => self::API_KEY,
            'shortname' => self::USER,
            'accesslevel' => 'public',
            'permission__remotecall' => 0,
            'secondsToInvalidation' => 60,
        ]);
        (new ApiclientRepository())->writeEntity($apiclientEntity);
        $apiclient = (new ApiclientRepository())->readEntity(self::API_KEY);

        // write apikey
        $apikeyEntity = new ApikeyEntity([
            'key' => $apiclient->clientKey,
            'createIP' => '',
            'ts' => $now,
            'apiclient' => $apiclient,
        ]);
        $apikeyEntity = (new ApikeyRepository())->writeEntity($apikeyEntity);

        //write session
        $session = new SessionEntity();
        $session->name = 'apikey';
        $session->id = self::API_KEY ;
        $session->testValid();
        if ($processList) {
            $session->content['processList'] = $processList;
        }
        (new SessionRepository())->updateEntity($session);
        return $apikeyEntity;
    }

    protected function dumpProfiler()
    {
        echo "\nProfiler:\n";
        $profiler = Select::getReadConnection()->getProfiler();
        $logger   = $profiler->getLogger();

        if (method_exists($logger, 'getMessages')) {
            foreach ($logger->getMessages() as $message) {
                echo $message . PHP_EOL;
            }
        }
    }
}
