<?php

namespace BO\Zmsapi\Tests;

use BO\Zmsentities\Process as ProcessEntity;

use BO\Zmsdb\Useraccount as UseraccountRepository;
use BO\Zmsdb\Process as ProcessRepository;
use BO\Zmsdb\Query\Useraccount as UseraccountQuery;
use BO\Zmsdb\Query\Base as BaseQuery;

class ProcessArchiveGetTest extends Base
{
    protected $classname = "ProcessArchiveGet";

    const API_TOKEN = 'wMdVa5Nu1seuCRSJxhKl2M3yw8zqaAilPH2Xc2IZs';
    const USER = 'testadmin';

    public function testRendering()
    {
        //login
        $this->setWorkstation();

        //add useraccount with apitoken
        $query = new UseraccountQuery(BaseQuery::UPDATE);
        $query->addConditionLoginName(self::USER);
        $query->addValues(['apitoken' => self::API_TOKEN]);
        (new UseraccountRepository())->writeItem($query);

        //update a process to get process_archive entry
        $entityData = json_decode($this->readFixture("GetProcess_10029.json"), 1);
        $entityData['amendment'] = "Test Update";
        $process = new ProcessEntity($entityData);
        $process = (new ProcessRepository())->updateEntity($process, \App::$now);

        $response = $this->render(['id' => $process->processArchiveId], [], []);
        $this->assertStringContainsString('process.json', (string)$response->getBody());
        $this->assertStringContainsString('"status":"confirmed"', (string)$response->getBody());
    }

    public function testProcessArchiveNotFound()
    {
        $this->expectException('BO\Zmsapi\Exception\Process\ProcessArchiveNotFound');
        $this->setWorkstation();
        $query = new UseraccountQuery(BaseQuery::UPDATE);
        $query->addConditionLoginName(self::USER);
        $query->addValues(['apitoken' => self::API_TOKEN]);
        (new UseraccountRepository())->writeItem($query);
        $this->render(['id' => 'wMdVa5Nu1seuCRSJxhKl2M3yw8zqaAil'], [], []);
    }

    public function testMissingUseraccount()
    {
        $this->expectException('BO\Zmsentities\Exception\UserAccountMissingLogin');
        $this->render(['id' => 'abc123456'], [], []);
    }

    public function testMissingToken()
    {
        $this->expectException('BO\Zmsapi\Exception\Process\ProcessArchiveNotFound');
        $this->render([], [], []);
    }
}
