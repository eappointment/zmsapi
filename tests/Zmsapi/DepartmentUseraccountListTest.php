<?php

namespace BO\Zmsapi\Tests;

use BO\Zmsentities\Permission;

use BO\Zmsdb\Workstation;
use BO\Zmsentities\Collection\UseraccountList;

class DepartmentUseraccountListTest extends Base
{
    protected $classname = "DepartmentUseraccountList";

    public function testRendering()
    {
        $this->setWorkstation()->getUseraccount()->setRights(Permission::RIGHT_USERACCOUNT);
        $this->setDepartment(74);
        $response = $this->render(['id' => 74], [], []);
        $this->assertStringContainsString('testuser', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
    }

    public function testDepartmentNotAssigned()
    {
        $this->setWorkstation()->getUseraccount()->setRights(Permission::RIGHT_USERACCOUNT);
        $this->setDepartment(74);
        $this->expectException('BO\Zmsentities\Exception\UserAccountMissingDepartment');
        $this->expectExceptionCode(403);
        $this->render(['id' => 72], [], []);
    }

    public function testDepartmentNotFound()
    {
        $this->setWorkstation()->getUseraccount()->setRights(Permission::RIGHT_PRIVILEGED, Permission::RIGHT_USERACCOUNT);
        $this->expectException('BO\Zmsentities\Exception\UserAccountMissingDepartment');
        $this->expectExceptionCode(403);
        $this->render(['id' => 99999], [], []);
    }

    public function testDepartmentMissing()
    {
        $this->setWorkstation()->getUseraccount()->setRights('superuser', 'useraccount');
        $this->expectException('BO\Zmsapi\Exception\PreConditionFailed');
        $this->expectExceptionCode(412);
        $this->render([], [], []);
    }
}
