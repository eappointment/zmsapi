<?php

namespace BO\Zmsapi\Tests;

use App;

use BO\Zmsdb\Apikey as ApikeyRepository;
use BO\Zmsdb\Apiclient as ApiclientRepository;

use BO\Zmsentities\Apiclient as ApiclientEntity;
use BO\Zmsentities\Apikey as ApikeyEntity;

class ApikeyCalendarSetTest extends Base
{
    protected $classname = "ApikeyCalendarSet";

    public function testRendering()
    {
        $this->setApikeyWithSession();

        $response = $this->render(['key' => self::API_KEY], [
            'token' => self::API_TOKEN,
            '__body' => '{
            "firstDay": {
                "year": '. \App::$now->format("Y") .',
                "month": '. \App::$now->format("n") .',
                "day": '. \App::$now->format("j") .'
            },
            "lastDay": {
                "year": '. \App::$now->modify("+1 month")->format("Y") .',
                "month": '. \App::$now->modify("+1 month")->format("n") .',
                "day": '. \App::$now->modify("+1 month")->format("t") .'
            },
            "requests": [
                {
                  "id": "120703",
                  "name": "Personalausweis beantragen",
                  "source": "dldb"
                }
            ],
            "providers": [
                {
                  "id": 122217,
                  "source": "dldb"
                }
            ],
            "scopes": [
                {
                  "id": 141
                }
            ]}'
        ], [], 'POST');
        $this->assertStringContainsString('calendar.json', (string)$response->getBody());
        $this->assertStringContainsString('"scopes":[{"id":141}]', (string)$response->getBody());
        $this->assertStringContainsString('"Personalausweis beantragen"', (string)$response->getBody());
        $this->assertStringContainsString(
            '"firstDay":{"year":2016,"month":4,"day":1',
            (string)$response->getBody()
        );
        $this->assertTrue(200 == $response->getStatusCode());
    }

    public function testMissingApiclient()
    {
        $this->expectException('BO\Zmsapi\Exception\Apikey\AccessDenied');
        // write apikey
        $apikeyEntity = new ApikeyEntity([
            'key' => self::API_KEY,
            'createIP' => '',
            'ts' => App::$now->getTimestamp()
        ]);
        (new ApikeyRepository())->writeEntity($apikeyEntity);
        $this->render(['key' => self::API_KEY], ['token' => self::API_TOKEN], [], 'POST');
    }

    public function testApiclientBlocked()
    {
        $this->expectException('BO\Zmsapi\Exception\Apikey\AccessDenied');

        // write apiclient
        $apiclientEntity = new ApiclientEntity([
            'clientKey' => self::API_KEY,
            'shortname' => self::USER,
            'accesslevel' => 'blocked',
            'permission__remotecall' => 0,
            'secondsToInvalidation' => 60,
        ]);
        (new ApiclientRepository())->writeEntity($apiclientEntity);
        $apiclient = (new ApiclientRepository())->readEntity(self::API_KEY);

        // write apikey
        $apikeyEntity = new ApikeyEntity([
            'key' => $apiclient->clientKey,
            'createIP' => '',
            'ts' => App::$now->getTimestamp(),
            'apiclient' => $apiclient,
        ]);
        (new ApikeyRepository())->writeEntity($apikeyEntity);
        $this->render(['key' => self::API_KEY], ['token' => self::API_TOKEN], [], 'POST');
    }

    public function testMissingSession()
    {
        $this->expectException('BO\Zmsapi\Exception\Apikey\AccessDenied');

        // write apiclient
        $apiclientEntity = new ApiclientEntity([
            'clientKey' => self::API_KEY,
            'shortname' => self::USER,
            'accesslevel' => 'public',
            'permission__remotecall' => 0,
            'secondsToInvalidation' => 60,
        ]);
        (new ApiclientRepository())->writeEntity($apiclientEntity);
        $this->apiclient = (new ApiclientRepository())->readEntity(self::API_KEY);

        // write apikey
        $apikeyEntity = new ApikeyEntity([
            'key' => $this->apiclient->clientKey,
            'createIP' => '',
            'ts' => App::$now->getTimestamp(),
            'apiclient' => $this->apiclient,
        ]);
        (new ApikeyRepository())->writeEntity($apikeyEntity);
        $this->render(['key' => self::API_KEY], ['token' => self::API_TOKEN], [], 'POST');
    }

    public function testMissingApikey()
    {
        $this->expectException('BO\Zmsapi\Exception\Apikey\ApiKeyNotValid');
        $this->render([], ['token' => self::API_TOKEN], []);
    }

    public function testInvalidKey()
    {
        $this->expectException('BO\Zmsapi\Exception\Apikey\ApiKeyNotValid');
        $this->render(['key' => 999], ['token' => self::API_TOKEN], [], 'POST');
    }

    public function testInvalidToken()
    {
        // write apiclient
        $apiclientEntity = new ApiclientEntity([
            'clientKey' => self::API_KEY,
            'shortname' => self::USER,
            'accesslevel' => 'public',
            'permission__remotecall' => 0,
            'secondsToInvalidation' => 60,
        ]);
        (new ApiclientRepository())->writeEntity($apiclientEntity);
        $this->apiclient = (new ApiclientRepository())->readEntity(self::API_KEY);

        // write apikey
        $apikeyEntity = new ApikeyEntity([
            'key' => $this->apiclient->clientKey,
            'createIP' => '',
            'ts' => App::$now->getTimestamp(),
            'apiclient' => $this->apiclient,
        ]);
        (new ApikeyRepository())->writeEntity($apikeyEntity);
        $this->expectException('BO\Zmsapi\Exception\Apikey\AccessDenied');
        $this->render(['key' => self::API_KEY], [], [], 'POST');
    }

    public function testAccessDeniedMissingToken()
    {
        $this->expectException('BO\Zmsapi\Exception\Apikey\AccessDenied');
        $this->render([], [], [], 'POST');
    }
}
