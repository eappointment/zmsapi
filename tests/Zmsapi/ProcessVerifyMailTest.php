<?php

namespace BO\Zmsapi\Tests;

class ProcessVerifyMailTest extends Base
{
    protected $classname = "ProcessVerifyMail";

    const PROCESS_ID = 10029;

    const AUTHKEY = '1c56';

    public function testRendering()
    {
        $response = $this->render([], [
            '__body' => $this->readFixture('GetMail.json')
        ], [], 'POST');
        $this->assertStringContainsString('mail.json', (string)$response->getBody());
        $this->assertStringContainsString('test@example.com', (string)$response->getBody());
        $this->assertStringContainsString('testclient@example.com', (string)$response->getBody());
        $this->assertStringContainsString('"id":"77"', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
        return $response;
    }

    public function testEmpty()
    {
        $this->expectException('\BO\Mellon\Failure\Exception');
        $this->render([], [], [], 'POST');
    }

    public function testInvalidClient()
    {
        $this->setWorkstation();
        $this->expectException('\BO\Zmsdb\Exception\Mail\ClientWithoutEmail');
        $this->expectExceptionCode(404);
        $this->render([], [
            '__body' => '{
                "createIP": "145.15.3.10",
                "createTimestamp": 1447931596000,
                "multipart": [
                    {
                      "queueId": "1234",
                      "mime": "text/html",
                      "content": "<h1>Title</h1><p>Message</p>",
                      "base64": false
                    },
                    {
                      "queueId": "1234",
                      "mime": "text/plain",
                      "content": "Title\nMessage",
                      "base64": false
                    }
                ],
                "process": {
                    "clients": [
                        {
                            "familyName": "Max Mustermann",
                            "email": "",
                            "telephone": "030 115"
                        }
                    ],
                    "scope": {
                        "id": 151
                    }
                },
                "subject": "Example Mail"
            }'
        ], [], 'POST');
    }

    public function testInvalidSchema()
    {
        $this->expectException('\BO\Zmsentities\Exception\SchemaValidation');
        $this->expectExceptionCode(400);
        $this->render([], [
            '__body' => $this->readFixture('GetMail_invalidSchema.json')
        ], [], 'POST');
    }
}
