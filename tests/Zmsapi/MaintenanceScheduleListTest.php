<?php

declare(strict_types=1);

namespace BO\Zmsapi\Tests;

use BO\Slim\Container;
use BO\Slim\Response;
use BO\Zmsapi\MaintenanceScheduleList;
use BO\Zmsdb\MaintenanceSchedule as Repository;
use BO\Zmsentities\Collection\MaintenanceScheduleList as Collection;
use BO\Zmsentities\MaintenanceSchedule;
use Fig\Http\Message\StatusCodeInterface;
use Prophecy\PhpUnit\ProphecyTrait;

class MaintenanceScheduleListTest extends Base
{
    use ProphecyTrait;

    public function testRendering()
    {
        $this->setWorkstation()->getUseraccount()->setRights('superuser', 'useraccount');

        $request   = $this->getRequest('GET', '/maintenanceschedule/');
        $entity  = new MaintenanceSchedule([
            'id' => 12,
            'startDateTime' => '2001-01-01 00:00',
            'timeString' => '0 0 * * *',
            'isActive' => true,
            'isRepetitive' => true
        ]);

        $repoProphecy = $this->prophesize(Repository::class);
        $repoProphecy
            ->readList()
            ->shouldBeCalled()
            ->willReturn((new Collection())->addEntity($entity));

        $container = new Container();
        $container->offsetSet('zmsdb.repositories.maintenance-schedule', $repoProphecy->reveal());
        $controller = new MaintenanceScheduleList($container);

        $response = $controller->readResponse($request, new Response(), []);

        self::assertSame(StatusCodeInterface::STATUS_OK, $response->getStatusCode());
        self::assertStringContainsString('maintenanceSchedule.json', (string)$response->getBody());
    }
}
