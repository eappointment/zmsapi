<?php

namespace BO\Zmsapi\Tests;

class WorkstationOAuthTest extends Base
{
    protected $classname = "WorkstationOAuth";

    protected static $hash = "a9b215f1e460490c8a0b6d42c274d5e4";

    public function testRendering()
    {
        $response = $this->render(
            [],
            [
                '__header' => [
                    'X-Authkey' => self::$hash
                ],
                '__body' => '{
                    "username":"keycloakuser",
                    "email":"keycloakuser@service.berlin.de"
                }',
                'state' => self::$hash
            ],
            [],
            'POST'
        );
        $this->assertStringContainsString('workstation.json', (string)$response->getBody());
        $this->assertStringContainsString('keycloakuser', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
    }

    public function testAuthFailed()
    {
        $this->expectException('\BO\Zmsapi\Exception\Workstation\WorkstationAuthFailed');
        $this->expectExceptionCode(403);
        $this->render(
            [],
            [
                '__header' => [
                    'X-Authkey' => 'unittestauthfailed'
                ],
                '__body' => '{
                    "username":"keycloakuser",
                    "email":"keycloakuser@service.berlin.de"
                }',
                'state' => self::$hash
            ],
            [],
            'POST'
        );
    }

    public function testOidcUserAlreadyExists()
    {
        $response =  $this->render(
            [],
            [
                '__header' => [
                    'X-Authkey' => self::$hash
                ],
                '__body' => '{
                    "username":"testuser",
                    "email":"keycloakuser@service.berlin.de"
                }',
                'state' => self::$hash
            ],
            [],
            'POST'
        );
        $this->assertStringContainsString('workstation.json', (string)$response->getBody());
        $this->assertStringContainsString('testuser', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
    }

    public function testSchemaValidationFailed()
    {
        $this->expectException('\BO\Zmsentities\Exception\SchemaValidation');
        $this->expectExceptionMessage('Es muss eine E-Mail-Adresse angegeben werden');
        $this->render(
            [],
            [
                '__header' => [
                    'X-Authkey' => self::$hash
                ],
                '__body' => '{
                    "username":"testuser",
                    "email":""
                }',
                'state' => self::$hash
            ],
            [],
            'POST'
        );
    }
}
