<?php

namespace BO\Zmsapi\Tests;

use BO\Zmsentities\Collection\ProcessList;
use BO\Zmsentities\Process as ProcessEntity;
use Exception;

/**
 *
 * @SuppressWarnings(public)
 */
class ProcessReserveTest extends Base
{
    protected $classname = "ProcessReserve";

    /**
     * @throws Exception
     */
    public function testRendering()
    {
        $processList = new ProcessList(
            json_decode($this->readFixture("GetFreeProcessList.json"))
        );
        $process = $processList->getFirst();
        $response = $this->render([], [
            '__body' => json_encode($process)
        ], [], 'POST');

        $this->assertStringContainsString('"status":"reserved"', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
    }

    /**
     * @throws Exception
     */
    public function testWithSlotsRequired()
    {
        $this->setWorkstation();
        $processList = new ProcessList(
            json_decode($this->readFixture("GetFreeProcessList.json"))
        );
        $process = $processList->getFirst();
        $response = $this->render([], [
            '__body' => json_encode($process),
            'slotsRequired' => 1,
            'slotType' => 'intern'
        ], [], 'POST');

        $this->assertStringContainsString('reserved', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
    }

    /**
     * @throws Exception
     */
    public function testWithSlotsRequiredExceeded()
    {
        $this->expectException('BO\Zmsdb\Exception\Process\ProcessReserveFailed');
        $this->setWorkstation();
        $processList = new ProcessList(
            json_decode($this->readFixture("GetFreeProcessList.json"))
        );
        $process = $processList->getFirst();
        $this->render([], [
            '__body' => json_encode($process),
            'slotsRequired' => 2,
            'slotType' => 'intern'
        ], [], 'POST');
    }

    /**
     * @throws Exception
     */
    public function testWithProcessReserveExists()
    {
        $this->expectException('BO\Zmsapi\Exception\Process\ProcessAlreadyExists');
        $this->setWorkstation();
        $process = new ProcessEntity(
            json_decode($this->readFixture("GetProcess_10029.json"), 1)
        );
        $this->render([], [
            '__body' => json_encode($process)
        ], [], 'POST');
    }

    /**
     * @throws Exception
     */
    public function testScopeHasRequests()
    {
        $this->expectException('BO\Zmsapi\Exception\Matching\RequestNotFound');
        $this->setWorkstation();
        $processList = new ProcessList(
            json_decode($this->readFixture("GetProcessListWithMissingRequests.json"))
        );
        $process = $processList->getLast();
        $this->render([], [
            '__body' => json_encode($process),
            'slotsRequired' => 2,
            'slotType' => 'intern'
        ], [], 'POST');
    }

    /**
     * @throws Exception
     */
    public function testMultipleSlots()
    {
        $process = new ProcessEntity(
            json_decode($this->readFixture("GetProcessWithMultipleSlotCount.json"), 1)
        );
        $response = $this->render([], [
            '__body' => json_encode($process)
        ], [], 'POST');

        $responseData = json_decode($response->getBody(), 1);
        $this->assertEquals('6', $responseData['data']['appointments'][0]['slotCount']);
        $this->assertTrue(200 == $response->getStatusCode());
    }

    /**
     * @throws Exception
     */
    public function testMultipleSlotsWithDublicatedRequests()
    {
        $process = new ProcessEntity(
            json_decode($this->readFixture("GetProcessWithDublicatedRequests.json"), 1)
        );
        $response = $this->render([], [
            '__body' => json_encode($process)
        ], [], 'POST');

        $responseData = json_decode($response->getBody(), 1);
        $this->assertEquals('8', $responseData['data']['appointments'][0]['slotCount']);
        $this->assertTrue(200 == $response->getStatusCode());
    }

    public function testInvalidInput()
    {
        $this->expectException('BO\Mellon\Failure\Exception');
        $this->render([], [
            '__body' => '',
        ], [], 'POST');
    }
}
