<?php

namespace BO\Zmsapi\Tests;

use BO\Zmsdb\Exception\Process\ProcessReserveFailed;
use BO\Zmsdb\Apikey as ApikeyRepository;
use BO\Zmsdb\Session as SessionRepository;

use BO\Zmsentities\Collection\ProcessList;
use BO\Zmsentities\Session as SessionEntity;
use BO\Zmsentities\Process as ProcessEntity;

class ApikeyConfirmTest extends Base
{
    protected $classname = "ApikeyConfirm";

    /**
     * @throws ProcessReserveFailed
     */
    public function testRendering()
    {
        $processListReserved = (new ProcessList())
            ->addData(json_decode($this->readFixture("GetReservedProcessList.json"), 1));

        $apikey = $this->setApikeyWithSession($processListReserved);

        //update process
        $entity = new ProcessEntity([
            'id' => $processListReserved->getFirst()->getId(),
            'authKey' => 'should-not-be-changeable',
            'clients' => [
                [
                    'familyName' => 'Max Mustermann',
                    'email' => 'max.mustermann@example.com',
                    'telephone' => '030 116117'
                ]
            ],
            'amendment' => 'This is an example',
        ]);

        self::assertEquals('webreservierung', $processListReserved->getFirst()->getFirstClient()->familyName);
        self::assertEquals('test@example.com', $processListReserved->getFirst()->getFirstClient()->email);
        self::assertEquals('', $processListReserved->getFirst()->getFirstClient()->telephone);
        self::assertEquals('', $processListReserved->getFirst()->getAmendment());


        $response = $this->render(['key' => $apikey->getId()], [
            'token' => self::API_TOKEN,
            '__body' => json_encode($entity),
        ], [], 'POST');
        self::assertStringContainsString('max.mustermann@example.com', (string)$response->getBody());
        self::assertStringContainsString('Max Mustermann', (string)$response->getBody());
        self::assertStringContainsString('030 116117', (string)$response->getBody());
        self::assertStringContainsString('This is an example', (string)$response->getBody());
        self::assertStringNotContainsString('should-not-be-changeable', (string)$response->getBody());

        self::assertTrue(200 == $response->getStatusCode());
    }

    /**
     * @throws ProcessReserveFailed
     */
    public function testWithExpiredApikey()
    {
        $processListReserved = (new ProcessList())
            ->addData(json_decode($this->readFixture("GetReservedProcessList.json"), 1));

        $now = \App::$now->modify('-90 seconds')->getTimestamp();
        $apikey = $this->setApikeyWithSession($processListReserved, $now);
        //update process
        $entity = new ProcessEntity([
            'id' => $processListReserved->getFirst()->getId(),
            'authKey' => 'should-not-be-changeable',
            'clients' => [
                [
                    'familyName' => 'Max Mustermann',
                    'email' => 'max.mustermann@example.com',
                    'telephone' => '030 116117'
                ]
            ],
            'amendment' => 'This is an example',
        ]);

        $response = $this->render(['key' => $apikey->getId()], [
            'token' => self::API_TOKEN,
            '__body' => json_encode($entity),
        ], [], 'POST');
        self::assertTrue(200 == $response->getStatusCode());

        $expiredApikey = (new ApikeyRepository())->readEntity($apikey->getId());
        self::assertFalse($expiredApikey->getId());

        $expiredSession = (new SessionRepository())->readEntity('apikey', $apikey->getId());
        self::assertEquals($expiredSession, null);
    }

    public function testInvalidInput()
    {
        $this->expectException('BO\Mellon\Failure\Exception');
        $this->render([], [
            '__body' => '',
        ], [], 'POST');
    }

    public function testApikeyInvalid()
    {
        $this->expectException('BO\Zmsapi\Exception\Apikey\ApiKeyNotValid');
        $this->expectExceptionCode(400);
        $input = json_encode((new ProcessEntity)->createExample());
        $this->render(['key' => '_invalid'], [
            'token' => self::API_TOKEN,
            '__body' => $input,
        ], [], 'POST');
    }

    public function testMissingToken()
    {
        $this->expectException('BO\Zmsapi\Exception\Apikey\AccessDenied');
        $this->expectExceptionCode(400);
        $input = json_encode((new ProcessEntity)->createExample());
        $this->render(['key' => '_invalid'], [
            '__body' => $input,
        ], [], 'POST');
    }
}
