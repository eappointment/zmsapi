<?php

namespace BO\Zmsapi\Tests;

use App;
use BO\Zmsdb\Apikey as ApikeyRepository;
use BO\Zmsdb\Apiclient as ApiclientRepository;
use BO\Zmsdb\Exception\Pdo\DeadLockFound;
use BO\Zmsdb\Exception\Pdo\LockTimeout;
use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsdb\Session as SessionRepository;

use BO\Zmsentities\Apiclient as ApiclientEntity;
use BO\Zmsentities\Apikey as ApikeyEntity;
use BO\Zmsentities\Session as SessionEntity;

class ApikeyUnlockGetTest extends Base
{
    protected $classname = "ApikeyUnlockGet";

    const API_TOKEN = 'wMdVa5Nu1seuCRSJxhKl2M3yw8zqaAilPH2Xc2IZs';
    const API_KEY = '136'; // testadmin
    const USER = 'testadmin';

    protected $apiclient = null;

    /**
     * @throws PDOFailed
     * @throws DeadLockFound
     * @throws LockTimeout
     */
    public function testRendering()
    {
        // write apiclient
        $apiclientEntity = new ApiclientEntity([
            'clientKey' => self::API_KEY,
            'shortname' => self::USER,
            'accesslevel' => 'public',
            'permission__remotecall' => 0,
            'secondsToInvalidation' => 60,
        ]);
        (new ApiclientRepository())->writeEntity($apiclientEntity);
        $this->apiclient = (new ApiclientRepository())->readEntity(self::API_KEY);

        // write apikey
        $apikeyEntity = new ApikeyEntity([
            'key' => $this->apiclient->clientKey,
            'createIP' => '',
            'ts' => App::$now->getTimestamp(),
            'apiclient' => $this->apiclient,
        ]);
        (new ApikeyRepository())->writeEntity($apikeyEntity);
        (new ApikeyRepository())->readEntity(self::API_KEY);

        //write session
        $session = new SessionEntity();
        $session->name = 'apikey';
        $session->id = self::API_KEY ;
        $session->testValid();
        (new SessionRepository())->updateEntity($session);

        $response = $this->render(['key' => self::API_KEY], ['token' => self::API_TOKEN], []);
        $this->assertStringContainsString('apikey.json', (string)$response->getBody());
        $this->assertStringContainsString('"clientKey":"136"', (string)$response->getBody());
        $this->assertStringContainsString('"accesslevel":"public"', (string)$response->getBody());
        $this->assertStringContainsString('"locked":"0"', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
    }

    public function testMissingSession()
    {
        $this->expectException('BO\Zmsapi\Exception\Apikey\AccessDenied');

        // write apiclient
        $apiclientEntity = new ApiclientEntity([
            'clientKey' => self::API_KEY,
            'shortname' => self::USER,
            'accesslevel' => 'public',
            'permission__remotecall' => 0,
            'secondsToInvalidation' => 60,
        ]);
        (new ApiclientRepository())->writeEntity($apiclientEntity);
        $this->apiclient = (new ApiclientRepository())->readEntity(self::API_KEY);

        // write apikey
        $apikeyEntity = new ApikeyEntity([
            'key' => $this->apiclient->clientKey,
            'createIP' => '',
            'ts' => App::$now->getTimestamp(),
            'apiclient' => $this->apiclient,
        ]);
        (new ApikeyRepository())->writeEntity($apikeyEntity);
        (new ApikeyRepository())->readEntity(self::API_KEY);
        $this->render(['key' => self::API_KEY], ['token' => self::API_TOKEN], []);
    }

    public function testMissingApikey()
    {
        $this->expectException('BO\Zmsapi\Exception\Apikey\ApiKeyNotValid');
        $this->render([], ['token' => self::API_TOKEN], []);
    }

    public function testInvalidKey()
    {
        $this->expectException('BO\Zmsapi\Exception\Apikey\ApiKeyNotValid');
        $this->render(['key' => 999], ['token' => self::API_TOKEN], []);
    }

    public function testAccessDenied()
    {
        $this->expectException('BO\Zmsapi\Exception\Apikey\AccessDenied');
        $this->render(['key' => 999], [], []);
    }
}
