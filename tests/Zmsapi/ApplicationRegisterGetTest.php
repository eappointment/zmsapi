<?php

declare(strict_types=1);

namespace BO\Zmsapi\Tests;

use BO\Slim\Container;
use BO\Slim\Response;
use BO\Zmsapi\ApplicationRegisterGet;
use BO\Zmsapi\Exception\ApplicationRegister\EntryNotFound;
use BO\Zmsdb\ApplicationRegister as Repository;
use BO\Zmsentities\ApplicationRegister;
use Prophecy\PhpUnit\ProphecyTrait;

class ApplicationRegisterGetTest extends Base
{
    use ProphecyTrait;

    protected $classname = "ApplicationRegisterGet";

    public function testRendering()
    {
        $this->setWorkstation()->getUseraccount()->setRights('scope', 'useraccount');
        $this->setValidatorInstance([]);
        $request   = $this->getRequest('GET', '/applicationregister/12345678/');
        $entity    = new ApplicationRegister(['id' => '12345678']);

        $repoProphecy = $this->prophesize(Repository::class);
        $repoProphecy
            ->readEntity('12345678')
            ->shouldBeCalled()
            ->willReturn($entity);

        $container = new Container();
        $container->offsetSet('zmsdb.repositories.application-register', $repoProphecy->reveal());
        $controller = new ApplicationRegisterGet($container);

        $response = $controller->readResponse($request, new Response(), ['id' => '12345678']);
        self::assertStringContainsString('applicationRegister.json', (string)$response->getBody());
        self::assertTrue(200 == $response->getStatusCode());
    }

    public function testReadResponseFails(): void
    {
        $this->expectException(EntryNotFound::class);
        $this->setWorkstation()->getUseraccount()->setRights('scope', 'useraccount');
        $this->setValidatorInstance([]);
        $request   = $this->getRequest('GET', '/applicationregister/12345678/');

        $repoProphecy = $this->prophesize(Repository::class);
        $repoProphecy
            ->readEntity('12345678')
            ->shouldBeCalled()
            ->willReturn(null);

        $container = new Container();
        $container->offsetSet('zmsdb.repositories.application-register', $repoProphecy->reveal());
        $controller = new ApplicationRegisterGet($container);

        $controller->readResponse($request, new Response(), ['id' => '12345678']);
    }

    public function testExample(): void
    {
        $this->setWorkstation()->getUseraccount()->setRights('scope', 'useraccount');
        $this->setValidatorInstance([]);
        $request   = $this->getRequest('GET', '/applicationregister/00000000-0000-0000-0000-000000000000/');
        $entity    = ApplicationRegister::getExample();

        $repoProphecy = $this->prophesize(Repository::class);
        $repoProphecy
            ->readEntity("00000000-0000-0000-0000-000000000000")
            ->shouldBeCalled()
            ->willReturn($entity);

        $container = new Container();
        $container->offsetSet('zmsdb.repositories.application-register', $repoProphecy->reveal());
        $controller = new ApplicationRegisterGet($container);

        $response = $controller->readResponse(
            $request,
            new Response(),
            ['id' => "00000000-0000-0000-0000-000000000000"]
        );

        $res = (string) $response->getBody();

        $exp =  '"data":{';
        $exp .=     '"$schema":"https://schema.berlin.de/queuemanagement/applicationRegister.json",';
        $exp .=     '"type":"calldisplay",';
        $exp .=     '"parameters":"collections[scopelist]=141,142&qrcode=1",';
        $exp .=     '"userAgent":"Mozilla/5.0 (Windows NT 6.1; rv:60.0) Gecko/20100101 Firefox/60.0",';
        $exp .=     '"scopeId":141,';
        $exp .=     '"startDate":"2016-04-01 00:00:00",';
        $exp .=     '"lastDate":"2016-04-01",';
        $exp .=     '"daysActive":1,';
        $exp .=     '"requestedChange":{"parameters":"collections[scopelist]=140,141,142&qrcode=1&template=clocknrplatz"},';
        $exp .=     '"id":"00000000-0000-0000-0000-000000000000"';
        $exp .= '}';

        self::assertStringContainsString($exp, $res);
    }
}
