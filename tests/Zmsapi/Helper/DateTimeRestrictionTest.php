<?php
/**
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmsapi\Tests\Helper;

use BO\Zmsapi\Tests\Base;
use BO\Zmsapi\Helper\DateTimeRestriction;
use BO\Zmsentities\Calendar as CalendarEntity;
use BO\Zmsentities\Collection\ProcessList;
use BO\Zmsentities\Day;

class DateTimeRestrictionTest extends Base
{
    const TIME_RESTRICTION_STRING = 'Mo-13-19,Mi-13-19,Fr-13-19';

    public function testRendering()
    {
        $timeRestrictionHelper = new DateTimeRestriction(self::TIME_RESTRICTION_STRING);
        self::assertInstanceOf(DateTimeRestriction::class, $timeRestrictionHelper);
    }

    public function testRestrictedDayList()
    {
        $timeRestrictionHelper = new DateTimeRestriction(self::TIME_RESTRICTION_STRING);
        $calendar = (new CalendarEntity())->addData(json_decode($this->readFixture('GET_calendar.json')));
        $restrictedDayList = $timeRestrictionHelper->getRestrictedDaylist($calendar);

        self::assertEquals('full', $restrictedDayList->getFirst()->status);
        self::assertEquals('restricted', $restrictedDayList->getLast()->status);

        $firstBookableDay = $restrictedDayList->getFirstBookableDay();
        self::assertEquals('2016-05-30', $firstBookableDay->toDateTime()->format('Y-m-d'));
        self::assertEquals(72, $firstBookableDay->freeAppointments['public']);
    }

    public function testRestrictedProcessList()
    {
        $timeRestrictionHelper = new DateTimeRestriction(self::TIME_RESTRICTION_STRING);
        $processList = (new ProcessList())
            ->addData(json_decode($this->readFixture('GET_processList_141_20160401.json')));
        $restrictedList = $timeRestrictionHelper->getRestrictedProcessList($processList);

        self::assertEquals(102, $processList->count());
        self::assertEquals(13, $restrictedList->count());

        $firstNotBookable = (new \DateTimeImmutable())
            ->setTimestamp($processList->getFirst()->getFirstAppointment()->date)
            ->format('Y-m-d H:i:s');
        self::assertEquals('2016-04-01 08:00:00', $firstNotBookable); //08:00 not in range of 13-19

        $firstBookable = (new \DateTimeImmutable())
            ->setTimestamp($restrictedList->getFirst()->getFirstAppointment()->date)
            ->format('Y-m-d H:i:s');
        $lastBookable = (new \DateTimeImmutable())
            ->setTimestamp($restrictedList->getLast()->getFirstAppointment()->date)
            ->format('Y-m-d H:i:s');
        self::assertEquals('2016-04-01 13:00:00', $firstBookable);
        self::assertEquals('2016-04-01 13:40:00', $lastBookable);
    }
}
