<?php
/**
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmsapi\Tests\Helper;

use BO\Slim\Container;
use BO\Zmsapi\ConfigGet;
use BO\Zmsapi\Helper\Maintenance;
use BO\Zmsapi\WorkstationProcess;
use BO\Zmsentities\Exception\SchemaValidation;
use BO\Zmsentities\MaintenanceSchedule;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;

class MaintenanceTest extends TestCase
{
    use ProphecyTrait;

    public function testIsBlockedController(): void
    {
        $tempDateTime = \App::$now;

        $activeEntity = $this->prophesize(MaintenanceSchedule::class);
        $activeEntity->getStartDateTime()->willReturn(new \DateTime('2016-04-01 01:00:00'));

        $repository = $this->prophesize(\BO\Zmsdb\MaintenanceSchedule::class);
        $repository->readActiveEntity()->willReturn($activeEntity->reveal());

        $container = new Container();
        $container->offsetSet('zmsdb.repositories.maintenance-schedule', $repository->reveal());

        $maintenance = new Maintenance($container);

        \App::$now = new \DateTime('2016-04-01 01:29:00');
        self::assertFalse($maintenance->isBlockedController(new WorkstationProcess($container)));

        \App::$now = new \DateTime('2016-04-01 01:30:00');
        self::assertTrue($maintenance->isBlockedController(new WorkstationProcess($container)));
        self::assertFalse($maintenance->isBlockedController(new ConfigGet($container)));

        \App::$now = $tempDateTime;
    }

    public function testValidSchedule(): void
    {
        $entityProphecy = $this->prophesize(MaintenanceSchedule::class);
        $entityProphecy
            ->getTimeString()
            ->shouldBeCalled()
            ->willReturn('2001-01-01 12:34:56');
        $entityProphecy
            ->isRepetitive()
            ->shouldBeCalled()
            ->willReturn(false);

        Maintenance::testValidSchedule($entityProphecy->reveal());

        $entityProphecy
            ->getTimeString()
            ->shouldBeCalled()
            ->willReturn('2001-01-01 12:34');

        Maintenance::testValidSchedule($entityProphecy->reveal());

        $entityProphecy
            ->getTimeString()
            ->shouldBeCalled()
            ->willReturn('34 12 1 * *');
        $entityProphecy
            ->isRepetitive()
            ->shouldBeCalled()
            ->willReturn(true);

        Maintenance::testValidSchedule($entityProphecy->reveal());
    }

    public function testValidScheduleRepetitiveFails(): void
    {
        $this->expectException(SchemaValidation::class);
        $entity = new MaintenanceSchedule([
            'isRepetitive' => true,
            'timeString' => '12:34'
        ]);
        Maintenance::testValidSchedule($entity);
    }

    public function testValidScheduleDateFails(): void
    {
        $this->expectException(SchemaValidation::class);
        $entity = new MaintenanceSchedule([
            'timeString' => '2022 Aug. 1. 12:34'
        ]);
        Maintenance::testValidSchedule($entity);
    }
}
