<?php

declare(strict_types=1);

namespace BO\Zmsapi\Tests;

use BO\Slim\Tests\TestLogger;
use BO\Zmsdb\CheckInConfig as Repository;
use BO\Zmsdb\Config;

class CheckInConfigListByDepartmentTest extends Base
{
    public function testRendering()
    {
        $this->setWorkstation()->getUseraccount()->setRights('superuser', 'useraccount');

        $entityRepository = new Repository();
        $configRepository = new Config();
        $loggerInstance   = new TestLogger();
        \App::$slim->getContainer()->offsetSet('zmsdb.repositories.check-in-config', $entityRepository);
        \App::$slim->getContainer()->offsetSet('zmsdb.repositories.config', $configRepository);
        \App::$slim->getContainer()->offsetSet('logger.default', $loggerInstance);

        $this->classname = 'CheckInConfigAdd';

        $response = $this->render([], [
            '__body' => '{
                "$schema":"https:\/\/schema.berlin.de\/queuemanagement\/checkInConfig.json",
                "id":"3483f171ec9c",
                "scopeIds": ":140:141:",
                "tooEarlyMinutes" : 30,
                "toleranceMinutes" : 5,
                "languages" : "de_DE,en_GB",
                "msgAlternative": "{\"de_DE\":\"Geh zur Rezeption!\", \"en_GB\":\"Go to the reception.\"}",
                "msgLate": "{\"de_DE\":\"Zu spät.\",\"en_GB\":\"You are late.\"}",
                "msgEarly": "{\"de_DE\":\"Viel zu früh\",\"en_GB\":\"You are early.\"}",
                "msgWrongDay": "{\"de_DE\":\"Nicht Heute\",\"en_GB\":\"You came the wrong day.\"}",
                "msgWrongCounter": "{\"de_DE\":\"Sie sind hier falsch.\",\"en_GB\":\"Wrong place.\"}",
                "msgSuccess": "{\"de_DE\":\"Nehmen Sie Platz\",\"en_GB\":\"Please take a seat.\"}",
                "msgMaintenance": "{\"de_DE\":\"Systemwartung\",\"en_GB\":\"System is maintained.\"}",
                "lastChange":""
            }'
        ], [], 'POST');

        $this->classname = 'CheckInConfigListByDepartment';

        $response = $this->render(['id' => '74'], [], []);

        self::assertTrue(200 == $response->getStatusCode());
        self::assertStringContainsString('checkInConfig.json', (string)$response->getBody());
    }
}
