<?php

declare(strict_types=1);

namespace BO\Zmsapi\Tests;

use BO\Slim\Container;
use BO\Slim\Response;
use BO\Zmsapi\Exception\Useraccount\InvalidCredentials;
use BO\Zmsapi\MaintenanceScheduleGet;
use BO\Zmsentities\MaintenanceSchedule;
use BO\Zmsdb\MaintenanceSchedule as Repository;
use Fig\Http\Message\StatusCodeInterface;
use Prophecy\PhpUnit\ProphecyTrait;

class MaintenanceScheduleGetTest extends Base
{
    use ProphecyTrait;

    public function testRendering()
    {
        $request = self::createBasicRequest('GET', '/maintenanceschedule/12/', ['X-Token' => \App::SECURE_TOKEN]);
        $entity  = new MaintenanceSchedule(['id' => 12, 'startDateTime' => '2001-01-01 12:34']);

        $repoProphecy = $this->prophesize(Repository::class);
        $repoProphecy
            ->readEntity(12)
            ->shouldBeCalled()
            ->willReturn($entity);

        $container = new Container();
        $container->offsetSet('zmsdb.repositories.maintenance-schedule', $repoProphecy->reveal());
        $controller = new MaintenanceScheduleGet($container);

        $response = $controller->readResponse($request, new Response(), ['id' => 12]);
        self::assertStringContainsString('maintenanceSchedule.json', (string)$response->getBody());
        self::assertSame(StatusCodeInterface::STATUS_OK, $response->getStatusCode());
    }

    public function testEntityNotFound(): void
    {
        $request = self::createBasicRequest('GET', '/maintenanceschedule/12/', ['X-Token' => \App::SECURE_TOKEN]);

        $repoProphecy = $this->prophesize(Repository::class);
        $repoProphecy
            ->readEntity(12)
            ->shouldBeCalled()
            ->willReturn(null);

        $container = new Container();
        $container->offsetSet('zmsdb.repositories.maintenance-schedule', $repoProphecy->reveal());
        $controller = new MaintenanceScheduleGet($container);

        $response = $controller->readResponse($request, new Response(), ['id' => 12]);

        self::assertSame(StatusCodeInterface::STATUS_NOT_FOUND, $response->getStatusCode());
    }

    public function testReadActiveEntity(): void
    {
        $request = self::createBasicRequest('GET', '/maintenanceschedule/active/', ['X-Token' => \App::SECURE_TOKEN]);
        $entity  = new MaintenanceSchedule([
            'id' => 12,
            'startDateTime' => '2001-01-01 00:00',
            'timeString' => '0 0 * * *',
            'isActive' => true,
            'isRepetitive' => true
        ]);

        $repoProphecy = $this->prophesize(Repository::class);
        $repoProphecy
            ->readActiveEntity()
            ->shouldBeCalled()
            ->willReturn($entity);

        $container = new Container();
        $container->offsetSet('zmsdb.repositories.maintenance-schedule', $repoProphecy->reveal());
        $controller = new MaintenanceScheduleGet($container);

        $response = $controller->readResponse($request, new Response(), ['id' => 12]);

        self::assertSame(StatusCodeInterface::STATUS_OK, $response->getStatusCode());
        self::assertStringContainsString('maintenanceSchedule.json', (string)$response->getBody());
    }

    public function testBadRequest(): void
    {
        $request = self::createBasicRequest('GET', '/maintenanceschedule/0/', ['X-Token' => \App::SECURE_TOKEN]);
        $container = new Container();
        $repoProphecy = $this->prophesize(Repository::class);
        $container->offsetSet('zmsdb.repositories.maintenance-schedule', $repoProphecy->reveal());

        $controller = new MaintenanceScheduleGet($container);

        $response = $controller->readResponse($request, new Response(), ['id' => 0]);
        self::assertSame(StatusCodeInterface::STATUS_BAD_REQUEST, $response->getStatusCode());
    }

    public function testCredentialsFail(): void
    {
        $this->expectException(InvalidCredentials::class);
        $request = self::createBasicRequest('GET', '/maintenanceschedule/0/', ['X-Token' => 'WrongToken']);
        $container = new Container();
        $repoProphecy = $this->prophesize(Repository::class);
        $container->offsetSet('zmsdb.repositories.maintenance-schedule', $repoProphecy->reveal());

        $controller = new MaintenanceScheduleGet($container);
        $controller->readResponse($request, new Response(), ['id' => 0]);
    }
}
