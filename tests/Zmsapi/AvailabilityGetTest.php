<?php

namespace BO\Zmsapi\Tests;

use BO\Zmsentities\Availability as Entity;

use BO\Zmsapi\Helper\User;

use BO\Zmsdb\Availability as Query;

class AvailabilityGetTest extends Base
{
    protected $classname = "AvailabilityGet";

    public function testRendering()
    {
        $input = (new Entity)->createExample();
        $entity = (new Query())->writeEntity($input);
        $this->setWorkstation();
        User::$workstation->useraccount->setRights('availability');
        $response = $this->render(['id' => $entity->getId()], [], []);
        $this->assertStringContainsString('availability.json', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
    }

    public function testMissingRights()
    {
        $input = (new Entity)->createExample();
        $entity = (new Query())->writeEntity($input);
        $this->setWorkstation();
        User::$workstation->useraccount->setRights('department');
        $this->expectException('\BO\Zmsentities\Exception\UserAccountMissingRights');

        $this->render(['id' => $entity->getId()], [], []);
    }

    public function testEmpty()
    {
        $this->setWorkstation();
        User::$workstation->useraccount->setRights('availability');
        $this->expectException('\ErrorException');
        $this->render([], [], []);
    }

    public function testNotFound()
    {
        $this->setWorkstation();
        User::$workstation->useraccount->setRights('availability');
        $this->expectException('\BO\Zmsapi\Exception\Availability\AvailabilityNotFound');
        $this->expectExceptionCode(404);
        $this->render(['id' => 1], [], []);
    }
}
