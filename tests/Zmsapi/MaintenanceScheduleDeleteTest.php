<?php
/**
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmsapi\Tests;

use BO\Zmsdb\MaintenanceSchedule as Repository;
use BO\Zmsdb\Log\TestLogger;
use BO\Zmsdb\Config;
use Fig\Http\Message\StatusCodeInterface;

class MaintenanceScheduleDeleteTest extends Base
{
    protected $classname = 'MaintenanceScheduleDelete';

    public function testRendering()
    {
        $maintenanceRepo  = new Repository();
        $configRepository = new Config();
        $loggerInstance   = new TestLogger();
        \App::$slim->getContainer()->offsetSet('zmsdb.repositories.maintenance-schedule', $maintenanceRepo);
        \App::$slim->getContainer()->offsetSet('zmsdb.repositories.config', $configRepository);
        \App::$slim->getContainer()->offsetSet('logger.default', $loggerInstance);

        $this->setWorkstation()->getUseraccount()->setRights('superuser', 'useraccount');
        $response = $this->render(['id' => 999], [], [], 'DELETE');

        self::assertSame(StatusCodeInterface::STATUS_OK, $response->getStatusCode());

        $response = $this->render(['id' => null], [], [], 'DELETE');
        self::assertSame(StatusCodeInterface::STATUS_BAD_REQUEST, $response->getStatusCode());
    }
}
