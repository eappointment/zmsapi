<?php

namespace BO\Zmsapi\Tests;

class WorkstationProcessGetTest extends Base
{
    protected $classname = "WorkstationProcessGet";

    public function testRendering()
    {
        $this->setWorkstation();
        $response = $this->render(['id' => 11468], [], []);
        $this->assertStringContainsString('process.json', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
    }

    public function testProcessMatchingWithWorkstationScopeFailed()
    {
        $this->expectException('BO\Zmsentities\Exception\WorkstationProcessMatchScopeFailed');
        $this->setWorkstation();
        $response = $this->render(['id' => 100032], [], []);
    }

    public function testProcessMatchingWithSelectedScope()
    {
        $this->setWorkstation();
        $response = $this->render(['id' => 100032], ['selectedscope' => 313], []);
        $this->assertStringContainsString('process.json', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
    }

    public function testEmpty()
    {
        $this->setWorkstation();
        $this->expectException('\ErrorException');
        $this->render([], [], []);
    }

    public function testNotFound()
    {
        $this->setWorkstation();
        $this->expectException('\BO\Zmsapi\Exception\Process\ProcessNotFound');
        $this->expectExceptionCode(404);
        $this->render(['id' => 999], [], []);
    }

    public function testNoLogin()
    {
        $this->expectException('BO\Zmsentities\Exception\UserAccountMissingLogin');
        $this->expectExceptionCode(401);
        $this->render([], [], []);
    }
}
