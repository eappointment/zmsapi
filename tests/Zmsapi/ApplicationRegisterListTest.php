<?php

declare(strict_types=1);

namespace BO\Zmsapi\Tests;

use BO\Mellon\Validator;
use BO\Slim\Container;
use BO\Slim\Response;
use BO\Zmsapi\ApplicationRegisterList;
use BO\Zmsdb\ApplicationRegister as Repository;
use BO\Zmsentities\ApplicationRegister;
use Prophecy\PhpUnit\ProphecyTrait;

class ApplicationRegisterListTest extends Base
{
    use ProphecyTrait;

    public function testRendering()
    {
        $this->setWorkstation()->getUseraccount()->setRights('scope', 'useraccount');
        $this->setValidatorInstance([]);
        $request   = $this->getRequest('GET', '/applicationregister/');
        $validator = Validator::getInstance();
        $listItem  = new ApplicationRegister();
        $theList   = new \BO\Zmsentities\Collection\ApplicationRegisterList();

        $repoProphecy = $this->prophesize(Repository::class);
        $repoProphecy
            ->readList()
            ->shouldBeCalled()
            ->willReturn($theList->addEntity($listItem));
        $repoProphecy
            ->readListByScopeIds([141,142], 'calldisplay')
            ->shouldBeCalled()
            ->willReturn($theList->addEntity($listItem));

        $container = new Container();
        $container->offsetSet('zmsdb.repositories.application-register', $repoProphecy->reveal());
        $controller = new ApplicationRegisterList($container);

        $response = $controller->readResponse($request->withAttribute('validator', $validator), new Response(), []);
        $this->assertStringContainsString('applicationRegister.json', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());

        $this->setValidatorInstance(['scopeIds' => '141,142', 'type' => ApplicationRegister::TYPE_ZMS2_CALLDISPLAY]);
        $validator = Validator::getInstance();

        $response = $controller->readResponse($request->withAttribute('validator', $validator), new Response(), []);
        $this->assertStringContainsString('applicationRegister.json', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
    }
}
