<?php
/**
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmsapi\Tests;

use BO\Zmsdb\Config;
use BO\Zmsdb\MaintenanceSchedule as Repository;
use BO\Zmsdb\Log\TestLogger;

use BO\Zmsentities\MaintenanceSchedule;

use Fig\Http\Message\StatusCodeInterface;

use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

class MaintenanceScheduleAddTest extends Base
{
    use ProphecyTrait;

    protected $classname = "MaintenanceScheduleAdd";

    public function testRendering()
    {
        $this->setWorkstation()->getUseraccount()->setRights('superuser', 'useraccount');

        $maintenanceRepo  = new Repository();
        $configRepository = new Config();
        $loggerInstance   = new TestLogger();
        \App::$slim->getContainer()->offsetSet('zmsdb.repositories.maintenance-schedule', $maintenanceRepo);
        \App::$slim->getContainer()->offsetSet('zmsdb.repositories.config', $configRepository);
        \App::$slim->getContainer()->offsetSet('logger.default', $loggerInstance);

        $response = $this->render([], [
            '__body' => '{
                "isActive": 0,
                "isRepetitive": 1,
                "timeString": "0 23 * * *",
                "duration": 90,
                "leadTime": 30,
                "area": "zms",
                "announcement": "Wait for it",
                "documentBody": "There it is"
            }'
        ], [], 'POST');

        self::assertStringContainsString('maintenanceSchedule.json', (string)$response->getBody());
        self::assertTrue(200 == $response->getStatusCode());
    }

    public function testDbOperationFails(): void
    {
        $this->setWorkstation()->getUseraccount()->setRights('superuser', 'useraccount');

        $repoProphecy  = $this->prophesize(Repository::class);
        $repoProphecy
            ->writeEntity(Argument::type(MaintenanceSchedule::class))
            ->shouldBeCalled()
            ->willReturn(null);
        \App::$slim->getContainer()->offsetSet('zmsdb.repositories.maintenance-schedule', $repoProphecy->reveal());

        $configRepository = new Config();
        $loggerInstance   = new TestLogger();
        \App::$slim->getContainer()->offsetSet('zmsdb.repositories.config', $configRepository);
        \App::$slim->getContainer()->offsetSet('logger.default', $loggerInstance);

        $response = $this->render([], [
            '__body' => '{
                "isActive": 0,
                "isRepetitive": 1,
                "timeString": "0 23 * * *",
                "duration": 90,
                "leadTime": 30,
                "area": "zms",
                "announcement": "Wait for it",
                "documentBody": "There it is"
            }'
        ], [], 'POST');

        self::assertSame(StatusCodeInterface::STATUS_INTERNAL_SERVER_ERROR, $response->getStatusCode());
    }
}
