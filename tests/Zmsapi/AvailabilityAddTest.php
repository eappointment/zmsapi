<?php

namespace BO\Zmsapi\Tests;

use BO\Zmsapi\Helper\User;

class AvailabilityAddTest extends Base
{
    protected $classname = "AvailabilityAdd";

    public function testRendering()
    {
        $this->setWorkstation();
        User::$workstation->useraccount->setRights('availability');
        $response = $this->render([], [
            '__body' => '[
                {
                    "id": 21202,
                    "description": "Test Öffnungszeit update",
                    "scope": {
                        "id": 312
                    }
                },
                {
                    "description": "Test Öffnungszeit ohne id",
                    "scope": {
                        "id": 141
                    }
                }
            ]'
        ], [], 'POST');
        $this->assertStringContainsString('availability.json', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
    }

    public function testMissingRights()
    {
        $this->setWorkstation();
        User::$workstation->useraccount->setRights('department');
        $this->expectException('\BO\Zmsentities\Exception\UserAccountMissingRights');

        $this->render([], [
            '__body' => '[
                {
                    "id": 21202,
                    "description": "Test Öffnungszeit update",
                    "scope": {
                        "id": 312
                    }
                },
                {
                    "description": "Test Öffnungszeit ohne id",
                    "scope": {
                        "id": 141
                    }
                }
            ]'
        ], [], 'POST');
    }

    public function testEmpty()
    {
        $this->setWorkstation();
        User::$workstation->useraccount->setRights('availability');
        $this->expectException('\BO\Mellon\Failure\Exception');
        $this->render([], [], [], 'POST');
    }

    public function testEmptyBody()
    {
        $this->setWorkstation();
        User::$workstation->useraccount->setRights('availability');
        $this->expectException('\BO\Zmsapi\Exception\BadRequest');
        $this->expectExceptionCode(400);
        $this->render([], [
            '__body' => '[]'
        ], [], 'POST');
    }

    public function testUpdateFailed()
    {
        $this->setWorkstation();
        User::$workstation->useraccount->setRights('availability');
        $this->expectException('\BO\Zmsapi\Exception\Availability\AvailabilityUpdateFailed');
        $this->expectExceptionCode(400);
        $this->render([], [
            '__body' => '[
                {
                  "id": 99999,
                  "description": "Test Öffnungszeit update failed",
                  "scope": {
                      "id": 312
                  }
                }
            ]',
            'migrationfix' => 0
        ], [], 'POST');
    }
}
