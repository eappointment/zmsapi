<?php

namespace BO\Zmsapi\Tests;

use \BO\Slim\Render;
use BO\Zmsapi\Helper\User;

class PollingGetTest extends Base
{
    protected $classname = "PollingGet";

    public function testRendering()
    {
        $this->setWorkstation();
        User::$workstation->useraccount->setRights('superuser');
        $response = $this->render([], ['report' => 'distance', 'date' => '2016-02-24'], []);
        $this->assertStringContainsString('"buchung":"2016-02-24"', (string)$response->getBody());
        $this->assertStringContainsString('"Termin":"2016-04-04"', (string)$response->getBody());
        $this->assertStringContainsString('"Tage":"40"', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
    }

    public function testMissingLogin()
    {
        $this->expectException('\BO\Zmsentities\Exception\UserAccountMissingLogin');
        $this->expectExceptionCode(401);
        $this->render([], [], []);
    }

    public function testMissingRights()
    {
        $this->setWorkstation();
        User::$workstation->useraccount->setRights('basic');
        $this->expectException('\BO\Zmsentities\Exception\UserAccountMissingRights');
        $this->expectExceptionCode(403);
        $this->render([], [], []);
    }
}
