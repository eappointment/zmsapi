<?php

namespace BO\Zmsapi\Tests;

use BO\Mellon\ValidMail;
use Fig\Http\Message\StatusCodeInterface;

class ProcessListSummaryMailTest extends Base
{
    protected $classname = "ProcessListSummaryMail";

    protected bool $tmp;

    public function setUp(): void
    {
        $this->tmp = ValidMail::$disableDnsChecks;
        ValidMail::$disableDnsChecks = true;

        parent::setUp();
    }

    public function tearDown(): void
    {
        ValidMail::$disableDnsChecks = $this->tmp;
        \App::$now = new \DateTimeImmutable('2016-04-01T11:55:00+02:00');

        parent::tearDown();
    }

    public function testRendering()
    {
        \App::$now = new \DateTimeImmutable('2016-04-01T07:05:00+2:00');

        $response = $this->render([], ['mail' => 'zms@service.berlinonline.de', 'limit' => 10], []);

        self::assertStringContainsString('Sie haben folgende Termine gebucht', (string)$response->getBody());
        self::assertStringContainsString('10178', (string)$response->getBody());
        self::assertStringContainsString('13359', (string)$response->getBody());
        self::assertStringContainsString('Freitag, 01. April 2016 um 07:10 Uhr', (string)$response->getBody());
        // test the filter of expired appointments
        self::assertStringNotContainsString('um 07:00 Uhr', (string)$response->getBody());

        self::assertSame(StatusCodeInterface::STATUS_OK, $response->getStatusCode());

        $this->testShortRepetitionSuccess();
    }

    public function testMailSenderFromMissing()
    {
        $this->expectException('BO\Zmsapi\Exception\Mail\MailSenderFromMissing');
        $response = $this->render([], ['mail' => 'not.existing@service.berlinonline.de', 'limit' => 3], []);
        self::assertSame(StatusCodeInterface::STATUS_FORBIDDEN, $response->getStatusCode());
    }

    public function testShortRepetitionFailure()
    {
        $this->testRendering();

        $this->expectException('BO\Zmsapi\Exception\Process\ProcessListSummaryTooOften');
        $response = $this->render([], ['mail' => 'zms@service.berlinonline.de', 'limit' => 3], []);
        self::assertSame(StatusCodeInterface::STATUS_TOO_MANY_REQUESTS, $response->getStatusCode());
    }

    private function testShortRepetitionSuccess()
    {
        \App::$now = \App::$now->modify("+ 15Minutes");

        $response = $this->render([], ['mail' => 'zms@service.berlinonline.de', 'limit' => 30], []);
        self::assertStringContainsString('Sie haben folgende Termine gebucht', (string)$response->getBody());
    }
    
    public function testProcessListEmpty()
    {
        $configRepository = (new \BO\Zmsdb\Config());
        $config = $configRepository->readEntity();
        $config->setPreference('mailings', 'noReplyDepartmentId', '74');
        $configRepository->updateEntity($config);
        $response = $this->render([], ['mail' => 'not.existing@service.berlinonline.de'], []);
        self::assertStringContainsString('Es wurden keine gebuchten Termine gefunden.', (string)$response->getBody());
    }

    public function testUnvalidMail()
    {
        ValidMail::$disableDnsChecks = false;

        $this->expectException('BO\Mellon\Failure\Exception');
        $this->expectExceptionMessage(
            "Validation failed: no valid email\nno valid DNS entry found\n({mail}=='test@unit')"
        );
        $this->render([], ['mail' => 'test@unit'], []);
    }
}
