<?php

namespace BO\Zmsapi\Tests;

use \BO\Zmsdb\Apikey as Query;

use \BO\Zmsentities\Apikey as Entity;

class CaptchaGetTest extends Base
{
    protected $classname = "CaptchaGet";

    public function testRendering()
    {
        $response = $this->render([], [], []);
        $this->assertStringContainsString('apikey.json', (string)$response->getBody());
        $this->assertStringContainsString(
            '"captcha":{"mime":"image/jpeg;base64","attachmentName":"","content":"data:image/jpeg;base64',
            (string)$response->getBody()
        );
        $this->assertTrue(200 == $response->getStatusCode());
    }
}
