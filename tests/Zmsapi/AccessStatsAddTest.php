<?php

declare(strict_types=1);

namespace BO\Zmsapi\Tests;

use BO\Zmsdb\AccessStats as Repository;

class AccessStatsAddTest extends Base
{
    protected $classname = "AccessStatsAdd";

    public function testRendering()
    {
        $repo = new Repository();
        \App::$slim->getContainer()->offsetSet('zmsdb.repositories.access-stats', $repo);

        $response = $this->render([], [
            '__body' => '{
                "id": 34764734357,
                "role": "user",
                "location": "zmsadmin",
                "lastActive": 1685534773
            }'
        ], [], 'POST');

        self::assertStringContainsString('accessStats.json', (string)$response->getBody());
        self::assertTrue(200 == $response->getStatusCode());
    }
}
