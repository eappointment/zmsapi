<?php

declare(strict_types=1);

namespace BO\Zmsapi\Tests;

use Fig\Http\Message\StatusCodeInterface;

class ApplicationRegisterChangeTest extends Base
{
    public function testRendering()
    {
        $this->prepareEntity();
        $this->classname = 'ApplicationRegisterChange';
        $this->setWorkstation()->getUseraccount()->setRights('scope', 'useraccount');

        $input = json_encode(['parameters' => 'ticketprinter[buttonlist]=s140']);
        $response = $this->render(
            ['id' => 'hbwd295dec9f5dc0c4f5fb93aa3e0ba28c4X3afC'],
            ['__body' => $input],
            [],
            'POST'
        );

        self::assertTrue(StatusCodeInterface::STATUS_ACCEPTED == $response->getStatusCode());

        $this->testChange();
    }

    protected function prepareEntity(): void
    {
        (new ApplicationRegisterAddTest())->testRendering();
    }

    protected function testChange(): void
    {
        $this->classname = 'ApplicationRegisterGet';

        $response = $this->render(
            ['id' => 'hbwd295dec9f5dc0c4f5fb93aa3e0ba28c4X3afC'],
            [],
            []
        );

        self::assertSame(StatusCodeInterface::STATUS_OK, $response->getStatusCode());
        self::assertStringContainsString(
            '"requestedChange":{"parameters":"ticketprinter[buttonlist]=s140"}',
            (string)$response->getBody()
        );
    }
}
