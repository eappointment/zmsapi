<?php

namespace BO\Zmsapi\Tests;

use \BO\Zmsdb\Apikey as ApikeyRepository;

use \BO\Zmsentities\Apikey as Entity;

class ApikeyDeleteTest extends Base
{
    protected $classname = "ApikeyDelete";

    public function testRendering()
    {
        $entity = (new Entity())->createExample();
        (new ApikeyRepository())->writeEntity($entity);
        $response = $this->render(['key' => 'wMdVa5Nu1seuCRSJxhKl2M3yw8zqaAilPH2Xc2IZs'], [], []);
        $this->assertStringContainsString('"data":null', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
    }
}
