<?php

declare(strict_types=1);

namespace BO\Zmsapi\Tests;

use BO\Slim\Container;
use BO\Slim\Response;
use BO\Zmsapi\CheckInConfigGet;
use BO\Zmsentities\CheckInConfig as ConfigEntity;
use BO\Zmsdb\CheckInConfig as Repository;
use Fig\Http\Message\StatusCodeInterface;

use Prophecy\PhpUnit\ProphecyTrait;

class CheckInConfigGetTest extends Base
{
    use ProphecyTrait;

    public function tearDown(): void
    {
        \BO\Zmsapi\Helper\User::$workstation = null;
    }

    public function testRendering()
    {
        $this->setWorkstation()->getUseraccount()->setRights('scope', 'useraccount');

        $request = self::createBasicRequest('GET', '/checkinconfig/abcdef1234556/', ['X-Token' => \App::SECURE_TOKEN]);
        $entity  = new ConfigEntity(['id' => 'abcdef1234556', 'scopeIds' => ':123:234:']);

        $repoProphecy = $this->prophesize(Repository::class);
        $repoProphecy
            ->readById('abcdef1234556')
            ->shouldBeCalled()
            ->willReturn($entity);

        $container = new Container();
        $container->offsetSet('zmsdb.repositories.check-in-config', $repoProphecy->reveal());
        $controller = new CheckInConfigGet($container);

        $response = $controller->readResponse($request, new Response(), ['id' => 'abcdef1234556']);
        self::assertStringContainsString('checkInConfig.json', (string)$response->getBody());
        self::assertStringContainsString(':123:234:', (string)$response->getBody());
        self::assertSame(StatusCodeInterface::STATUS_OK, $response->getStatusCode());
    }
}
