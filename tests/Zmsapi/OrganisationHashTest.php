<?php

namespace BO\Zmsapi\Tests;
use BO\Zmsdb\Ticketprinter as TicketprinterRepo;

class OrganisationHashTest extends Base
{
    protected $classname = "OrganisationHash";

    public function testRendering()
    {
        $repo = new TicketprinterRepo();
        \App::$slim->getContainer()->offsetSet('zmsdb.repositories.ticketprinter', $repo);

        $response = $this->render(['id' => 54], [], []); //Pankow
        $this->assertStringContainsString('ticketprinter.json', (string)$response->getBody());
        $this->assertStringContainsString('"id":', (string)$response->getBody());
        $this->assertStringContainsString('"hash":"54', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
    }

    public function testWithName()
    {
        $response = $this->render(['id' => 54], ['name' => 'unittest'], []); //Pankow
        $this->assertStringContainsString('"id":', (string)$response->getBody());
        $this->assertStringContainsString('"name":"unittest"', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
    }

    /**
     * the ticketprinter got a certain identifier with its start
     */
    public function testWithSid()
    {
        $response = $this->render(['id' => 54], ['sid' => 'hbw90dec13011a6f3b095d077986bbcb648jr0hq'], []); //Pankow
        $this->assertStringContainsString('"id":', (string)$response->getBody());
        $this->assertStringContainsString(
            '"hash":"hbw90dec13011a6f3b095d077986bbcb648jr0hq"',
            (string)$response->getBody()
        );
        $this->assertTrue(200 == $response->getStatusCode());
    }

    public function testTicketprinterDisabled()
    {
        $response = $this->render(['id' => 65], [], []); //Friedrichshain-Kreuzberg mit kioskpasswortschutz
        $this->assertStringContainsString('ticketprinter.json', (string)$response->getBody());
        $this->assertStringContainsString('"id":', (string)$response->getBody());
        $this->assertStringContainsString('"enabled":false', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
    }

    public function testOrganisationNotFound()
    {
        $this->expectException('\BO\Zmsapi\Exception\Organisation\OrganisationNotFound');
        $this->expectExceptionCode(404);
        $this->render(['id' => 999], [], []);
    }
}
