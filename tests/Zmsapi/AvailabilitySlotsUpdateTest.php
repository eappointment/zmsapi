<?php

namespace BO\Zmsapi\Tests;

use BO\Zmsapi\Helper\User;

class AvailabilitySlotsUpdateTest extends Base
{
    protected $classname = "AvailabilitySlotsUpdate";

    public function testRendering()
    {
        $this->setWorkstation();
        User::$workstation->useraccount->setRights('availability');
        $response = $this->render([], [
            '__body' => '[
                {
                    "id": 21202,
                    "description": "Test Öffnungszeit update",
                    "scope": {
                        "id": 312
                    }
                }
            ]'
        ], [], 'POST');
        $this->assertStringContainsString('availability.json', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
    }

    public function testMissingRights()
    {
        $this->setWorkstation();
        User::$workstation->useraccount->setRights('department');
        $this->expectException('\BO\Zmsentities\Exception\UserAccountMissingRights');

        $this->render([], [
            '__body' => '[
                {
                    "id": 21202,
                    "description": "Test Öffnungszeit update",
                    "scope": {
                        "id": 312
                    }
                }
            ]'
        ], [], 'POST');
    }

    public function testEmpty()
    {
        $this->setWorkstation();
        User::$workstation->useraccount->setRights('availability');
        $this->expectException('\BO\Mellon\Failure\Exception');
        $this->render([], [], []);
    }

    public function testEmptyBody()
    {
        $this->setWorkstation();
        User::$workstation->useraccount->setRights('availability');
        $this->expectException('\BO\Zmsapi\Exception\BadRequest');
        $this->expectExceptionCode(400);
        $this->render([], [
            '__body' => '[]'
        ], [], 'POST');
    }

    public function testNotFound()
    {
        $this->setWorkstation();
        User::$workstation->useraccount->setRights('availability');
        $this->expectException('\BO\Zmsapi\Exception\Availability\AvailabilityNotFound');
        $this->expectExceptionCode(404);
        $this->render([], [
            '__body' => '[
                {
                  "id": 99999,
                  "description": "Test Öffnungszeit not found",
                  "scope": {
                      "id": 312
                  }
                }
            ]',
            'migrationfix' => 0
        ], [], 'POST');
    }
}
