<?php

namespace BO\Zmsapi\Tests;

use App;
use BO\Zmsdb\Apiclient as ApiclientRepository;
use BO\Zmsdb\Apikey as ApikeyRepository;
use BO\Zmsentities\Apiclient as ApiclientEntity;
use BO\Zmsentities\Apikey as ApikeyEntity;

use \BO\Zmsdb\Apikey as Query;
use BO\Zmsentities\Apikey as Entity;

class ApikeyUpdateTest extends Base
{
    protected $classname = "ApikeyUpdate";

    public function testRendering()
    {
        $entity = (new Entity)->createExample();
        $entity->apiclient['apiClientID'] = 1;
        $entity->apiclient['clientKey'] = 'default';
        $entity->key = 'onffaoiv0Bcl68CwHeLOHedeyvMkphYMr7p1MimO';
        $input = json_encode($entity);
        $response = $this->render([], [
            '__body' => $input
        ], [], 'POST');
        $this->assertStringContainsString('apikey.json', (string)$response->getBody());
        $this->assertStringContainsString('onffaoiv0Bcl68CwHeLOHedeyvMkphYMr7p1MimO', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
    }

    public function testUpdateExistingApikey()
    {
        $input = (new Entity)->createExample();
        $entity = (new Query())->writeEntity($input);

        $response = $this->render([], [
            '__body' => (string) $entity
        ], [], 'POST');
        $this->assertStringContainsString('apikey.json', (string)$response->getBody());
        $this->assertStringContainsString('wMdVa5Nu1seuCRSJxhKl2M3yw8zqaAilPH2Xc2IZs', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
    }

    public function testClientkey()
    {
        $input = json_encode((new Entity)->createExample());
        $response = $this->render([], [
            '__body' => $input,
            'clientkey' => 'default',
        ], [], 'POST');
        $this->assertStringContainsString('apikey.json', (string)$response->getBody());
        $this->assertStringContainsString('wMdVa5Nu1seuCRSJxhKl2M3yw8zqaAilPH2Xc2IZs', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
    }

    public function testClientkeyBlocked()
    {
        $this->expectException('BO\Zmsapi\Exception\Apikey\AccessDenied');
        $this->expectExceptionCode(400);
        $input = json_encode((new Entity)->createExample());
        $this->render([], [
            '__body' => $input,
            'clientkey' => '8pnaRHkUBYJqz9i9NPDEeZq6mUDMyRHE',
        ], [], 'POST');
    }

    public function testClientkeyInvalid()
    {
        $this->expectException('BO\Zmsapi\Exception\Apikey\AccessDenied');
        $this->expectExceptionCode(400);
        $input = json_encode((new Entity)->createExample());
        $this->render([], [
            '__body' => $input,
            'clientkey' => '_invalid',
        ], [], 'POST');
    }

    public function testUpdateDataNotMatchingApiKey()
    {
        // write apiclient
        $apiclientEntity = new ApiclientEntity([
            'clientKey' => self::API_KEY,
            'shortname' => self::USER,
            'accesslevel' => 'public',
            'permission__remotecall' => 0,
            'secondsToInvalidation' => 60,
        ]);
        (new ApiclientRepository())->writeEntity($apiclientEntity);
        $this->apiclient = (new ApiclientRepository())->readEntity(self::API_KEY);

        // write apikey
        $apikeyEntity = new ApikeyEntity([
            'key' => 'onffaoiv0Bcl68CwHeLOHedeyvMkphYMr7p1MimO',
            'createIP' => '',
            'ts' => App::$now->getTimestamp(),
            'apiclient' => $this->apiclient,
        ]);
        (new ApikeyRepository())->writeEntity($apikeyEntity);

        $entity = (new Entity)->createExample();
        $entity->apiclient['apiClientID'] = $this->apiclient->apiClientID;
        $entity->apiclient['clientKey'] = $this->apiclient->clientKey;
        $entity->key = 'unittest'; //not matching $apikeyEntity::key
        $input = json_encode($entity);

        $response = $this->render([], [
            '__body' => $input,
            'clientkey' => $this->apiclient->clientKey,
        ], [], 'POST');

        $this->assertStringContainsString('apikey.json', (string)$response->getBody());
        $this->assertStringNotContainsString('onffaoiv0Bcl68CwHeLOHedeyvMkphYMr7p1MimO', (string)$response->getBody());
        $this->assertStringContainsString('unittest', (string)$response->getBody());
    }

    public function testUpdateDataNotMatchingClientKey()
    {
        // write apiclient
        $apiclientEntity = new ApiclientEntity([
            'clientKey' => self::API_KEY,
            'shortname' => self::USER,
            'accesslevel' => 'public',
            'permission__remotecall' => 0,
            'secondsToInvalidation' => 60,
        ]);
        (new ApiclientRepository())->writeEntity($apiclientEntity);
        $this->apiclient = (new ApiclientRepository())->readEntity(self::API_KEY);

        // write apikey
        $apikeyEntity = new ApikeyEntity([
            'key' => 'onffaoiv0Bcl68CwHeLOHedeyvMkphYMr7p1MimO',
            'createIP' => '',
            'ts' => App::$now->getTimestamp(),
            'apiclient' => $this->apiclient,
        ]);
        (new ApikeyRepository())->writeEntity($apikeyEntity);

        $entity = (new ApikeyEntity)->createExample();
        $entity->apiclient['apiClientID'] = $this->apiclient->apiClientID;
        $entity->apiclient['clientKey'] = 'unittest'; //not matching $apiclient::clientkey
        $entity->key = $this->apiclient->clientKey;
        $input = json_encode($entity);

        $response = $this->render([], [
            '__body' => $input,
            'clientkey' => self::API_KEY,
        ], [], 'POST');

        $this->assertStringContainsString('apikey.json', (string)$response->getBody());
        $this->assertStringNotContainsString('unittest', (string)$response->getBody());
    }
}
