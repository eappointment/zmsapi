<?php

declare(strict_types=1);

namespace BO\Zmsapi\Tests;

use BO\Mellon\Validator;
use BO\Slim\Container;
use BO\Slim\Response;
use BO\Zmsapi\AccessStatsList;
use BO\Zmsdb\AccessStats as StatsRepository;
use BO\Zmsdb\Session as SessionRepository;
use BO\Zmsentities\AccessStats;
use BO\Zmsentities\Collection\AccessStatsList as AccessStatsCollection;
use BO\Zmsentities\Collection\SessionList;
use BO\Zmsentities\Session;
use Prophecy\PhpUnit\ProphecyTrait;

class AccessStatsListTest extends Base
{
    use ProphecyTrait;

    protected $classname = "AccessStatsList";

    public function testRendering()
    {
        $this->setWorkstation()->getUseraccount()->setRights('superuser', 'useraccount');
        $this->setValidatorInstance([]);
        $request   = $this->getRequest('GET', '/accessStats/');
        $validator = Validator::getInstance();
        $listItem  = new AccessStats([
            "id" => 34764734357,
            "role" => "user",
            "location" => "zmsadmin",
            "lastActive" => 1685534773,
        ]);
        $statsList = new AccessStatsCollection();
        $statsRepo = $this->prophesize(StatsRepository::class);
        $statsRepo
            ->readList()
            ->shouldBeCalled()
            ->willReturn($statsList->addEntity($listItem));

        $listItem  = new Session([
            "id" => '3b26ec21f79c0634fe61a894dc297d86',
            "name" => "Zmsappointment",
            "ts" => "2016-04-01 12:34:56",
        ]);
        $sessionList = new SessionList();
        $sessionRepo = $this->prophesize(SessionRepository::class);
        $sessionRepo
            ->readLastActiveList(900, 'Zmsappointment')
            ->shouldBeCalled()
            ->willReturn($sessionList->addEntity($listItem));

        $container = new Container();
        $container->offsetSet('zmsdb.repositories.access-stats', $statsRepo->reveal());
        $container->offsetSet('zmsdb.repositories.session', $sessionRepo->reveal());
        $controller = new AccessStatsList($container);

        $response = $controller->readResponse($request->withAttribute('validator', $validator), new Response(), []);
        $this->assertStringContainsString('accessStats.json', (string)$response->getBody());
        $this->assertStringContainsString('user', (string)$response->getBody());
        $this->assertStringContainsString('citizen', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
    }
}
